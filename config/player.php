<?php

return [
    'title' => 'IVS Proxy',
    'main_subdomain' => env('APP_MAIN_SUBDOMAIN'),
    'experiment' => [
        'style_tag' => '<link type="text/css" href="https://dev.ivideosmart.com/antoni/dev-ivideosense-player/style/player_v6.css" rel="stylesheet">',

        'script_tag' => '<script> window.ivsPlayerInfo = {ivsApiKey: "x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K", ivsWidgetID: "98586cd3-5138", ivsDomain: "prod", } </script> <script src="https://dev.ivideosmart.com/antoni/dev-ivideosense-player/js/player_v6.js"></script>',
    ],

    'v2' => [
        'widget_tag' => '<div id="ivs-widget-98586cd3-5138"></div>',
        'style_tag' => '<link type="text/css" href="https://player.ivideosmart.com/ivideosense/player/style/player_v6.03071304.css" rel="stylesheet">',
        'script_tag' => '<script>
    window.ivsPlayerInfo = {
        ivsApiKey: "x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K",
        ivsWidgetID: "98586cd3-5138",
        ivsDomain: "prod",
    }
</script>
<script src="https://player.ivideosmart.com/ivideosense/player/js/player_v6.03071304.js"></script>',
    ],

    'v3' => [
        'style_tag' => '<link href="https://s3v3.5kdg.com/app.css" rel="stylesheet">',

        'script_tag' => '<script type="text/javascript" src="https://s3v3.5kdg.com/vendors~sp-tools~video-player.bundle_4629fee6434b1029e9a0.chunk.js"></script><script type="text/javascript" src="https://s3v3.5kdg.com/vendors~sp-tools.bundle_69b23ac7d82734ed14a0.chunk.js"></script><script type="text/javascript" src="https://s3v3.5kdg.com/sp-tools.bundle.js"></script>',

        'widget_tag' => '<div>
    <ivs-video thumbnail="https://2.bp.blogspot.com/-xpgIOttwTTg/TWRni-cC-tI/AAAAAAAAA-o/9GMqFyMnQCY/s1600/african-elephant2.jpg" id="ivsplayer001"></ivs-video>
    <ivs-playlist type="carousel" widget-id="e40647ab-4135" api-key="38727828a6bcbae337183a66c50af04d" :playlist-data="playlistData" ivsplayer-id="ivsplayer001"></ivs-playlist>
</div>',
    ],
];
