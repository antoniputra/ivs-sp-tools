@extends('layout')

@section('content')

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-10">
            @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <strong>Whops! There are an error</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="card" v-pre>
                {!! Form::open(['method' => 'POST', 'url' => route('updateShortcode')]) !!}
                    <h4 class="card-header">Manage Shortcode</h4>
                    <div class="card-body">
                        <p>If you have a code that frequently to be used, instead of manually copy and paste your code You can use this Shortcode Feature. So you will be able to reuse your code anything, anytime, anywhere you like!</p>
                    </div>
                    
                    <h5 class="card-header">Default Shortcode</h5>
                    <div class="card-body">
                        @forelse ($options_default as $opt)
                            <div class="form-group py-2">
                                <label>{{ $opt->title }} &nbsp;-&nbsp; <code>{{ $opt->name }}</code></label>
                                {!! Form::textarea($opt->getNameAsFieldName(), old($opt->getNameAsFieldName(), $opt->value), ['class' => 'form-control '. ($errors->first($opt->getNameAsFieldName()) ? 'is-invalid' : ''), 'rows' => 5, 'required' => true, 'data-editor' => 'html']) !!}
                            </div>
                        @empty
                            There is no data yet. Run the seeder first!
                        @endforelse
                    </div>

                    <h5 class="card-header">Current Custom Shortcode ({{ $options->count() }} items)</h5>
                    <div class="card-body">
                        @php
                        $i = 0;
                        @endphp
                        @forelse ($options as $opt)
                            <div class="py-3 border-bottom" style="position: relative;">
                                <span class="badge badge-secondary" style="position: absolute; top: 5px; left: -25px; font-size: 0.9rem;">{{ ++$i }}</span>

                                <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?')) { location.href='{{ route('deleteShortcode', $opt->id) }}' }" style="position: absolute; right: 5px; top: 5px;" >&times; Delete</a>

                                {!! Form::hidden('settings['.$i.'][id]', $opt->id) !!}
                                <div class="form-group">
                                    <label>Name</label>
                                    {!! Form::text('settings['.$i.'][name]', old('settings['.$i.'][name]', $opt->name), ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Render to:</label>
                                    {!! Form::textarea('settings['.$i.'][value]', old('settings['.$i.'][value]', $opt->value), ['class' => 'form-control '. ($errors->first('ssetting['.$i.'][value]') ? 'is-invalid' : ''), 'rows' => 5, 'data-editor' => 'html']) !!}
                                </div>
                            </div>
                        @empty
                        There is no custom settings.
                        @endforelse
                    </div>

                    <h5 class="card-header">Add new Shortcode</h5>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Name</label>
                            {!! Form::text('settings['. $newKey .'][name]', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label>Render to:</label>
                            {!! Form::textarea('settings['. $newKey .'][value]', null, ['class' => 'form-control', 'rows' => 5, 'data-editor' => 'html']) !!}
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button type="submit" class="btn btn-primary">Save Settings</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop

@section('before_script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.3/ace.js"></script>
@stop

@section('after_script')
    <script>
        $('textarea[data-editor]').each(function () {
            var textarea = $(this);
            var mode = textarea.data('editor')
            var editDiv = $('<div>', {
              position: 'relative',
              width: textarea.width(),
              height: textarea.height(),
              'class': textarea.attr('class')
            }).insertBefore(textarea);
            textarea.css('display', 'none');

            var editor = window.ace.edit(editDiv[0], {
              mode: 'ace/mode/' + mode,
              maxLines: 12,
              minLines: 3,
              wrap: true,
              fontSize: '12px',
            });

            editor.getSession().setValue(textarea.val());

            // copy back to textarea on form submit...
            textarea.closest('form').submit(function (e) {
              // e.preventDefault();
              textarea.val(editor.getSession().getValue());
              // $('form').submit();
            })
        });

        $('button[type=submit]').click(function (e) {
            if ($(this).attr('name') == 'is_preview') {
                $(this).closest('form').attr('target', '_blank')
            } else {
                $(this).closest('form').removeAttr('target')
            }
        })
    </script>
@stop