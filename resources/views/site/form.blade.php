@extends('layout')

@section('content')

<div class="container">
    <a id="btn_shortcode" href="#" class="btn btn-secondary" style="position: fixed; right: 0px; z-index: 3;">&laquo; Show Shortcode</a>
    <div v-pre id="wrapper_shortcode" class="border shadow" style="position: fixed; top: 0px; right: -400px; background: #fff; width: 400px; height: 100%; z-index: 2;">
        <h4 class="card-header">List Shortcode</h4>
        <div class="card-body overflow-auto" style="padding-bottom: 100px; height: 100%;">
            @forelse ($shortcodes as $numb => $sc)
                <div class="pt-2 pb-3 border-bottom" style="position: relative;">
                    <label class="d-block m-0">
                        <span class="badge badge-secondary" style="position: absolute; left: -20px; top: 5px;">{{ ++$numb }}</span>
                        {{ $sc->title }}
                    </label>
                    <code class="d-block">{{ $sc->name }}</code>
                    <code class="d-block mt-2">{{ $sc->value }}</code>
                </div>
            @empty
                <p class="text-center text-muted">There is no Shortcode yet.</p>
            @endforelse
        </div>
    </div>

    <div class="row justify-content-md-center">
        <div class="col-md-10">
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    <strong>Whops! There are an error</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if (!empty($site))
                <h3 class="text-center mb-4">
                    Edit site: <b>{{ $site->title }}</b>
                    <small class="d-block mt-2"><a href="{{ $site->getResultUrl() }}" target="_blank">Visit Proxy Page</a></small>
                </h3>
            @endif

            <div class="card">
                @if (!empty($site))
                    {!! Form::model($site, ['method' => 'POST', 'url' => route('site.process', $site->id)]) !!}
                    {!! Form::hidden('site_id', $site->id) !!}
                @else
                    {!! Form::open(['method' => 'POST', 'url' => route('site.process')]) !!}
                @endif
                    <h5 class="card-header">1. Website Details</h5>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Target Website*</label>
                            {!! Form::text('url', null, ['class' => 'form-control '. ($errors->first('url') ? 'is-invalid' : ''), 'required' => true, 'placeholder' => 'https://kompas.com/article/title-prend']) !!}
                            <small id="emailHelp" class="form-text text-muted">Target website for attach widget.</small>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Subdomain (unique) <small class="d-block text-muted">You can leave this input blank it will automatically generated</small></label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">{{ env('APP_SP_SUBDOMAIN', 'spp-') }}</span>
                                </div>
                                {!! Form::text('subdomain', old('subdomain', str_replace(env('APP_SP_SUBDOMAIN', 'spp-'), '', @$site['subdomain'])), ['class' => 'form-control '. ($errors->first('subdomain') ? 'is-invalid' : ''), 'placeholder' => 'kompas']) !!}
                            </div>
                            <small class="form-text text-muted">Result url to display. e.g: <code>https://<b>{{ env('APP_SP_SUBDOMAIN', 'spp-') }}kompas</b>.{{ request()->server('HTTP_HOST') }}/some-article</code></small>
                        </div>

                        <div class="form-group">
                            <label>Replace Related Links (optional)</label>
                            {!! Form::text('replace_link_pattern', null, ['class' => 'form-control', 'placeholder' => 'news/nasional']) !!}
                        </div>

                        <br>
                        <div class="form-group">
                            <label>Style Tag:</label>
                            {!! Form::textarea('style_tag', null, ['rows' => 3, 'data-editor' => 'html', 'class' => 'form-control']) !!}
                            <p class="form-text my-3 text-muted">Style tag will be added before <code>&lt;/head&gt;</code></p>
                        </div>
                        <div class="form-group">
                            <label>Script Tag:</label>
                            {!! Form::textarea('script_tag', null, ['rows' => 3, 'data-editor' => 'html', 'class' => 'form-control']) !!}
                            <p class="form-text my-3 text-muted">Script tag will be added before <code>&lt;/body&gt;</code></p>
                        </div>
                    </div>

                    @if (!empty($site))
                        @if ($site->inject_script)
                            <h5 class="card-header border-top">Generated Script from Chrome Extension</h5>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Inject Script:</label>
                                    {!! Form::textarea('inject_script', null, ['rows' => 3, 'data-editor' => 'html', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        @endif
                    @endif

                    <div id="widget_regex">
                        <h5 class="card-header border-top">2. Widget Attachment</h5>
                        <div class="card-body">
                            <p class="text-muted">If you need to make sure, You can try your Regex Result in: <a href="https://regexr.com" target="_blank">Regex Online Tools &raquo;</a></p>
                            
                            <div class="form-group" style="font-size: 1.1rem;">
                                {!! Form::hidden('regex_pattern', null, ['class' => 'form-control']) !!}

                                <label class="d-block m-0">Regex Result:</label>
                                <code id="regex_result" style="letter-spacing: 1.3px;">/(<span id="display_regex_start"></span>)([.\s\S]*?)(<span id="display_regex_end"></span>)/</code>
                            </div>

                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col">
                                        <label>Start Position</label>
                                        @php
                                        $reg_val = !empty($site) ? $site->getRegexGroup(1) : null;
                                        @endphp
                                        {!! Form::text('regex_start', $reg_val, ['class' => 'form-control', 'placeholder' => 'Start target']) !!}
                                    </div>
                                    <div class="col">
                                        <label>End Position</label>
                                        @php
                                        $reg_val = !empty($site) ? $site->getRegexGroup(3) : null;
                                        @endphp
                                        {!! Form::text('regex_end', $reg_val, ['class' => 'form-control', 'placeholder' => 'End target']) !!}
                                    </div>
                                </div>
                                {{-- <p class="mt-2 text-muted text-center">You must fill both of Start and End Position value.</p> --}}
                            </div>

                            <div class="form-group">
                                <label>Regex Replacement</label>
                                {!! Form::textarea('regex_replacement', null, ['rows' => 3, 'data-editor' => 'html', 'class' => 'form-control']) !!}
                                <p class="my-2">
                                    <code>$1</code> is <u>Start Position Value</u>, <code>$2</code> is content in between, <code>$3</code> is <u>End Position Value</u>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="card-header border-top">
                        <h5 class="card-title d-inline" style="margin: 0px;">3. Replacements</h5>
                    </div>
                    <div class="py-4">
                        <p class="px-3">
                            The Replacements below will be processed using the <code>str_replace</code> php function.
                            {{-- If you put search value as multi-line (more than one line) it will be processed one by one each line. --}}
                        </p>

                        <replacements items="{{ json_encode(old('replacements', optional(@$site)->replacements)) }}"></replacements>
                    </div>

                    <h5 class="card-header border-top">4. Save to Collections (required)</h5>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Collection Name*:</label>
                            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'CY Kompas v3']) !!}
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{-- <button type="submit" name="is_preview" value="1" class="btn btn-secondary">Preview Only</button>
                        &nbsp;&nbsp; --}}
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop

@section('before_script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.3/ace.js"></script>
@stop

@section('after_script')
    <script>
        $('textarea[data-editor]').each(function () {
            var textarea = $(this);
            var mode = textarea.data('editor')
            var editDiv = $('<div>', {
              position: 'relative',
              width: textarea.width(),
              height: textarea.height(),
              'class': textarea.attr('class')
            }).insertBefore(textarea);
            textarea.css('display', 'none');

            var editor = window.ace.edit(editDiv[0], {
              mode: 'ace/mode/' + mode,
              maxLines: 15,
              minLines: 3,
              wrap: true,
              fontSize: '12px',
            });

            editor.getSession().setValue(textarea.val());

            // copy back to textarea on form submit...
            textarea.closest('form').submit(function (e) {
              textarea.val(editor.getSession().getValue());
            });
        });

        // Regex Field
        var storeRegex = function () {
            var regex_result = $('code#regex_result').text()

            if ($regStart.val().length && $regEnd.val().length) {
                $('input[name=regex_pattern]').val(regex_result)
            }
        }

        var filterRegex = function (value) {
            return value
            .replace(/\//g, '\\/')
            .replace(/\(/g, '\\(')
            .replace(/\)/g, '\\)')
            .replace(/\^/g, '\\^')
            .replace(/\+/g, '\\+')
        }

        $regStart = $('input[name=regex_start]');
        $regStart.val($regStart.val().replace('\\', ''));

        $regStart.keyup(function () {
            $('span#display_regex_start').text(filterRegex(this.value))
            setTimeout(function () {
                storeRegex();
            }, 50)
        });

        $regEnd = $('input[name=regex_end]');
        $regEnd.val($regEnd.val().replace('\\', ''));

        $regEnd.keyup(function () {
            $('span#display_regex_end').text(filterRegex(this.value))
            setTimeout(function () {
                storeRegex();
            }, 50)
        });
        
        $regStart.trigger('keyup')
        $regEnd.trigger('keyup')
        // End Regex Field


        $('button[type=submit]').click(function (e) {
            var form = $(this).closest('form')
            form.removeAttr('target')
            form.submit();
        })


        // Start Shortcode
        $('a#btn_shortcode').click(function (e) {
            e.preventDefault();

            var btn_class = $(this).attr('class')
            if (btn_class.indexOf('open') !== -1) {
                $('div#wrapper_shortcode').animate({'right': '-400px'}, 200)
                $(this).animate({'right': '0px'}, 200)
                $(this).removeClass('open')
                $(this).html('&laquo; Show Shortcode')
            } else {
                $(this).animate({'right': '400px'}, 200)
                $('div#wrapper_shortcode').animate({'right': '0px'}, 200)
                $(this).addClass('open')
                $(this).html('&raquo; Hide Shortcode')
            }
        })
        // End Shortcode
    </script>
@stop