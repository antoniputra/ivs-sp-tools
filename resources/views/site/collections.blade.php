@extends('layout')

@section('after_script')
    <script>
        $('[data-toggle="tooltip"]').tooltip()
    </script>
@stop

@section('content')

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-10">
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    <strong>Whops! There are an error</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="card">
                <h4 class="card-header">Collections [{{ $sites->total() }}]</h4>
                <div class="list-group list-group-flush">
                    <div class="list-group-item pb-4">
                        {{ Form::model(request()->all(), ['method' => 'GET']) }}
                            <div class="row justify-content-md-center">
                                {{-- <div class="col-sm-2">
                                    <label>Status:</label>
                                    {!! Form::select('status', [
                                        'all' => 'All',
                                        'verified' => 'Verified',
                                        'unverified' => 'Unverified',
                                    ], null, ['class' => 'form-control']) !!}
                                </div> --}}
                                <div class="col-sm-8">
                                    <label>Search by:</label>
                                    {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Search...']) !!}
                                </div>
                                <div class="col-sm-1">
                                    <label class="d-block">&nbsp;</label>
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    @php
                        $url = 'https://jawapos.com/nasional/politik/08/03/2019/selain-habib-rizieq-obor-rakyat-juga-wawancara-khusus-novel-baswedan';
                        $i = $sites->firstItem() -1;
                    @endphp
                    @foreach ($sites as $site)
                        <div class="list-group-item px-4">
                            <div class="row">
                                <div class="col-xs-1">
                                    {{-- <div class="d-flex align-items-center" style="height: 100%">
                                        <span class="badge badge-secondary">{{ $i += 1 }}</span>
                                    </div> --}}
                                    
                                    <div><span class="badge badge-secondary" style="font-size: 0.9rem;">{{ $i += 1 }}</span></div>
                                    @switch($site->status)
                                        @case(\App\Models\Site::STATUS_SUCCESS)
                                            <span class="badge badge-success" data-toggle="tooltip" data-placement="bottom" title="Works">√</span>
                                            @break
                                        @case(\App\Models\Site::STATUS_ERROR)
                                            <span class="badge badge-danger">error</span>
                                            @break
                                    @endswitch
                                </div>
                                <div class="col-sm">
                                    <h6 class="font-weight-bold mb-2" style="font-size: 1.1rem;">{{ $site->title }}</h6>
                                    <div>
                                        <b>Original Url:</b>
                                        <a href="{{ $site->url }}" target="_blank">{{ substr($site->url, 0, 80) }}{!! strlen($site->url) >= 80 ? '&hellip;' : '' !!}</a>
                                    </div>
                                    <div>
                                        <b>Proxy Url:</b>
                                        <a href="{{ $site->getResultUrl() }}" target="_blank">{{ substr($site->getResultUrl(), 0, 80) }}{!! strlen($site->url) >= 80 ? '&hellip;' : '' !!}</a>
                                    </div>
                                    <div class="mt-2 pt-2">
                                        {{-- @if ($site->status != \App\Models\Site::STATUS_SUCCESS)
                                            <a href="{{ route('site.updateStatus', [$site->id, 'status' => 1]) }}" class="text-success text-underline mr-3">Mark as Works</a>
                                        @else
                                            <a href="{{ route('site.updateStatus', [$site->id, 'status' => 0]) }}" class="text-secondary text-underline mr-3">Remove Works Mark</a>
                                        @endif --}}
                                        <a href="javascript:void(0)" class="text-danger text-underline font-weight-bold" onclick="if(confirm('Are you sure to delete?')) { location.href='{{ route('site.delete', $site->id) }}' }" >Delete</a>
                                        <span class="text-muted ml-5" title="{{ $site->created_at->format('M j, Y - H:i') }}">
                                            @if (str_contains($site->created_at->diffForHumans(), ['week', 'month', 'year']))
                                                {{ $site->created_at->format('M j, Y - H:i') }}
                                            @else
                                                {{ $site->created_at->diffForHumans() }}
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="col-xs-1">
                                    <a href="{{ route('site.edit', $site->id) }}" class="btn btn-sm btn-secondary btn-block">Edit</a>
                                    <a href="{{ $site->getResultUrl() }}" target="_blank" class="btn btn-sm btn-success btn-block">Go &raquo;</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                @if ($sites->lastPage() > 1)
                    <div class="card-footer">
                        {!! $sites->appends([
                            'status' => request('status'),
                            'search' => request('search'),
                        ])->links() !!}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@stop