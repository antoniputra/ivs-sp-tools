<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>SP Page</title>
    <style>
        body , html {
            padding: 0;
            margin: 0;
        }
    </style>
    <link href="https://ivxplayer-3.test/dist/app.css" rel="stylesheet">
</head>
<body>
    <h1>SP Page</h1>

    <ivs-video></ivs-video>
    <script type="text/javascript" src="https://ivxplayer-3.test/dist/vendors~sp-tools.bundle_7adcc8f4aaec05022350.chunk.js"></script>
    <script type="text/javascript" src="https://ivxplayer-3.test/dist/sp-tools.bundle.js"></script>
</body>
</html>