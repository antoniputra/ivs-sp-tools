<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SP Page - V3 Demo</title>
    <style>
        body,
        html {
            padding: 0;
            margin: 0;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

    <link href="https://v3.5kdg.com/app.sp-tools.css" rel="stylesheet">
</head>

<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <h1 class="text-center">SP Page - V3 Demo</h1>
                <div class="mt-5">
                    <ivs-video video-id="823352" api-key="38727828a6bcbae337183a66c50af04d" id="ivsplayer001"></ivs-video>
                    <hr/>
                    <ivs-video video-id="818167" api-key="38727828a6bcbae337183a66c50af04d" id="ivsplayer002"></ivs-video>
                    <hr/>
                    <ivs-video video-id="823348" api-key="38727828a6bcbae337183a66c50af04d" id="ivsplayer003"></ivs-video>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 768px; background-color: teal; width: 40px;"></div>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://v3.5kdg.com/sp-tools.bundle.js"></script>
</body>

</html>