<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dummy Page</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div class="container">
        <h1>JawaPos</h1>
        <article class="col-12 article" style="">
            <p style=""><strong>JawaPos.com</strong> - Temuan data aset warga negara Indonesia (WNI) di luar negeri senilai
                lebih dari Rp 1.300 triliun yang belum dilaporkan dalam surat pemberitahuan (SPT) harus segera ditindaklanjuti.
                Pemerintah harus mampu mengolah dan memetakan mana saja yang layak dijadikan sebagai objek pajak.
            </p>

            <p style="">Direktur Eksekutif Center for Indonesia Taxation Analysis (CITA) Yustinus Prastowo mengatakan, beberapa
                hal perlu ditelaah terkait data aset yang diperoleh dari kerja sama automatic exchange of information (AEoI)
                itu. Di antaranya, apa jenis aset keuangan tersebut, berapa tahun dimiliki wajib pajak, siapa wajib pajaknya,
                dan di mana aset disimpan.
            </p>

            <p style="">Kemudian, pemerintah harus teliti dalam melakukan profiling. "Dibandingkan datanya, apakah wajib pajak
                yang memiliki aset tersebut sudah ikut amnesti pajak atau belum. Kalau memang sudah ikut, apakah benar harta itu
                tidak ikut dideklarasikan pada saat mengikuti amnesti pajak," jelasnya kemarin (18/3). Menurut dia, penerapan
                AEoI akan membawa citra ketegasan pemerintah.
            </p>

            <p>Tahun lalu Indonesia mengirimkan data keuangan warga negara asing (WNA) ke 54 negara. Sementara itu, Indonesia
                menerima data keuangan WNI dari 66 negara. Tahun ini negara-negara yang mengikuti kerja sama AEoI bertambah.
            </p>

            <p>Menteri Keuangan Sri Mulyani Indrawati mengungkapkan, pemerintah saat ini masuk tahap mencocokkan data-data yang
                diterima dari kerja sama AEoI. "Kami belum sampai pada tahap berapa yang akan diidentifikasi," ujarnya.
            </p>

            <p>Sementara itu, hingga kemarin pagi Direktorat Jenderal Pajak (DJP) Kemenkeu telah menerima laporan SPT dari 7
                juta wajib pajak. Sebanyak 188.000 di antaranya merupakan wajib pajak badan. Sisanya adalah wajib pajak orang
                pribadi. Pelaporan tersebut meningkat 15,7 persen jika dibandingkan dengan laporan pada 18 Maret tahun lalu
                sebanyak 6,05 juta SPT.
            </p>

            <p>"Penyampaian SPT tahunan melalui e-filing lebih dari 93 persen dari total laporan SPT yang masuk," ungkap
                Direktur Penyuluhan, Pelayanan, dan Hubungan Masyarakat DJP Kemenkeu Hestu Yoga Saksama.&nbsp;</p>

            <p class="txt-fair">Editor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: Ilham Safutra
                <br> Reporter &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: (rin/wan/c10/fal)
            </p>
            <div class="article-tags">
                <span> <i class="fa fa-tags" aria-hidden="true"></i> </span>
                <a href="https://jawapos.com/tag/aset" class="tags">
                    #aset
                </a>
                <a href="https://jawapos.com/tag/objek-pajak" class="tags">
                    #objek pajak
                </a>
                <a href="https://jawapos.com/tag/kementerian-keuangan" class="tags">
                    #kementerian keuangan
                </a>
                <a href="https://jawapos.com/tag/penerimaan-pajak" class="tags">
                    #penerimaan pajak
                </a>
            </div>
        </article>
    </div>
</body>
</html>