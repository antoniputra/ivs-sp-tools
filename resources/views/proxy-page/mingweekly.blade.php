
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta charset=utf-8>

<!-- meta -->
<title>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏！ ∣ 明潮 M'INT</title>
<link rel="icon" href="/images/favicon.ico" />
<meta property="fb:app_id" content="1211563258925102"/>
<meta property="og:url" content="https://www.mingweekly.com/beauty/beautynews/content-17413.html" />
<meta property="og:site_name" content="明潮" />
<meta property="og:type" content="website" />
<meta property="og:title" content="擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏！"/>
<meta property="og:description" content="THREE 也推出超美的潤唇膏了！而起一定要親自擦上去才知道會有多美！除此之外還有一系列新色指甲油！快趁著母親節檔期通通一次擁有好了！" />
<meta property="og:image" content="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042756562869.jpg" />
<meta name="Title" content="擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏！" />
<meta name="keywords" content="THREE 唇膏,THREE 指甲油,THREE 2019" />
<meta name="description" content="THREE 也推出超美的潤唇膏了！而起一定要親自擦上去才知道會有多美！除此之外還有一系列新色指甲油！快趁著母親節檔期通通一次擁有好了！" />
<link rel="image_src" href="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042756562869.jpg" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1" />
<!--<meta name="viewport" content="user-scalable=1" />-->
<meta name="MobileOptimized" content="320" />
<meta name="HandheldFriendly" content="True" />
<meta name="format-detection" content="telephone=no" />

<!--/ meta -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WHV3SWF');</script>
<!-- End Google Tag Manager -->
<meta property="fb:pages" content="261455160552326"><!-- Pixnet RTB Start -->
<script async src="https://pixnet-network-falcon-asset.pixfs.net/js/adsbyfalcon.min.js"></script>
<!-- Pixnet End Start -->
<!--橙果 RTB STart-->
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
  	//mobile 首
  	googletag.defineSlot('/90805880/mingweekly_m_home_300x250_M', [300, 250], 'div-gpt-ad-1494321632866-0').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_m_home_300x250_T', [300, 250], 'div-gpt-ad-1494321632866-1').addService(googletag.pubads());
    //mobile 頻首
    googletag.defineSlot('/90805880/mingweekly_m_topic_300x250_T', [300, 250], 'div-gpt-ad-1494323838245-0').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_m_topic_300x250_M', [300, 250], 'div-gpt-ad-1494323838245-1').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_m_topic_300x250_B', [300, 250], 'div-gpt-ad-1494323838245-2').addService(googletag.pubads());
    //mobile 文內
    googletag.defineSlot('/90805880/mingweekly_article_300x250_foucs', [300, 250], 'div-gpt-ad-1494323838245-3').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_article_300x250_news', [300, 250], 'div-gpt-ad-1494323838245-4').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_m_article_300x250_T', [300, 250], 'div-gpt-ad-1494321743277-0').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_m_article_300x250_M', [300, 250], 'div-gpt-ad-1494321743277-1').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_m_article_300x250_B', [300, 250], 'div-gpt-ad-1494321743277-2').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_article_300x250_news', [300, 250], 'div-gpt-ad-1494321743277-3').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_article_300x250_foucs', [300, 250], 'div-gpt-ad-1494321743277-4').addService(googletag.pubads());
    //PC 首
    googletag.defineSlot('/90805880/mingweekly_PC_home_970x250_M', [970, 250], 'div-gpt-ad-1494321408727-0').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_home_970x250_T', [970, 250], 'div-gpt-ad-1494321408727-1').addService(googletag.pubads());
    //PC頻首
    googletag.defineSlot('/90805880/mingweekly_PC_topic_970x250_T', [970, 250], 'div-gpt-ad-1495529198226-0').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_topic_336x280_ML', [336, 280], 'div-gpt-ad-1495529198226-1').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_topic_336x280_MR', [336, 280], 'div-gpt-ad-1495529198226-2').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_topic_336x280_BL', [336, 280], 'div-gpt-ad-1495529198226-3').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_topic_336x280_BR', [336, 280], 'div-gpt-ad-1495529198226-4').addService(googletag.pubads());
    //PC文內
    googletag.defineSlot('/90805880/mingweekly_PC_article_300x250_BL', [300, 250], 'div-gpt-ad-1494321572943-0').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_article_300x250_BR', [300, 250], 'div-gpt-ad-1494321572943-1').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_article_300x600_right', [300, 600], 'div-gpt-ad-1494321572943-2').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_PC_article_970x250_T', [970, 250], 'div-gpt-ad-1494321572943-3').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_article_300x250_foucs', [300, 250], 'div-gpt-ad-1494321572943-4').addService(googletag.pubads());
    googletag.defineSlot('/90805880/mingweekly_article_300x250_news', [300, 250], 'div-gpt-ad-1494321572943-5').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>
<!--橙果 RTB End-->

<!-- jQuery library (served from Google) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<meta property="fb:pages" content="261455160552326" />

<!-- CSS file -->
<link rel="stylesheet" type="text/css" href="https://www.mingweekly.com/css/mint.css" />
<link rel="stylesheet" type="text/css" href="https://www.mingweekly.com/css/mint-page.css" />
<link href="https://www.mingweekly.com/css/jquery.bxslider.css" rel="stylesheet" />
<link href="https://www.mingweekly.com/css/megamenu.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://www.mingweekly.com/css/component.css" />
<link type="text/css" href="https://www.mingweekly.com/css/bottom.css" rel="stylesheet" /><!-- Article Slider -->
<link rel="stylesheet" href="https://www.mingweekly.com/css/debug.css">
<link href="https://www.mingweekly.com/assets/css/ShareButton.min.css" rel="stylesheet">

<!-- 20160523 rwd-->
<link rel="stylesheet" href="https://www.mingweekly.com/css/rwd.css">

<!--[if IE]>
	<script type="text/javascript">
		var console = { log: function() {} };
	</script>
<![endif]-->


<!-- Javascript file -->

<script type="text/javascript" src="https://www.mingweekly.com/js/jquery.SuperSlide.2.1.1.js"></script>
<script type="text/javascript">
	$(function(){
			$(".focus_body").hover(function(){$("#slideLeftBtn,#slideRightBtn").show();},function(){$("#slideLeftBtn,#slideRightBtn").hide();});
			$(".focus_body").slide({ 
				mainCell:"#slideContent",
				targetCell:"#slideContent .title",
				titCell:"#slideNum",
				prevCell:"#slideLeftBtn",
				nextCell:"#slideRightBtn",
				effect:"left",
				titOnClassName:"selected",
				autoPlay:true,
				delayTime:1000,
				easing:"easeInOutCirc",
				autoPage:"<span></span>"
			})
		})
</script>  

<script type="text/javascript" src="https://www.mingweekly.com/js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script> 
<script src="https://www.mingweekly.com/js/modernizr.custom.js"></script>


<script src="https://www.mingweekly.com/js/jquery.jcarousel.min.js"></script>
<script src="https://www.mingweekly.com/js/jquery.pikachoose.js"></script>
<script language="javascript">
      // $(document).ready(
      //   function (){
      //     $("#pikame").PikaChoose({carousel:true});
      //   });

      $(function (){
        $("#pikame").PikaChoose({
          carousel: true,
          // 第一則如果是影片，自動輪播會讓第一個播放的影片自動換到下一張，禁止這種情況，拿掉自動輪播
          autoPlay: true
        });
      });
</script>

<script type="text/javascript" src="https://www.mingweekly.com/js/jq22.js?t=20180328"></script>    


<script src="https://www.mingweekly.com/assets/js/ShareButton.min.js" async type="text/javascript"></script>

<script type="text/javascript" src="https://www.mingweekly.com/scripts/adajax.js"></script>
<!-- Facebook Pixel Code --> <script> !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n; n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0; t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js'); fbq('init', '467912576739826'); fbq('track', "PageView");</script> <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=467912576739826&ev=PageView&noscript=1" /></noscript> <!-- End Facebook Pixel Code -->

<script type="application/javascript" src="//ced.sascdn.com/tag/2060/smart.js" async></script>
<script type="application/javascript">
var sas = sas || {};
sas.cmd = sas.cmd || [];
sas.cmd.push(function() {
sas.setup({ networkid: 2060, domain: "//adnetwork.adasiaholdings.com", async: true });
});
sas.cmd.push(function() {
sas.call("onecall", {
siteId: 176765,
pageId: 1010770,
formats: [
{ id: 44269 }
],
target: ''
});
});
</script>
<!--
<script type="application/javascript" src="//ced.sascdn.com/tag/2060/smart.js" async></script>
<script type="application/javascript">
    var sas = sas || {};
    sas.cmd = sas.cmd || [];
    sas.cmd.push(function() {
        sas.setup({ networkid: 2060, domain: "//adnetwork.adasiaholdings.com", async: true });
    });
</script>
-->

{!! $options['{v3_style_tag}'] !!}
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WHV3SWF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=1211563258925102";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper">

<div id="wrapper">

<!-- AD banner 980x120-->
<div class="adbanner">
  <script language="javascript">
  //alert('10\na')
  AD_FEEL(10);
  </script>
  </div>
<!-- /AD banner 980x120-->


<!-- Header -->
<div class="area-hdr" >
 <div class="mint-logo"><a href="/"><img src="https://www.mingweekly.com/images/mint-logo.png" alt="明潮 M'INT" title="明潮 M'INT"></a></div>
 
 
  
 <!-- social media -->
<div class="soc">
  <div class="soc-fb"><a href="https://www.facebook.com/MINGWeekly" target="_blank">分享到facebook</a></div>
  <div class="soc-gpls"><a href="https://plus.google.com/117701555212752352391/" target="_blank">G+分享</a></div>
<!--  <div class="soc-twr"><a href="https://twitter.com/" target="_blank">twitter</a></div>
-->  <div class="soc-ytb"><a href="https://www.youtube.com/mingtv" target="_blank">youtube</a></div>
  <div class="soc-ig"><a href="https://www.instagram.com/mingweekly/" target="_blank">instagram</a></div>
</div>

 <!-- /social media -->
 
 <!-- search -->
 <div class="column">
          <div id="sb-search" class="sb-search">
						<form action="/search/" method="get" target="_blank">
              <input type="hidden" name="cx" value="002091278206172957848:yaufrw27ucu" />
              <input type="hidden" name="cof" value="FORID:10" />
              <input type="hidden" name="ie" value="utf8" />
							<input class="sb-search-input" placeholder="Enter your search ..." type="text" id="SearchWord" name="q" required>
							<input class="sb-search-submit" type="submit">
							<span class="sb-icon-search"></span>
						</form>
					</div>
				</div>
                <script src="https://www.mingweekly.com/js/classie.js"></script>
 <!-- /search -->
 
 <!-- mint nav-->
  <div class="mint-nav">
	<ul class="megamenu Mwhite">
		
		
        
        <li>
         <a href="/fashion/"><span class="mgre">F</span><span class="en">ashion</span><span class="chinese">流行時尚</span></a>
         <ul class="dropdown">
			<li><a href="/fashion/fashionnews/">時尚快遞</a></li>
			<li><a href="/fashion/streetsnap/">潮流街拍</a></li>
			<li><a href="/fashion/trend/">流行觀測站</a></li>
           <li><a href="/fashion/fashionshow/">時裝周直擊</a></li>
<!-- 			<li><a href="/fashion/hotitems/">戀物</a></li>
-->            <li><a href="/fashion/redcarpet/">紅毯直擊</a></li>
		 </ul>
		</li>
        
        <li class="active">
         <a href="/beauty/"><span class="mgre">B</span><span class="en">eauty</span><span class="chinese">彩妝保養</span></a>
         <ul class="dropdown">
			<li><a href="/beauty/beautynews/">美妝速報</a></li>
			<li><a href="/beauty/beautytips/">美妝密技</a></li>
			<li><a href="/beauty/skincare/">保養心肌</a></li>
			<li><a href="/beauty/hair/">香氛美髮</a></li>
            <li><a href="/beauty/body/">健身纖體</a></li>
			<li><a href="/beauty/beautysecret/">名人美心肌</a></li>
		 </ul>
		</li>
        
        <li>
         <a href="/entertainment/" ><span class="mgre">E</span><span class="en">ntertainment</span><span class="chinese">名人娛樂</span></a>
         <ul class="dropdown">
			<li><a href="/entertainment/tvshow/">看好戲</a></li>
			<li><a href="/entertainment/movie/">找電影</a></li>
			<li><a href="/entertainment/music/">聽音樂</a></li>
            <li><a href="/entertainment/entertainmentnews/">星鮮事</a></li>
			<li><a href="/entertainment/celebrity/">外星聞</a></li>
		 </ul>
		</li>
        
        <li>
         <a href="/luxury/"><span class="mgre">L</span><span class="en">uxury</span><span class="chinese">奢逸品味</span></a>
         <ul class="dropdown">
			<li><a href="/luxury/jewelandwatch/">珠寶盒</a></li>
			<li><a href="/luxury/watch/">玩錶誌</a></li>
			<li><a href="/luxury/taste/">品味玩家</a></li>
			<li><a href="/luxury/wine/">微醺時光</a></li>
		 </ul>
		</li>
        
        <li>
         <a href="/lifestyle/"><span class="mgre">L</span><span class="en">ifestyle</span><span class="chinese">風格生活</span></a>
         <ul class="dropdown">
			<li><a href="/lifestyle/news/">生活新鮮事</a></li>
			<li><a href="/lifestyle/gastronomy/">食尚旅遊</a></li>
			<!--<li><a href="/lifestyle/travel/">趣旅行</a></li>
            <li><a href="/lifestyle/whatshot/">潮地方</a></li>-->
			<li><a href="/lifestyle/design/">風格設計</a></li>
            <!--<li><a href="/lifestyle/focus/">國際焦點</a></li>-->
            <li><a href="/lifestyle/astrology/">星座塔羅</a></li>
            <!--<li><a href="/lifestyle/creativity/">文化創意</a></li>-->
		 </ul>
		</li>
        
        <li>
         <a href="/culture/"><span class="mgre">C</span><span class="en">ulture</span><span class="chinese">藝文焦點</span></a>
         <ul class="dropdown">
			<li><a href="/culture/issue/">焦點特企</a></li>
			<li><a href="/culture/people/">明潮人物</a></li>
			<li><a href="/culture/art/">藝文空間</a></li>
			<li><a href="/culture/book/">迷閱讀</a></li>
			<li><a href="/culture/archiandinterior/">達人觀點</a></li>
		 </ul>
		</li>
        
        <li>
         <a href="/live/"><span class="mgre">M</span><span class="en">int TV</span><span class="chinese">影音專區</span></a>
          <!--<ul class="dropdown">
			<li><a href="#"></a></li>
		 </ul>-->
		</li>

        <!-- <li>
         <a href="/column/"><span class="mgre">C</span><span class="en">olumn</span><span class="chinese">明潮觀點</span></a>
         <ul class="dropdown">
			<li><a href="#"></a></li>
		 </ul>
		</li>
        
        <li class="r-none">
         <a href="/magazine/"><span class="mgre">M</span><span class="en">agazine</span><span class="chinese">明潮雙周</span></a>
          <ul class="dropdown">
			<li><a href="#"></a></li>
		 </ul>
		</li>-->

	</ul>
</div>
<div style="text-align:center;clear:both">

</div> 
 <!-- /mint nav -->

</div>
<div id = 'oneadICIPMICTag'></div>
<script type="text/javascript" src="/scripts/onead_ic_ip_mic.js"></script>
<!-- /Header -->

<!--ad asia-->

<!--PC-->
<!--<script type="application/javascript" src="https://adnetwork.adasiaholdings.com/ac?
out=js&nwid=2060&siteid=176765&pgname=tw_mingweekly_outstream_pc&fmtid=44269&tgt=
[sas_target]&visit=m&tmstp=[timestamp]&clcturl=[countgo]"></script>
-->


<!--M-->
<!--<script type="application/javascript" src="https://adnetwork.adasiaholdings.com/ac?
out=js&nwid=2060&siteid=176765&pgname=tw_mingweekly_outstream_mb&fmtid=44269&tgt=
[sas_target]&visit=m&tmstp=[timestamp]&clcturl=[countgo]"></script>-->


<!--end ad asia-->

<!-- main content ---------------------------------->
<div class="content-wrp">
<div align="center" id="ad2434">

  <script language="javascript">
  //alert('195\na')
  AD_FEEL(195);
  </script>
  <!-- [Mingweekly] Desktop-Article Top_970x250 -->
<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="101984" style="display:inline-block;width:970px;height:250px"></ins>
</div>

 <div class="page-article">
 
   <div class="article-hdr">
    <div class="article-tt">
     <span>美妝速報</span><div class="art-date">Apr 27 , 2019</div><div class="art-time">00:00</div>
     <h1>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏！</h1>
     <div class="art-edit">文／sophialiang　　來源／THREE</div>
    </div>
    
    <div class="article-fb"> <div class="page_share facebook">
      <div class="fb-like" data-href="http://www.mingweekly.com/beauty/beautynews/content-17413.html" data-width="55px" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
     </div></div>
   </div>
   
   <!--article slider-->
   
        <div class="article-slider"><div class="pikachoose"><ul id="pikame" class="jcarousel-skin-pika" ><li><a href="lightbox-17413-125051.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042756562869.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏！</span></li><li><a href="lightbox-17413-125043.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754283897.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125044.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754290181.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125045.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754296197.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125046.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754302137.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125047.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754307957.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125048.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754315413.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125049.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754321345.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li><li><a href="lightbox-17413-125050.html"><img data-youtube="" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754328197.jpg"/></a><span>擦出來的顏色絕對比你看到美10倍！THREE 限量 魅光透漾潤唇膏 三色都好值得收藏～</span></li></ul></div></div>
   <!--/article slider-->
   <script>
   

      $(function() {
        $('.pikachoose .clip').on('click', function() {
          window._atrk_fired = false;
          ga('tracker1.send', 'pageview', '/beauty/beautynews/content-17413.html#slide');
          window.atrk();
          readloadframe();
          
          
            //reloadrtb();
            
        });
      });
    </script>
  
   <h2>THREE 也推出超美的潤唇膏了！而起一定要親自擦上去才知道會有多美！除此之外還有一系列新色指甲油！快趁著母親節檔期通通一次擁有好了！</h2>
   <hr>
   

    <!--/article cnt-->
    
          <!-- Start AD Asia-->
         
<div id="sas_44269"></div>
<script type="application/javascript">
   sas.cmd.push(function() {
       sas.render("44269");  // Format : VideoRead 1x1
   });
</script>


<!-- End AD Asia-->
                
            <div align="center"><!-- [Mingweekly] Desktop-Article Center_336x280 -->
<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="103124" style="display:inline-block;width:336px;height:280px"></ins>
</div>
                
    <p><a href="lightbox-17413-125049.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754321345.jpg" alt=""></a></p>
    <h3></h3>
    <p>THREE推出有霓光感的潤色唇膏！很多女孩其實會害怕唇膏有『霓光』這兩個字！但TＨREE新推出的限量三色『魅光透樣潤唇膏』擦起來絕對不是你想的那樣！</p>

    <p><a href="lightbox-17413-125050.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754328197.jpg" alt=""></a></p>
    <h3></h3>
    <p>『魅光透漾潤唇膏』 擦在雙唇上所呈現出得光澤就是彩虹光！ 有透明感卻能分辨出的七種不同的色彩！因此就是，<u>放晴的天空。大大的彩虹。耀眼的光暈。</u>這種感覺！</p>

    <h3></h3>
    <p>THREE 魅光魅光透樣潤唇膏中有<u>透明感與顯色度同時並存的〝主調色〞</u>，讓唇彩不覆蓋原有唇膏色澤卻能夠呈現出不同的印象，宛如在唇上反射出彩虹霓光，搭配<u>〝變色虹霓珠光〞使雙唇能因不同的光源變化而創造不同的光澤質感</u>，最後利用<u>9種植物油．油脂．植物精華成分來保養雙唇肌膚！</u></p>

    <p><a href="lightbox-17413-125046.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754302137.jpg" alt=""></a></p>
    <h3></h3>
    <p>THREE魅光透漾潤唇膏 共推出限量三色 ：<br />
<u>X01 RAINBOW WARRIOR</u>，宛如天使般的純粹，略帶藍調的粉紅色，展現甜美可人氣質。👇</p>

    <p><a href="lightbox-17413-125043.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754283897.jpg" alt=""></a></p>
    <h3></h3>
    <p>&nbsp;</p>

<p><u>X02 SUNKISSABLE，</u>親吻陽光嶄露健康活力的維他命色，覆上一層橙黃色薄紗般豐郁。👇</p>

    <p><a href="lightbox-17413-125044.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754290181.jpg" alt=""></a></p>
    <h3></h3>
    <p>&nbsp;</p>

<p><u>X03 FLOWER OF ROMANCE，</u>緩緩綻放的花朵，緩緩增強的紅潤。完美調和黑色調及堇紫色勾勒出魅惑魔法。👇</p>

    <p><a href="lightbox-17413-125045.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754296197.jpg" alt=""></a></p>
    <h3></h3>
    <p>THREE魅光透漾潤唇膏，限量3色，各&nbsp;NT1,40。（5/1上市)&nbsp;</p>

    <h3></h3>
    <p>除了潤唇膏外，還同步推出12色指甲油，THREE的指甲油粉千萬不要錯過唷～<br />
&nbsp;</p>

    <p><a href="lightbox-17413-125048.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm640480_images_A1/17413/2019042754315413.jpg" alt=""></a></p>
    <h3></h3>
    <p><strong>THREE </strong><strong>魅光指彩，</strong>限量發售 全12色，各&nbsp;NT600。</p>

<p>&nbsp;</p>


<!--第三方影音廣告 Start-->
<!--inc/ad_MJBWZM2 20180612移除-->
<!--第三方影音廣告End-->



<!-- IR & MIR 廣告插入點(開始) -->
<!-- IR & MIR 廣告插入點(結束) -->




    <div class="page">
      <div class="mnp">
      
      </div>
    </div>
    
    <!--article cnt-->
    <div align="center" id="ad2536">
   
  <script language="javascript">
  //alert('196\na')
  AD_FEEL(196);
  </script>
  <!-- [Mingweekly] Desktop-Article End Left_336x280 -->
<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="101909" style="display:inline-block;width:336px;height:280px"></ins>
<script async src="https://pixnet-network-falcon-asset.pixfs.net/js/adsbyfalcon.min.js"></script>
<br><br>

<!-- [Mingweekly] Desktop-Article End Right_336x280 -->
<!--<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="101969" style="display:inline-block;width:336px;height:280px"></ins>-->
<script async src="https://pixnet-network-falcon-asset.pixfs.net/js/adsbyfalcon.min.js"></script>


<!--第三方影音廣告 Start-->

  <!-- container div can be styled in anyway required -->
<div style="width:100%">
 {{-- <script id="ivsn" src="https://player.ivideosmart.com/ivideosense/player/js/ivsnload_v1.js?key=x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K&wid=6dcce4ba-5814"></script> --}}
 <ivs-video video-id="801253" api-key="eca5f1d021abc134cb2e100b669c5e9a" id="ivsplayer001"></ivs-video>
</div>
  

<!--第三方影音廣告End-->
    </div>
   <!--article share-->
   <div class="article-share">
    <div class="share-fb"><a href="#" onClick="sharebuttonPopup('https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww%2Emingweekly%2Ecom%2Fbeauty%2Fbeautynews%2Fcontent%2D17413%2Ehtml%3Ft%3D802608',600,620);return false;">分享到facebook</a></div>
    <div class="share-line"><a href="http://line.me/R/msg/text/?%E6%93%A6%E5%87%BA%E4%BE%86%E7%9A%84%E9%A1%8F%E8%89%B2%E7%B5%95%E5%B0%8D%E6%AF%94%E4%BD%A0%E7%9C%8B%E5%88%B0%E7%BE%8E10%E5%80%8D%EF%BC%81THREE+%E9%99%90%E9%87%8F+%E9%AD%85%E5%85%89%E9%80%8F%E6%BC%BE%E6%BD%A4%E5%94%87%E8%86%8F+%E4%B8%89%E8%89%B2%E9%83%BD%E5%A5%BD%E5%80%BC%E5%BE%97%E6%94%B6%E8%97%8F%EF%BC%81%0D%0Ahttps%3A%2F%2Fwww%2Emingweekly%2Ecom%2Fbeauty%2Fbeautynews%2Fcontent%2D17413%2Ehtml%3Ft%3D802608" target="_blank">分享到Line</a></div>
    <div class="share-gpls"><a href="#" onClick="sharebuttonPopup('https://plus.google.com/share?url=https%3A%2F%2Fwww%2Emingweekly%2Ecom%2Fbeauty%2Fbeautynews%2Fcontent%2D17413%2Ehtml%3Ft%3D802608',500,680);return false;">分享到G+</a></div>
    <div class="share-twr"><a href="#" onClick="sharebuttonPopup('https://twitter.com/intent/tweet?text=&original_referer=https%3A%2F%2Fwww%2Emingweekly%2Ecom%2Fbeauty%2Fbeautynews%2Fcontent%2D17413%2Ehtml%3Ft%3D802608&url=https%3A%2F%2Fwww%2Emingweekly%2Ecom%2Fbeauty%2Fbeautynews%2Fcontent%2D17413%2Ehtml%3Ft%3D802608',560,480);return false;">分享到Twitter</a></div>
    <div class="share-ig"><a href="https://www.instagram.com/mingweekly/" target="_blank">分享到instagram</a></div>
   </div>
   <!--/article share-->
<div style="text-align: center; border: 3px solid black;">你可能也會喜歡</div>
<div id="_popIn_recommend"></div>
<script type="text/javascript">
    (function() {
        var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.charset = "utf-8"; pa.async = true;
        pa.src = "https://api.popin.cc/searchbox/mingweekly.js";
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
    })(); 
</script>
   
<div style="text-align: center; border: 3px solid black;">延伸閱讀</div>

   <!-- article more-->
          <div class="artmore">
           <ul>
      <li><a href="/beauty/beautynews/content-16137.html"><img alt="「綿密的滋潤感，用在雙眼真的好水潤」冬天就要這樣質地的眼霜，用過POLA肌密全能眼霜後的雙眼，真的太幸福了~~" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm428428_images_A1/16137/2018111671647489.jpg"><p>「綿密的滋潤感，用在雙眼真的好水潤」冬天就要這樣質地的眼霜，用過POLA肌密全能眼霜後的雙眼，真的太幸福了~~</p></a></li><li><a href="/beauty/beautynews/content-16180.html"><img alt="DHC純欖護唇膏星際大戰限定版，拿在手裡真的就像迷你光劍，買來與男友一起打擊乾燥剛剛好啊" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm428428_images_A1/16180/2018112278537225.jpg"><p>DHC純欖護唇膏星際大戰限定版，拿在手裡真的就像迷你光劍，買來與男友一起打擊乾燥剛剛好啊</p></a></li><li><a href="/beauty/beautynews/content-16347.html"><img alt="10款英國皇室夢幻沐浴油一次擁有！Aromatherapy Associate 2018 聖誕禮盒 比去年更低價，不買對不起聖誕老公公！" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm428428_images_A1/16347/2018121278301489.jpg"><p>10款英國皇室夢幻沐浴油一次擁有！Aromatherapy Associate 2018 聖誕禮盒 比去年更低價，不買對不起聖誕老公公！</p></a></li><li><a href="/beauty/beautynews/content-16309.html"><img alt="櫻花妹正夯的指甲底油！日本文青系指甲油uka推指甲底油，拯救受傷指甲重回健康美麗！" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm428428_images_A1/16309/2018120769522593.jpg"><p>櫻花妹正夯的指甲底油！日本文青系指甲油uka推指甲底油，拯救受傷指甲重回健康美麗！</p></a></li><li><a href="/beauty/beautynews/content-16716.html"><img alt="有益生菌的粉底液！嬌蘭「亮顏裸光純萃粉底液」的成份和效果讓人跌破眼鏡掉下巴！還有那個粉底刷也太犯規！" src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm428428_images_A1/16716/2019012902819097.jpg"><p>有益生菌的粉底液！嬌蘭「亮顏裸光純萃粉底液」的成份和效果讓人跌破眼鏡掉下巴！還有那個粉底刷也太犯規！</p></a></li><li><a href="/beauty/beautynews/content-16707.html"><img alt="一秒發射超模光！NARS 「超模珍珠光澤乳」打造亞洲人專屬的光澤肌！   " src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm428428_images_A1/16707/2019012780963993.JPG"><p>一秒發射超模光！NARS 「超模珍珠光澤乳」打造亞洲人專屬的光澤肌！   </p></a></li>
       </ul>
          </div>
          <!-- /article more-->



   <!--article tag-->
   <div class="art-tag">
    <ul>
     <li><a href="/keyword/keyword-THREE 唇膏-49391.html" target="_blank">THREE 唇膏</a></li><li><a href="/keyword/keyword-THREE 潤唇-49392.html" target="_blank">THREE 潤唇</a></li>
    </ul>
   </div>
   <!--/article tag-->
   
<div align="center">

</div>
   <div class="fb_comments">
    <div class="fb-comments" data-href="https://www.mingweekly.com/beauty/beautynews/content-17413.html" data-width="870px" data-numposts="5" data-colorscheme="light"></div>
   </div>
   
   
 </div>
 
 
 
<!--Right 300-->
<div class="cnt-R">

          <!-- IR & MIR 廣告插入點(開始) -->
          <!--<div id = 'oneadIRMIRTag'></div>-->
          <!--<script type="text/javascript" src="/scripts/onead_ir_mir.js"></script>-->
          <!-- IR & MIR 廣告插入點(結束) -->

  <!-- AD banner 300x600-->
  <div class="ad300">
  <script language="javascript">
  //alert('1\na')
  AD_FEEL(1);
  </script>
  </div>
  <!-- /AD banner 300x600-->

  <div align="center" id="ad2937">
  
  <script language="javascript">
  //alert('200\na')
  AD_FEEL(200);
  </script>
  <!-- [Mingweekly] Desktop-Article Top Sidebar_300x600 -->
<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="101903" style="display:inline-block;width:300px;height:600px"></ins>
</div>




  
  <!-- News -->
   
   <div class="right-line"><div class="right-tt">潮最新</div></div>
   <div class="mintnew">
    
    <div class="news">
     <div class="news-L"></div>
     <div class="news-R"><a href="/beauty/beautynews/content-17413.html">擦出來的顏色絕對比你看到美10倍！T...</a></div>
    </div>
    
    <div class="news">
     <div class="news-L"></div>
     <div class="news-R"><a href="/beauty/beautynews/content-17395.html">先顯色、再封色！香奈兒「超炫耀持色唇...</a></div>
    </div>
    
    <div class="news">
     <div class="news-L"></div>
     <div class="news-R"><a href="/beauty/beautynews/content-17393.html">「推開後，是超美的水光肌…」香奈兒這...</a></div>
    </div>
    
    <div class="news">
     <div class="news-L"></div>
     <div class="news-R"><a href="/beauty/beautynews/content-17365.html">RMK 無雷色『經典輕潤口紅』 驚夏...</a></div>
    </div>
    
    <div class="news">
     <div class="news-L"></div>
     <div class="news-R"><a href="/beauty/beautynews/content-17360.html">粉紅唇恐懼症有解！黃皮膚擦也超美的粉...</a></div>
    </div>
    
   </div>
   
  <!-- /News -->
   <!-- FB hot -->
  <div class="right-line"><div class="right-tt">潮影音</div></div>
  <div class="ytb-video">
    
    <div class="ytb300"><a href="https://www.mingweekly.com/live/content-17138.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm300169_images_A1/17138/2019032270929281.jpg"></a></div>
    
    <div class="ytb300"><a href="https://www.mingweekly.com/live/content-17137.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm300169_images_A1/17137/2019032270709809.jpg"></a></div>
    
    <div class="ytb300"><a href="https://www.mingweekly.com/live/content-17056.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm300169_images_A1/17056/2019031467635265.jpg"></a></div>
    
  </div>
  <!--/ FB hot -->
  
<div align="center" id="ad3038">

  <script language="javascript">
  //alert('201\na')
  AD_FEEL(201);
  </script>
  <!-- [Mingweekly] Desktop-Article Middle Sidebar_300x250 -->
<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="101978" style="display:inline-block;width:300px;height:250px"></ins>
</div>

  <!-- /Product slider-->
  
  
  <!-- FB hot -->
  <div class="right-line"><div class="right-tt">潮關注</div></div>
  
  
  <div class="fb-hot">
  
  <script language="javascript">
  //alert('23\na')
  AD_FEEL(23);
  </script>
  
  </div>
  
  <div class="fb-hot">
  <a href="/beauty/beautynews/content-8356.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm300225_images_A1/8356/2016082878899857.jpg" alt=""></a>
  <p><a href="/beauty/beautynews/content-8356.html">就是要紅！5款大勢紅色系眼彩盤</a></p>
  </div>
  
  <div class="fb-hot">
  <a href="/beauty/beautynews/content-10138.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm300225_images_A1/10138/2017021067373145.JPG" alt=""></a>
  <p><a href="/beauty/beautynews/content-10138.html"> 呼叫刷具控！快帶這款「花瓣粉底刷」回家！ 底妝零刷痕！輕裸光膚質彷彿是天生的！</a></p>
  </div>
  
  <div class="fb-hot">
  <a href="/beauty/beautynews/content-13742.html"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm300225_images_A1/13742/2018030778989777.jpg" alt=""></a>
  <p><a href="/beauty/beautynews/content-13742.html">蘭芝小熊晚安膠囊 幫你趁睡覺時偷偷把肌膚變白 </a></p>
  </div>
  
  <!--/ FB hot -->
  
<div align="center" id="ad31">

  <script language="javascript">
  //alert('202\na')
  AD_FEEL(202);
  </script>
  <!-- [Mingweekly] Desktop-Article End Sidebar_300x600 -->
<ins class="adsbyfalcon" data-ad-client="100022" data-ad-slot="101981" style="display:inline-block;width:300px;height:600px"></ins>
</div>

  <!-- AD banner 300x250-->
  <div class="ad300-2">
  <script language="javascript">
  //alert('2\na')
  AD_FEEL(2);
  </script>
  </div>
  <!-- /AD banner 300x250-->
  
  <div data-href="https://www.facebook.com/MINGWeekly/" data-tabs="timeline" data-width="300" data-height="600" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" class="fb-page">
                <blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
              </div>

 </div>
<!--Right 300--> 
 
  
 <script async src="https://pixnet-network-falcon-asset.pixfs.net/js/adsbyfalcon.min.js"></script>

</div>
<!-- /main content -->

<!-- IR & MIR 廣告插入點(開始) -->
<div id = 'oneadIRMIRTag'></div>
<script type="text/javascript" src="https://www.mingweekly.com/scripts/onead_ir_mir.js"></script>
<!-- IR & MIR 廣告插入點(結束) -->

<!-- footer -->
<div class="footer-wrp">
 <div class="footer">
 
 <!--logo + social + copy -->
 <div class="ft-logo"> 
  <a href="/"><img src="https://www.mingweekly.com/images/mint-logo-s.png" alt="明潮 M'INT" title="明潮 M'INT"></a>
  
  <div class="ft-social -footer">
    <!-- social media -->
 
    <div class="soc-fb"><a href="https://www.facebook.com/MINGWeekly" target="_blank">分享到facebook</a></div>
  <div class="soc-gpls"><a href="https://plus.google.com/117701555212752352391/" target="_blank">G+分享</a></div>
<!--  <div class="soc-twr"><a href="https://twitter.com/" target="_blank">twitter</a></div>
-->  <div class="soc-ytb"><a href="https://www.youtube.com/mingtv" target="_blank">youtube</a></div>
  <div class="soc-ig"><a href="https://www.instagram.com/mingweekly/" target="_blank">instagram</a></div>


 <!-- /social media -->
 
 <!-- search -->
 <div class="column">
					<div id="sb-search-footer" class="sb-search">
						<form action="/search/" method="get" target="_blank">
							<input type="hidden" name="cx" value="002091278206172957848:yaufrw27ucu" />
							<input type="hidden" name="cof" value="FORID:10" />
							<input type="hidden" name="ie" value="utf8" />
							<input class="sb-search-input" placeholder="Enter your search ..." type="text" name="q" id="search" required>
							<input class="sb-search-submit" type="submit">
							<span class="sb-icon-search"></span>
						</form>
					</div>
				</div>
                
 <!-- /search -->
  </div>
  
  <div class="copyright">© 2019 Mingweekly All Rights Reserved. <br>明周國際岀版有限公司版權所有</div>
 </div>
 <!--/logo + social + copy -->
 
 <!--channel -->
 <div class="ft-list-wrp">
 
  
  
  <!-- Fashion -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/fashion/">Fashion</a></li>
    <li><a href="/fashion/fashionnews/">時尚快遞</a></li>
      <li><a href="/fashion/streetsnap/">潮流街拍</a></li>
      <li><a href="/fashion/trend/">流行觀測站</a></li>
           <li><a href="/fashion/fashionshow/">時裝周直擊</a></li>
<!--      <li><a href="/fashion/hotitems/">戀物</a></li>
-->            <li><a href="/fashion/redcarpet/">紅毯直擊</a></li>
   </ul>
  </div>
  
  <!-- Beauty -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/beauty/">Beauty</a></li>
    <li><a href="/beauty/beautynews/">美妝速報</a></li>
      <li><a href="/beauty/beautytips/">美妝密技</a></li>
      <li><a href="/beauty/skincare/">保養心肌</a></li>
      <li><a href="/beauty/hair/">香氛美髮</a></li>
            <li><a href="/beauty/body/">健身纖體</a></li>
      <li><a href="/beauty/beautysecret/">名人美心肌</a></li>
   </ul>
  </div>
  <!-- Entertainment -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/entertainment/">Entertainment</a></li>
    <li><a href="/entertainment/tvshow/">看好戲</a></li>
      <li><a href="/entertainment/movie/">找電影</a></li>
      <li><a href="/entertainment/music/">聽音樂</a></li>
            <li><a href="/entertainment/entertainmentnews/">星鮮事</a></li>
      <li><a href="/entertainment/celebrity/">外星聞</a></li>
   </ul>
  </div>
  <!-- Luxury -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/luxury/">Luxury</a></li>
    <li><a href="/luxury/jewelandwatch/">珠寶盒</a></li>
      <li><a href="/luxury/watch/">玩錶誌</a></li>
      <li><a href="/luxury/taste/">品味玩家</a></li>
      <li><a href="/luxury/wine/">微醺時光</a></li>
   </ul>
  </div>
  
  <!-- Lifestyle -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/lifestyle/">Lifestyle</a></li>
    <li><a href="/lifestyle/news/">生活新鮮事</a></li>
      <li><a href="/lifestyle/gastronomy/">食尚旅遊</a></li>
      <!--<li><a href="/lifestyle/travel/">趣旅行</a></li>
            <li><a href="/lifestyle/whatshot/">潮地方</a></li>-->
      <li><a href="/lifestyle/design/">風格設計</a></li>
            <!--<li><a href="/lifestyle/focus/">國際焦點</a></li>-->
            <li><a href="/lifestyle/astrology/">星座塔羅</a></li>
            <!--<li><a href="/lifestyle/creativity/">文化創意</a></li>-->
   </ul>
  </div>
  
  <!-- Culture -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/culture/">Culture</a></li>
    <li><a href="/culture/issue/">焦點特企</a></li>
      <li><a href="/culture/people/">明潮人物</a></li>
      <li><a href="/culture/art/">藝文空間</a></li>
      <li><a href="/culture/book/">迷閱讀</a></li>
      <li><a href="/culture/archiandinterior/">達人觀點</a></li>
   </ul>
  </div>
  
  <!-- Column + Magazine -->
  <div class="ft-list">
   <ul>
    <li class="mainlist"><a href="/live/">Mint TV</a></li>
    <!--<li class="mainlist"><a href="/magazine/">Magazine</a></li>-->
   </ul>
  </div>
  
  <!-- About -->
  <!--<div class="ft-list">
   <ul>
    <li class="mainlist"><a href="#">關於明潮</a></li>
    <li class="mainlist"><a href="#">服務條款</a></li>
    <li class="mainlist"><a href="#">聯絡我們</a></li>
   </ul>
  </div>-->
  
 </div>
 <!--channel -->
 
 <!--cover -->
 <div class="ft-cover"><a href="/magazine/"><img src="https://dw6vrgax4fzym.cloudfront.net/userfiles/sm/sm250332_images_MZ1/2017050448479762.jpg" alt=""></a></div>
 <!--cover -->
 
 </div>
 <script type="text/javascript">
/*	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-40463731-1']);
	_gaq.push(['_trackPageview']);
	setTimeout("_gaq.push(['_trackEvent', '15_seconds', 'read'])", 15000);
	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();*/
</script>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-40463731-1', 'auto', 'tracker1');
	//ga('create', 'UA-XXXXX-Z', 'auto', 'tracker2');

	
	ga('tracker1.set', 'userId', '6568688434');

	ga('tracker1.send', 'pageview');
	setTimeout("ga('tracker1.send', 'event', '15_seconds', 'read', '')", 15000);

</script>


<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"S/jNm1a4KM+2uP", domain:"mingweekly.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=S/jNm1a4KM+2uP" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->


<!--DEJ tracker begin-->
    <script type="text/javascript" src="https://collect.prod.ai-analytics.biz/statics/co.js"></script>
<!--DEJ tracker end-->




 <iframe frameborder="0" scrolling="no" id="dadframe" width="1" height="1" style="display:inline;margin: 0;padding: 0; " src="https://www.mingweekly.com/js/ad.html?t=201711161239"></iframe>
  
 	
</div>
</div>

<!-- overlayer -->
<div id="over_layer">
<div class="logo-xs"><a href="/"><img src="https://www.mingweekly.com/images/mint-logo-xs.png" alt="明潮" title="明潮"></a></div>
<!-- mint nav-->
 <div class="mint-nav" style="margin-top:0px;  ">
 
 
	<ul class="megamenu Mwhite">
		
		
        
        <li>
         <a href="/fashion/"><span class="mgre">F</span><span class="en">ashion</span><span class="chinese">流行時尚</span></a>
         <ul class="dropdown">
			<li><a href="/fashion/fashionnews/">時尚快遞</a></li>
			<li><a href="/fashion/streetsnap/">潮流街拍</a></li>
			<li><a href="/fashion/trend/">流行觀測站</a></li>
           <li><a href="/fashion/fashionshow/">時裝周直擊</a></li>
<!-- 			<li><a href="/fashion/hotitems/">戀物</a></li>
-->            <li><a href="/fashion/redcarpet/">紅毯直擊</a></li>
		 </ul>
		</li>
        
        <li class="active">
         <a href="/beauty/"><span class="mgre">B</span><span class="en">eauty</span><span class="chinese">彩妝保養</span></a>
         <ul class="dropdown">
			<li><a href="/beauty/beautynews/">美妝速報</a></li>
			<li><a href="/beauty/beautytips/">美妝密技</a></li>
			<li><a href="/beauty/skincare/">保養心肌</a></li>
			<li><a href="/beauty/hair/">香氛美髮</a></li>
            <li><a href="/beauty/body/">健身纖體</a></li>
			<li><a href="/beauty/beautysecret/">名人美心肌</a></li>
		 </ul>
		</li>
        <li>
         <a href="/entertainment/" ><span class="mgre">E</span><span class="en">ntertainment</span><span class="chinese">名人娛樂</span></a>
         <ul class="dropdown">
			<li><a href="/entertainment/tvshow/">看好戲</a></li>
			<li><a href="/entertainment/movie/">找電影</a></li>
			<li><a href="/entertainment/music/">聽音樂</a></li>
            <li><a href="/entertainment/entertainmentnews/">星鮮事</a></li>
			<li><a href="/entertainment/celebrity/">外星聞</a></li>
		 </ul>
		</li>
        <li>
         <a href="/luxury/"><span class="mgre">L</span><span class="en">uxury</span><span class="chinese">奢逸品味</span></a>
         <ul class="dropdown">
			<li><a href="/luxury/jewelandwatch/">鐘錶珠寶</a></li>
			<li><a href="/luxury/taste/">品味玩家</a></li>
			<li><a href="/luxury/wine/">微醺時光</a></li>
		 </ul>
		</li>
        
        <li>
         <a href="/lifestyle/"><span class="mgre">L</span><span class="en">ifestyle</span><span class="chinese">風格生活</span></a>
         <ul class="dropdown">
			<li><a href="/lifestyle/news/">生活新鮮事</a></li>
			<li><a href="/lifestyle/gastronomy/">食尚旅遊</a></li>
			<!--<li><a href="/lifestyle/travel/">趣旅行</a></li>
            <li><a href="/lifestyle/whatshot/">潮地方</a></li>-->
			<li><a href="/lifestyle/design/">風格設計</a></li>
            <!--<li><a href="/lifestyle/focus/">國際焦點</a></li>-->
            <li><a href="/lifestyle/astrology/">星座塔羅</a></li>
            <!--<li><a href="/lifestyle/creativity/">文化創意</a></li>-->
		 </ul>
		</li>
        
        <li>
         <a href="/culture/"><span class="mgre">C</span><span class="en">ulture</span><span class="chinese">藝文焦點</span></a>
         <ul class="dropdown">
			<li><a href="/culture/issue/">焦點特企</a></li>
			<li><a href="/culture/people/">明潮人物</a></li>
			<li><a href="/culture/art/">藝文空間</a></li>
			<li><a href="/culture/book/">迷閱讀</a></li>
			<li><a href="/culture/archiandinterior/">達人觀點</a></li>
		 </ul>
		</li>
        
        <li>
         <a href="/live/"><span class="mgre">M</span><span class="en">int TV</span><span class="chinese">影音專區</span></a>
          <!--<ul class="dropdown">
			<li><a href="#"></a></li>
		 </ul>-->
		</li>

        <!-- <li>
         <a href="/column/"><span class="mgre">C</span><span class="en">olumn</span><span class="chinese">明潮觀點</span></a>
         <ul class="dropdown">
			<li><a href="#"></a></li>
		 </ul>
		</li>
        
        <li class="r-none">
         <a href="/magazine/"><span class="mgre">M</span><span class="en">agazine</span><span class="chinese">明潮雙周</span></a>
          <ul class="dropdown">
			<li><a href="#"></a></li>
		 </ul>
		</li>-->

	</ul>
</div>
<div style="text-align:center;clear:both">

</div> 
 <!-- /mint nav -->
 
</div><!-- /overlayer --> 

<script>
$(function(){
	$(window).load(function(){
		$(window).bind('scroll resize', function(){
			var $this = $(this);
			var $this_Top=$this.scrollTop();
			
			//當高度小於410時，關閉區塊	
			if($this_Top < 250){
				$('#over_layer').stop().animate({top:"-250px"});
				}
			if($this_Top > 250){
				$('#over_layer').stop().animate({top:"0px"});
						
			}
		}).scroll();
	});
});
</script>
<!-- overlayer -->

<script src="https://www.mingweekly.com/js/uisearch.js"></script>
<script>
 new UISearch(document.getElementById('sb-search'));
 new UISearch(document.getElementById('sb-search-footer'));
</script>
<script>
  $(function(){
  $(window).load(function(){
  $(window).bind('scroll resize', function(){
  var $this = $(this);
  var $this_Top=$this.scrollTop();
  //當高度小於410時，關閉區塊
  if($this_Top < 410){
  $('#over_layer').stop().animate({top:"-410px"});
  }
  if($this_Top > 410){
  $('#over_layer').stop().animate({top:"0px"});
  }
  }).scroll();
  });
  });
</script>

<script type="text/javascript">
	$(function(){
		$('.mint-article-coc').each(function(){
			var $this = $(this), 
				$desc = $this.find('.social'),
				_descHeight = $desc.outerHeight(true),
				_speed = 400;
			
			$desc.css({
				height: 0,
				display: 'block'
			});
			
			$this.hover(function(){
				$desc.stop().animate({
					height: _descHeight
				}, _speed);
			}, function(){
				$desc.stop().animate({
					height: 0
				}, _speed);
			});
		});
	});
</script>


<!-- S新增的JS -->
<script src="https://www.mingweekly.com/js/page.js"></script>


<!-- footer -->
{!! $options['{v3_script_tag}'] !!}
</body>
</html>