
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible">
<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1, user-scalable=yes" />
	

	<meta property="fb:app_id" content="469667836553383" />
	<meta property="fb:pages" content="255057814552667" />
	<meta name="robots" content="follow, index"/>
	<meta name="title" content="士乃中总及村民协会．6月23联办周年联欢宴" />
	<meta name="description" content="士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。" />
	<meta name="twitter:site" content="https://www.sinchew.com.my/content/content_2044619.html" />
	<meta name="twitter:title" content="士乃中总及村民协会．6月23联办周年联欢宴" />
	<meta name="twitter:description" content="士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。" />
		
	<meta property="og:image" content="https://cdnpuc.sinchew.com.my/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" />
	<meta name="twitter:image" content="https://cdnpuc.sinchew.com.my/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" />
		
	<meta property="og:image:width" content="1200"/>	
	<meta property="og:image:height" content="630"/>
	<meta property="og:url" content="https://www.sinchew.com.my/content/content_2044619.html" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="士乃中总及村民协会．6月23联办周年联欢宴" />
	<meta property="og:description" content="士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。" />	
		
	
<title>
士乃中总及村民协会．6月23联办周年联欢宴 - 地方 - 大柔佛 | 
大柔佛 | 
星洲网 Sin Chew Daily
</title>
<script type="text/javascript">
if (location.protocol != 'https:')
{
 location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}	
</script>

<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Cache" content="no-cache">

<!-- jquery library -->
<script src="https://cdnpuc.sinchew.com.my/resource/jquery.min.js"></script>

<!-- detect user device -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.4.3/mobile-detect.min.js"></script>

<!-- redirect to correct url -->
<script src="https://www.sinchew.com.my/resource/detect.js?a3"></script>

<!-- bootstrap css and js -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://cdnpuc.sinchew.com.my/resource/bootstrap.min.js"></script>

<!-- sinchew logo -->
<link rel="shortcut icon" type="image/png" href="https://cdnpuc.sinchew.com.my/resource/sinchewlogo.png"/>

<link rel="stylesheet" href="https://cdnpuc.sinchew.com.my/resource/desktop.css">

<!-- mainpage slider css -->
<link rel="stylesheet" type="text/css" href="https://cdnpuc.sinchew.com.my/resource/lightslider.css">

<!-- mainpage slider js -->
<script src="https://cdnpuc.sinchew.com.my/resource/lightslider.js"></script>

<!-- comment css -->
<link href="https://cdnpuc.sinchew.com.my/resource/comment.css" rel="stylesheet">

<!-- menubar css -->
<link href="https://cdnpuc.sinchew.com.my/resource/perfect-scrollbar.css" rel="stylesheet">

<!-- menubar js -->
<script src="https://cdnpuc.sinchew.com.my/resource/perfect-scrollbar.min.js"></script>

<!-- swiper css -->
<link rel="stylesheet" href="https://cdnpuc.sinchew.com.my/resource/swiper.css">

<!-- footer css -->
<link rel="stylesheet" href="https://cdnpuc.sinchew.com.my/resource/d-footer.css">

<!-- Founder -->
<script src="https://cdnpuc.sinchew.com.my/resource/mypersonalcommon.js"></script>
<script src="https://cdnpuc.sinchew.com.my/resource/personalmessage.js"></script>
<script src="https://cdnpuc.sinchew.com.my/resource/md5.js"></script>
<script async src="https://static.addtoany.com/menu/page.js"></script>

<!-- display time for IE and Safari -->
<script src="https://cdnpuc.sinchew.com.my/resource/moment.js"></script>

<!-- display time/clickcount and others -->
<script src="https://cdnpuc.sinchew.com.my/resource/swiper.js"></script>

<!-- 分享点击量js -->
<script type="text/javascript" src="https://app.sinchew.com.my/amucsite/stat/WebShare.js"></script>

<!-- Desktop sub homepage css  -->
<link rel="stylesheet" type="text/css" href="https://cdnpuc.sinchew.com.my/resource/d_subhomepage.css">

<!--Fav -->
<link rel="stylesheet" type="text/css" href="https://cdnpuc.sinchew.com.my/resource/fav.css">
<script src="https://cdnpuc.sinchew.com.my/resource/fav.js"></script>

<!-- All windows onload -->
<script src="https://cdnpuc.sinchew.com.my/resource/specialtime.js"></script>
	
<!-- Admanager -->
<!-- 柔佛详情页 PC  -->
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
	//googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_1x1', [1, 1], 'div-gpt-ad-1543077774044-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_Leaderboard', [[970, 90], [728, 90], [970, 250]], 'div-gpt-ad-1543077840668-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_RectangleI', [300, 250], 'div-gpt-ad-1543077896195-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_RectangleII(Inner)', [300, 250], 'div-gpt-ad-1543078174365-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_RectangleIII(Inner)', [300, 250], 'div-gpt-ad-1543078238595-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2_BottomBanner_ROS', [[970, 90], [728, 90]], 'div-gpt-ad-1542969361433-0').addService(googletag.pubads());
	googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>	
	
<script>
	var _pvmax = { "region": "my", "siteId": "fb6c2f5d-ef4e-40f8-a264-88f50da0f92c" };
	(function (d, s, id) {
		var js, pjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id; js.async = true;
		js.src = "//api.pvmax.net/v1.0/pvmax.js";
		pjs.parentNode.insertBefore(js, pjs);
	}(document, 'script', 'pvmax-jssdk'));

	!function (e, t, n, a, o, s, c) { e.tpq || (o = e.tpq = function () { o.callMethod ? o.callMethod.apply(o, arguments) : o.queue.push(arguments) }, o.queue = [], o.callMethods = [], s = t.createElement(n), c = t.getElementsByTagName(n)[0], s.async = !0, s.src = a, c.parentNode.insertBefore(s, c)) }(window, document, "script", "//t.funp.com/js/loader.js", "tpq");
	tpq('init', '134018364875');
	tpq('track', 'PageView');
	tpq('trackCustom', 'retargeting', { "tag_id": "403229476961" });
</script>

<style>
	html, body 
	{
		position: relative;
		height: 100%;
	}

	body 
	{
		font-size: 15px;
		margin: 0;
		padding: 0;
	}
	
	p {
    margin: 0 0 24px;
	}
	
	figcaption#figures_1547704106451.imgComment_content{
		width:100% !important;
	}		
	
	*
	{ 
		padding:0;
		margin:0;
		list-style:none;
		border:0;
	}

	/*.swiper-container 
	{
		width: 100%;
		height: 100%;
	}

	.swiper-slide 
	{
		text-align: center;
		font-size: 18px;
		background: #fff;
	
		display: -webkit-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		-webkit-box-pack: center;
		-ms-flex-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		align-items: center;
	}

	#swiper-button-prev, .swiper-container-rtl #swiper-button-next
	{
		background-image:url("https://www.sinchew.com.my/resource/icon-prev.png");
		width: 40px;
	}

	#swiper-button-next, .swiper-container-rtl #swiper-button-prev
	{
		background-image:url("https://www.sinchew.com.my/resource/icon-next.png");
		width: 40px;
	}

	.d-kuaibao-weather-container
	{
		max-width:1024px;
		margin:0 auto;
		text-align:center;
		display:inline-block;
		height:auto;
		vertical-align:middle;
	}

	.d-kuaibao
	{
		width:70%;
		text-align:left;
		display:inline-block;
		float:left;
		vertical-align:middle;
	}*/

	.daohang
	{
		text-align:left;
	}

	/*.swiper-pagination
	{
		text-align:right;
		padding-right:10px;
	}*/

	ivideoplayer-title
	{
		display:none;
	}

	.ivideoplayer-lastUpdate
	{
		display:none;
	}

	.ivideoplayer-description.ivideoplayer-info-header.displayBox
	{
		display: none !important;
	}

	.ivideoplayer-tags.ivideoplayer-info-header
	{
		display: none !important;
	}

	.dneiwen-hr
	{
		height:1px;
		margin-top:10px;
		margin-bottom:10px;
	}

	.neiwen-logn-guanggao
	{
		width:100%;
		margin:0 auto;
		font-size:12px;
		text-align:center;
	}
	
	.neiwen-top-container
	{
		width:100%;
		position:relative;
		vertical-align:middle;
	}
	
	.neiwen-top-title
	{
		width:60%;
		display:inline-block;
		text-align:left;
		font-size:16px;
	}
	
	.neiwen-top-share
	{
		width:38%;
		display:inline-block;
		vertical-align:middle;
		text-align:right;
	}
	
	.neiwen-top-bookmark
	{
		width:10%;
		display:inline-block;
	}
	
	#forsharebutton img
	{
		width:25px;
		height:25px;
	}
	
	#forsharebutton
	{
		display:inline-block;
		vertical-align:middle;
	}
	
	.neiwen-interest
	{
		width:100%;
		height:auto;
		padding:10px;
		background:#e6e6e6;
		font-size:20px;
		margin-top:10px;
		font-weight:bold;
	}
	
	.othersnew_left, .comment_left
	{
		text-align:left;
		width:50%;
		color:#0b3831;
		font-weight:bold;
		display:inline-block;
		float:left;
		padding-bottom:15px;
		font-size:20px;
	}

	/*.othersnew_right, .comment_right
	{
		text-align:right;
		width:50%;
		font-weight:bold;
		display:inline-block;
		float:left;
		padding-bottom:15px;
		font-size:14px;
	}*/
	
	.hot_title
	{
		text-align:left;
	}
	
	/*.comment_right
	{
		cursor:pointer;
	}*/
</style>

{!! $options['{v3_style_tag}'] !!}
</head>

<body>
<div align="center" style="max-width: 1024px; margin: auto">
	
	<!-- Menubar 第一层 -->
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WC3H2D');</script>
<!-- End Google Tag Manager -->

<script>
var script_tag = document.createElement('script'); script_tag.type = 'text/javascript'; script_tag.src = '//storage.googleapis.com/adasia-ad-network-origin/atm/library/avmLibrary.js'; script_tag.async = true; script_tag.onload = function() { var adAsiaTM = window.adAsiaTM || adAsiaTM, zoneList = []; adAsiaTM.init(zoneList);}; document.head.appendChild(script_tag);
</script>

<style>
#d-menu-container 
{
	margin: 0 auto;
	height: auto;
	max-width: 1024px;
	/*overflow: auto;*/
	position: relative;
	white-space:nowrap;
	/*font-weight: bold;*/
	text-align:center;
	background-color:#f9f9f9;
	border-bottom:2px solid #133b34;
	
}

.d-menu-son
{
	font-size:22px;
	color:#464646;
	position:relative;
	display:inline-block;
	/*border-style: solid;*/
    /*padding: 5px 30px 15px 15px;*/
    /*border-color: #afafaf;*/
    /*border-width: 2px 0px 0px 0px;*/
	width:102.4px; /* follow max width */
	padding:10px 0;
	text-align:center;
}

.dropdownlistbylist
{
	color:#464646;
	position:relative;
	display:inline-block;
	width:102.39px; /* follow max width */
	font-size:14px;
	vertical-align:top;
	padding:12px 0 12px 0;
	text-align:center;
}

.forlineonly
{
	border-right:1px solid;
	border-image: linear-gradient(to right, #adadad 0%,#afafaf 100%);
    border-image-slice: 1;
	min-height:105px;
	width:1px;
	display:inline-block;
}

.dropdownlistbylist a, .dropdownlistbylist a:hover
{
	color:#464646;
	text-decoration:none;
}

.d-menu-son a, .d-menu-son a:hover
{
	color:#464646;
	text-decoration:none;
}

.d-header
{
	width:100%;
	padding-bottom:5px;
}

.d-header a, .d-header a:hover
{
	text-decoration:none;
	color:black;
}

.dh-topleft
{
	width:40%;
	display:inline-block;
	text-align:left;
	vertical-align: top;
}

.dh-middle
{
	width:20%;
	display:inline-block;
}

.dh-middle img
{
	width:80%;
}

.dh-topright
{
	width:40%;
	display:inline-block;
	vertical-align:top;
	text-align: right;
    padding: 3px;
}

.dropdown 
{
    position: relative;
    display: inline-block;
}

.dropdown-content 
{
    display: none;
    position: absolute;
	right: 0;
    background-color: #f9f9f9;
    width: 1024px;
    /*box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);*/
    z-index: 999;
	text-align:left;
	margin-top:-15px;
}

/*.dropdown:hover .dropdown-content 
{
    display: block;
}*/

.dh-topright, .dh-tr-name, .dh-tr-subscribe, .dh-tr-newsletter
{
	display:inline-block;
}

/**************** drop down *****************/
.dropbtn 
{
	background-color: #4CAF50;
	color: white;
	padding: 16px;
	font-size: 16px;
	border: none;
}

.dh-tr-name 
{
	position: relative;
	display: inline-block;
}

.dh-tr-name-content 
{
	display: none;
	position: absolute;
	background-color: #f1f1f1;
	width: 230px;
	box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
	z-index: 1;
	padding:5px;
}

.dh-tr-name-content a 
{
	color: black;
	padding: 10px;
	text-decoration: none;
	display: block;
}

.dh-tr-name:hover .dh-tr-name-content 
{
	display: block;
}

.dh-tr-name:hover .dropbtn 
{
	background-color: white;
}
/**************** drop down end *****************/

.dh-dd-seting a, .dh-dd-seting a:hover
{
	text-decoration:none;
	color:black;
	display:inline-block;
}

.dh-dd-right
{
	width:50%;
	margin:0 auto;
	text-align:center;
	display:inline-block;
	vertical-align:middle;
	height:70px;
	line-height:70px;
}

.dh-dd-seting
{
	float: left;
    text-align: left;
	width:100%;
}

.dh-dd-right img
{
	width:40%;
}

.dh-dd-left
{
	display: inline-block;
    width: 50%;
    text-align: left!important;
    float: left;
    vertical-align: middle;
    padding-top: 10px;
    padding-left: 10px;
    height: 70px;
}

#contentKey
{
	width:100px;
	border:1px solid black;
}

.searchbutton
{
	background-image: url(https://www.sinchew.com.my/resource/iconsearch.png);
	background-repeat: no-repeat;
	background-position: 50% 50%;
	width: 30px;
	border: none;
	background-color:white !important;
	vertical-align: middle;
	outline: none;
}

/* 关注我们 */

.dh-tr-name1 
{
	position: relative;
	display: inline-block;
}

.dh-tr-name-content1 
{
	display: none;
	position: absolute;
	background-color: #f1f1f1;
	width: 120px;
	box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
	z-index: 9999;
	padding:5px;
}

.dh-tr-name-content1 a 
{
	color: black;
	padding: 10px;
	text-decoration: none;
	display: block;
}

.dh-tr-name1:hover .dh-tr-name-content1 
{
	display: block;
}

.dh-dd-seting1 img
{
	width:25px;
	display:inline-block;
}

.share-inside-dropdown
{
    display: inline-block !important;
    padding: 5px !important;
}

#loginaftercheck
{
	display:inline-block;
	cursor:pointer;
}

.dropdownlistbylist img
{
	width: 35px;
    margin-left: 20px;
    margin-bottom: 8px;
}

#gengduo
{
	cursor:pointer;
}
</style>

<script>
	//when touch or click hide this
	$(document).on('click touchstart', function(){
	  $("#dropdown-menu").hide();
	});
	
	$(document).ready(function() {
		//skip it when touch or click this 2 div
		$("#dropdown-menu").on('click touchstart', function(e){
		  e.stopPropagation();
		});
		
		$("#skipthis").on('click touchstart', function(e){
		  e.stopPropagation();
		});
	});
	
	//normal show hide js
	function openmylist()
	{
		var checkdisplay_kuaibao = document.getElementById("dropdown-menu").style.display;
		
		if(checkdisplay_kuaibao == "none" || checkdisplay_kuaibao == "")
		{
			document.getElementById("dropdown-menu").style.display = "block";
		}
		else
		{
			document.getElementById("dropdown-menu").style.display = "none";
		}
	}
	
	//提交表单
	function per_submit()
	{
		var flag = true;

		//alert("content="+form1.contentKey.value);
		if(form1.contentKey.value=="")
		{
			return;
		}
		if( flag == true ) 
		{
			document.form1.submit(); 
			result_list.innerHTML="正在进行检索，请耐心等待结果...";
		} 
	}
	
	var userID = doGetCookie("uid_fouNder");
	
	function doGetCookie(uid)
	{
		var aCookie = document.cookie.split("; ");
		for (var i = 0; i < aCookie.length; i++) 
		{
		  var aCrumb = aCookie[i].split("=");
		  if (uid == aCrumb[0])
			return unescape(aCrumb[1]);
		}
		return "false";
	}
	
	function ssologin() 
	{
		window.open("https://sso.sinchew.com.my:1443/SSOv2/user/ssoLogin?code=" + myFav.authCode + "&redirectUrl=" + myFav.login_out + "sso/setCookie.html?&anyUrl=" + location.href, "newwindow", myFav.window_site);
	}
	
	function getuserinfo(userIDfromcookie)
	{
		var url = 'https://sso.sinchew.com.my:1443/SSOv2/api/user/info?uid='+userIDfromcookie;
		  $.ajax ({
			url:url,
			type:'GET',
			dataType:'json',
			success:function(data)
			{	
				//data.username;
				if(data.nickname != "")
				{
					document.getElementById("changetoname").innerHTML = data.nickname;
					document.getElementById("changetonameagain").innerHTML = "你好<br>" + data.nickname;
				}
				else
				{
					document.getElementById("changetoname").innerHTML = data.email;
					document.getElementById("changetonameagain").innerHTML = "你好<br>" + data.email;
				}
				
				if(data.head != "")
				{
					$('.dh-dd-right').html("<img src='"+ data.head +"' style='max-width:100%;'>");
				}
			}
		}) 
	}
	
	function logoutnow()
	{
		window.open("https://sso.sinchew.com.my:1443/SSOv2/user/ssoLogout?code=" + personalMessage.authCode + "&uid=" + personalMessage.uid + "&sso_token=" + personalMessage.doGetCookie("sso_token") + "&from=" + personalMessage.login_out + "sso/delCookie.html", "newwindow", personalMessage.window_site);
		
		setTimeout(function () {
			location.reload();
		}, 3000);
	}
	
	function search()
	{
		if(document.getElementById("contentKey").value == "")
			alert("搜索不能为空！");
		else
		{
			var contentKey = document.getElementById("contentKey").value;
			
			window.location.href = "https://www.sinchew.com.my/column/search.html?searchkey=" + contentKey;
		}
	}
	
	function gethotarticlezz(huatiid,columnappend,huatiamount)
	{
		if(huatiid == null)
		{
			//do nothing
		}
		else if(huatiid == "")
		{
			//do nothing
		}
		else
		{
			var url = 'https://www.sinchew.com.my/app_if/webTopicsByGroup?siteID=1';
			  $.ajax ({
				url:url,
				data:{"page":"0","groupID":huatiid},
				type:'GET',
				async: false,
				cache: true,
				dataType:'text',
				success:function(data)
				{	 
					datalist = JSON.parse(data).list;
					
					//如果现有话题数量 少于需求话题数量 选择现有话题数量 这样就不会出现js报错
					var compare = (datalist.length < huatiamount)  ?  datalist.length : huatiamount;
					var testxiaplus = "";
					
					for(var i=0;i<compare;i++)
					{
						var title = datalist[i].title;
						var publishtime = datalist[i].publishtime;
						var topicID = datalist[i].topicID;
						if (parseInt(topicID)) //topicID 有数字
						{
							if(i == 0)
							{
								testxiaplus = "<a style='color:#cd2026;text-decoration:none;' href='https://www.sinchew.com.my/column/node_147.html?topicId=" + topicID + "'>" + title + "</a><br>";
							}
							else
							{
								testxiaplus += "<a style='color:#cd2026;text-decoration:none;' href='https://www.sinchew.com.my/column/node_147.html?topicId=" + topicID + "'>" + title + "</a><br>";
							}
						}
						else
						{
							testxiaplus = "";
						}
					}
					
					document.getElementById(columnappend).innerHTML = testxiaplus;
				}		
			})
		}
	}
	
	function toogleme()
	{
		var check = document.getElementById("lowbw");
		var dd = document.getElementById("showmydd");
		var leftside = document.getElementById("menuson13");
		var hh25 = document.getElementById("menuson25");
		var hh2 = document.getElementById("menuson2");
		var hh4 = document.getElementById("menuson4");
		var hh5 = document.getElementById("menuson5");
		var hh6 = document.getElementById("menuson6");
		var hh7 = document.getElementById("menuson7");
		var hh26 = document.getElementById("menuson26");
		var hh27 = document.getElementById("menuson27");
		var rightside = document.getElementById("gengduo");
		
		if(check.value == "hide")
		{
			dd.style.display = "block";
			check.value = "show";
			leftside.style.backgroundColor = "#f9f9f9";
			rightside.style.backgroundColor = "#f9f9f9";
			hh25.style.backgroundColor = "#f9f9f9";
			hh2.style.backgroundColor = "#f9f9f9";
			hh4.style.backgroundColor = "#f9f9f9";
			hh5.style.backgroundColor = "#f9f9f9";
			hh6.style.backgroundColor = "#f9f9f9";
			hh7.style.backgroundColor = "#f9f9f9";
			hh26.style.backgroundColor = "#f9f9f9";
			hh27.style.backgroundColor = "#f9f9f9";
			
		}
		else
		{
			dd.style.display = "none";
			check.value = "hide";
			leftside.style.backgroundColor = "#f1f1f2";
			rightside.style.backgroundColor = "#f1f1f2";
			hh25.style.backgroundColor = "#f1f1f2";
			hh2.style.backgroundColor = "#f1f1f2";
			hh4.style.backgroundColor = "#f1f1f2";
			hh5.style.backgroundColor = "#f1f1f2";
			hh6.style.backgroundColor = "#f1f1f2";
			hh7.style.backgroundColor = "#f1f1f2";
			hh26.style.backgroundColor = "#f1f1f2";
			hh27.style.backgroundColor = "#f1f1f2";
		}
	}
</script>

<div class="d-header">
	<div class="dh-topleft"><!-- 关注我们 -->
		<div class="dh-tr-name1">
			<div class="dh-tr-name-click1"><img src="https://cdnpuc.sinchew.com.my/resource/followus.png"></div>
			  <div class="dh-tr-name-content1">
				<div class="dh-dd-seting1">
					<a href="http://epaper.sinchew.my">星洲电子报</a>
					<a href="http://www.logon.my" target="_blank">Logon网购</a>
					<!--<a href="">Mysinchew</a>-->
					<a>社交媒体</a>
					<a href="https://www.facebook.com/SinChewDaily/" class="share-inside-dropdown" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-fb.png"></a><!--
					--><a href="https://twitter.com/SinChewPress" class="share-inside-dropdown" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-twitter.png"></a><!--
					--><a href="https://www.youtube.com/channel/UCKC1q1zc_CG2MtOCEHNUt1Q" class="share-inside-dropdown" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-youtube.png"></a><!--
					--><a href="https://www.instagram.com/sinchewdaily/" class="share-inside-dropdown" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-instagram.png"></a><!--
					--><a href="https://www.weibo.com/sinchewi" class="share-inside-dropdown" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-weibo.png"></a><!--
					--><a href="https://www.sinchew.com.my/wechat" class="share-inside-dropdown" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-wechat.png"></a>
				</div>
			  </div>
		</div>
		<a href="https://www.sinchew.com.my" style="text-decoration:none;color:black;"><img src="https://cdnpuc.sinchew.com.my/resource/iconhome.png" style="margin-top:-5px;"> 首页</a> | 
		<a href="">登广告</a> |
		
							<a href="https://www.sinchew.com.my/column/node_9.html">活动</a>

		<input name="contentKey" id="contentKey" type="text" size="50" maxlength="50" value="">
		<input type="button" id="enterkeydetect" name="search" onclick="javascript:search()" class="searchbutton">
	</div><!--
	--><div class="dh-middle">
		<a href="https://www.sinchew.com.my/" target="_self">
			<img src="https://cdnpuc.sinchew.com.my/resource/dsinchew.png" id="dh-logo">
		</a>
	</div><!--
	--><div class="dh-topright">
		<div id="changebylogin" style="display:inline-block;">
		<div class="dh-tr-name">
			<div class="dh-tr-name-click" id="changetoname"></div>
			  <div class="dh-tr-name-content">
				<div class="dh-dd-left" id="changetonameagain"></div>
				<div class="dh-dd-right"><img src="https://cdnpuc.sinchew.com.my/resource/icon-avatar.png"></div><br>
				<div class="dh-dd-seting">
					<a href="https://www.sinchew.com.my/column/personal.html">个人中心</a>
					<!--<a href="">设置</a>-->
					<a href="#" onclick="logoutnow()" style="text-align: center;float: right;width: 110px;">登出</a>
				</div>
			  </div>
		</div> |
		</div>
		<div class="dh-tr-subscribe"><a href="https://www.sinchew.com.my/SubscriptionPress/">订阅</a></div> |
		<div class="dh-tr-newsletter"><a href="https://www.sinchew.com.my/newsletter.html">NEWSLETTER</a></div>
	</div>
</div>

<script>
if(userID != "false" && userID != "")
{
	//登陆了
	getuserinfo(userID);
}
else
{
	//还没登陆
	//var loginalready = false;
	$("#changebylogin").html("<div id='loginaftercheck' onclick='javascript:ssologin();'>登录 |</div>");
	//document.getElementById("changebylogin").innerHTML = "<div id='changebylogin' onclick='javascript:ssologin();'>登录</div>";
}

var input = document.getElementById("contentKey");

input.addEventListener("keyup", function(event) {
  event.preventDefault();
  if (event.keyCode === 13)
  {
    document.getElementById("enterkeydetect").click();
  }
});
</script>

<style>
.m-menu-son
{
	cursor:pointer;
}
</style>

<div id="d-menu-container">
	<div class="d-menu-son" id="menuson13">
		<a href="https://www.sinchew.com.my/nation.html" id="ca13">全国</a>
	</div><!--
	--><div class="d-menu-son" id="menuson25">
		<a href="https://www.sinchew.com.my/world.html" id="ca25">国际</a>
	</div><!--
	--><div class="d-menu-son" id="menuson2">
		<a href="https://www.sinchew.com.my/business.html" id="ca2">财经</a>
	</div><!--
	--><div class="d-menu-son" id="menuson4">
		<a href="https://www.sinchew.com.my/opinion.html" id="ca4">言路</a>
	</div><!--
	--><div class="d-menu-son" id="menuson5">
		<a href="https://www.sinchew.com.my/mykampung.html" id="ca5">地方</a>
	</div><!--
	--><div class="d-menu-son" id="menuson6">
		<a href="https://www.sinchew.com.my/entertainment.html" id="ca6">娱乐</a>
	</div><!--
	--><div class="d-menu-son" id="menuson7">
		<a href="https://www.sinchew.com.my/sport.html" id="ca7">体育</a>
	</div><!--
	--><div class="d-menu-son" id="menuson26">
		<a href="https://www.sinchew.com.my/life.html" id="ca26">副刊</a>
	</div><!--
	--><div class="d-menu-son" id="menuson27">
		<a href="https://www.sinchew.com.my/pocketimes.html" id="ca27">百格</a>
	</div><!--
	--><div class="d-menu-son dropdown" id="gengduo" style="border-right:none;color:black;" onclick="toogleme()">
			更多
			<span class="caret"></span>
			<input type="hidden" value="hide" id="lowbw">
		</div>
		
		<div class="dropdown-content" id="showmydd">
			
			<div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_7075.html">即时</a><br>
						<a href="https://www.sinchew.com.my/column/node_15.html">封面头条</a><br>
						<a href="https://www.sinchew.com.my/column/node_16.html">暖势力</a><br>
						<a href="https://www.sinchew.com.my/column/node_17.html">热点</a><br>
						<a href="https://www.sinchew.com.my/column/node_18.html">政治</a><br>
						<a href="https://www.sinchew.com.my/column/node_20.html">教育</a><br>
						<a href="https://www.sinchew.com.my/column/node_19.html">社会</a><br>
						<a href="https://www.sinchew.com.my/column/node_21.html">全国综合</a><br>
						<a href="https://www.sinchew.com.my/column/node_22.html">求真</a><br>
						<a href="https://www.sinchew.com.my/column/node_7215.html">国会</a><br>
						<a href="https://www.sinchew.com.my/column/node_23.html">专题</a><br>
						<a href="https://www.sinchew.com.my/column/node_24.html">我们</a><br>
						<a href="https://www.sinchew.com.my/column/node_6830.html">华社</a><br>
						<a href="https://www.sinchew.com.my/column/node_4651.html">图说大马</a><br>

					<div id="mthuati13" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_7076.html">即时</a><br>
						<a href="https://www.sinchew.com.my/column/node_29.html">天下事</a><br>
						<a href="https://www.sinchew.com.my/column/node_30.html">国际拼盘</a><br>
						<a href="https://www.sinchew.com.my/column/node_31.html">带你看世界</a><br>

					<div id="mthuati25" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_33.html">即时</a><br>
						<a href="https://www.sinchew.com.my/column/node_181.html">财经封面</a><br>
						<a href="https://www.sinchew.com.my/column/node_34.html">市场盘点</a><br>
						<a href="https://www.sinchew.com.my/column/node_35.html">企业动向</a><br>
						<a href="https://www.sinchew.com.my/column/node_36.html">国际财经</a><br>
						<a href="https://www.sinchew.com.my/column/node_37.html">投资法则</a><br>
						<a href="https://www.sinchew.com.my/column/node_42.html">活用理财</a><br>
						<a href="https://www.sinchew.com.my/column/node_206.html">产业兵法</a><br>

					<div id="mthuati2" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_127.html">总编时间</a><br>
						<a href="https://www.sinchew.com.my/column/node_119.html">风起波生</a><br>
						<a href="https://www.sinchew.com.my/column/node_118.html">非常常识</a><br>
						<a href="https://www.sinchew.com.my/column/node_4738.html">骑驴看本</a><br>
						<a href="https://www.sinchew.com.my/column/node_129.html">亮剑</a><br>
						<a href="https://www.sinchew.com.my/column/node_128.html">风雨看潮生</a><br>
						<a href="https://www.sinchew.com.my/column/node_123.html">星期天拿铁</a><br>
						<a href="https://www.sinchew.com.my/column/node_117.html">星．观点</a><br>
						<a href="https://www.sinchew.com.my/column/node_115.html">纯粹诚见</a><br>
						<a href="https://www.sinchew.com.my/column/node_7250.html">开门见山</a><br>
						<a href="https://www.sinchew.com.my/column/node_7257.html">天马行空</a><br>
						<a href="https://www.sinchew.com.my/column/node_7254.html">一派胡言</a><br>
						<a href="https://www.sinchew.com.my/column/node_4739.html">每日漫画</a><br>

					<div id="mthuati4" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/metro.html">大都会</a><br>
						<a href="https://www.sinchew.com.my/johor.html">大柔佛</a><br>
						<a href="https://www.sinchew.com.my/perak.html">大霹雳</a><br>
						<a href="https://www.sinchew.com.my/sarawak.html">砂拉越</a><br>
						<a href="https://www.sinchew.com.my/sabah.html">沙巴</a><br>
						<a href="https://www.sinchew.com.my/northern.html">大北马</a><br>
						<a href="https://www.sinchew.com.my/sembilan.html">花城</a><br>
						<a href="https://www.sinchew.com.my/melaka.html">古城</a><br>
						<a href="https://www.sinchew.com.my/eastcoast.html">东海岸</a><br>
						<a href="https://www.sinchew.com.my/column/node_6951.html">年轻人</a><br>
						<a href="https://www.sinchew.com.my/column/node_6952.html">有故事的人</a><br>
						<a href="https://www.sinchew.com.my/column/node_7183.html">地方味</a><br>

					<div id="mthuati5" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_66.html">国外娱乐</a><br>
						<a href="https://www.sinchew.com.my/column/node_68.html">影视</a><br>
						<a href="https://www.sinchew.com.my/column/node_65.html">大马娱乐</a><br>
						<a href="https://www.sinchew.com.my/column/node_64.html">即时娱乐</a><br>
						<a href="https://www.sinchew.com.my/column/node_67.html">星人物</a><br>
						<a href="https://www.sinchew.com.my/column/node_72.html">娱乐圈喜事</a><br>
						<a href="https://www.sinchew.com.my/column/node_69.html">明星二代</a><br>
						<a href="https://www.sinchew.com.my/column/node_71.html">艺人八卦</a><br>
						<a href="https://www.sinchew.com.my/column/node_70.html">星视镜</a><br>

					<div id="mthuati6" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_7136.html">即时</a><br>
						<a href="https://www.sinchew.com.my/column/node_7137.html">封面</a><br>
						<a href="https://www.sinchew.com.my/column/node_4757.html">论点</a><br>
						<a href="https://www.sinchew.com.my/column/node_54.html">大马</a><br>
						<a href="https://www.sinchew.com.my/column/node_55.html">羽球</a><br>
						<a href="https://www.sinchew.com.my/column/node_56.html">足球</a><br>
						<a href="https://www.sinchew.com.my/column/node_60.html">篮球</a><br>
						<a href="https://www.sinchew.com.my/column/node_4758.html">水上</a><br>
						<a href="https://www.sinchew.com.my/column/node_61.html">综合</a><br>
						<a href="https://www.sinchew.com.my/column/node_58.html">聚焦人物</a><br>
						<a href="https://www.sinchew.com.my/column/node_62.html">场外花絮</a><br>
						<a href="https://www.sinchew.com.my/column/node_63.html">沿图细说</a><br>

					<div id="mthuati7" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_91.html">副刊专题</a><br>
						<a href="https://www.sinchew.com.my/column/node_97.html">优质生活</a><br>
						<a href="https://www.sinchew.com.my/column/node_105.html">旅游</a><br>
						<a href="https://www.sinchew.com.my/column/node_106.html">美食</a><br>
						<a href="https://www.sinchew.com.my/column/node_177.html">专栏</a><br>
						<a href="https://www.sinchew.com.my/column/node_92.html">东西</a><br>
						<a href="https://www.sinchew.com.my/column/node_94.html">新教育</a><br>
						<a href="https://www.sinchew.com.my/column/node_95.html">e潮</a><br>
						<a href="https://www.sinchew.com.my/column/node_96.html">艺文Show</a><br>
						<a href="https://www.sinchew.com.my/column/node_98.html">护生</a><br>
						<a href="https://www.sinchew.com.my/column/node_99.html">看车</a><br>
						<a href="https://www.sinchew.com.my/column/node_100.html">养生</a><br>
						<a href="https://www.sinchew.com.my/column/node_101.html">快乐家庭</a><br>
						<a href="https://www.sinchew.com.my/column/node_103.html">文艺春秋</a><br>
						<a href="https://www.sinchew.com.my/column/node_104.html">星云</a><br>
						<a href="https://www.sinchew.com.my/column/node_107.html">非常人物</a><br>
						<a href="https://www.sinchew.com.my/column/node_108.html">影音</a><br>
						<a href="https://www.sinchew.com.my/column/node_109.html">读家</a><br>
						<a href="https://www.sinchew.com.my/column/node_7325.html">教育特辑</a><br>

					<div id="mthuati126" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_44.html">百格新闻</a><br>
						<a href="https://www.sinchew.com.my/column/node_7142.html">百格晨报</a><br>
						<a href="https://www.sinchew.com.my/column/node_7143.html">百格大事纪</a><br>
						<a href="https://www.sinchew.com.my/column/node_7144.html">财经Espresso</a><br>
						<a href="https://www.sinchew.com.my/column/node_7145.html">百格大家讲</a><br>
						<a href="https://www.sinchew.com.my/column/node_7146.html">On The Hot Seat</a><br>
						<a href="https://www.sinchew.com.my/column/node_7151.html">笔记本</a><br>
						<a href="https://www.sinchew.com.my/column/node_7152.html">百格人物</a><br>
						<a href="https://www.sinchew.com.my/column/node_46.html">百格纷</a><br>
						<a href="https://www.sinchew.com.my/column/node_47.html">百格特别企划</a><br>

					<div id="mthuati127" class="testinghuati"></div>
			</div><!--
			
			--><div class="dropdownlistbylist">
				
											<a href="https://www.sinchew.com.my/column/node_9.html">活动</a>
<br>
				
											<a href="https://www.sinchew.com.my/column/node_89.html">最热门</a>
<br>
				
											<a href="https://www.sinchew.com.my/column/node_88.html">图辑</a>
<br>
				
											<a href="https://www.sinchew.com.my/column/node_7207.html">美食</a>
<br>
			</div>
			
			<script>
				//var fzarray = [13,25,2,4,5,6,7,26,27];
				var fzarray = [13,25,2,7];
				//var htarray = [2,3,4,5,9,7,6,10,11];
				var htarray = [2,3,4,6];
				
				for(var k=0;k<fzarray.length;k++)
				{
					var getdivid = "mthuati" + fzarray[k];
					gethotarticlezz(htarray[k],getdivid,5);
				}
			</script>
	</div>
</div>
	
		<script>
							var element = document.getElementById("ca5");

				if(element != null)
				{
					element.classList.add("smplifymycode");
				}
				var element = document.getElementById("ca78");

				if(element != null)
				{
					element.classList.add("smplifymycode");
				}
		</script>

	<!-- Menubar 第一层 END -->

	<!-- 第二层 menubar -->
	<!-- 当前栏目 显示红色 -->
	<script>
		var mybrotherurl = new Array;
		var mybrothername = new Array;
		var mybrotherid = new Array;
		var mysonurl = new Array;
		var mysonname = new Array;
		var mysonid = new Array;
	</script>

	
		<script>
							mybrotherurl.push("https://www.sinchew.com.my/metro.html");
				mybrothername.push("大都会");
				mybrotherid.push("74");
				mybrotherurl.push("https://www.sinchew.com.my/johor.html");
				mybrothername.push("大柔佛");
				mybrotherid.push("78");
				mybrotherurl.push("https://www.sinchew.com.my/perak.html");
				mybrothername.push("大霹雳");
				mybrotherid.push("77");
				mybrotherurl.push("https://www.sinchew.com.my/sarawak.html");
				mybrothername.push("砂拉越");
				mybrotherid.push("86");
				mybrotherurl.push("https://www.sinchew.com.my/sabah.html");
				mybrothername.push("沙巴");
				mybrotherid.push("84");
				mybrotherurl.push("https://www.sinchew.com.my/northern.html");
				mybrothername.push("大北马");
				mybrotherid.push("75");
				mybrotherurl.push("https://www.sinchew.com.my/sembilan.html");
				mybrothername.push("花城");
				mybrotherid.push("81");
				mybrotherurl.push("https://www.sinchew.com.my/melaka.html");
				mybrothername.push("古城");
				mybrotherid.push("80");
				mybrotherurl.push("https://www.sinchew.com.my/eastcoast.html");
				mybrothername.push("东海岸");
				mybrotherid.push("79");
				mybrotherurl.push("https://www.sinchew.com.my/column/node_6951.html");
				mybrothername.push("年轻人");
				mybrotherid.push("6951");
				mybrotherurl.push("https://www.sinchew.com.my/column/node_6952.html");
				mybrothername.push("有故事的人");
				mybrotherid.push("6952");
				mybrotherurl.push("https://www.sinchew.com.my/column/node_7183.html");
				mybrothername.push("地方味");
				mybrotherid.push("7183");
		</script>


	
		<script>
					</script>


	<div id="m-menu-container2"></div>

	<script>
		if (mysonurl.length != "") 
		{
			for (var i = 0; i < mysonurl.length; i++) 
			{
				if(mysonname[i] == "话题")
				{
					//do nothing
				}
				else
				{
					$("#m-menu-container2").append('<div class="m-menu-son2" id="mt' + mysonid[i] + '"><a href="' + mysonurl[i] + '">' + mysonname[i] + '</a></div>');
				}

			}
		}
		else 
		{
			for (var i = 0; i < mybrotherurl.length; i++) 
			{
				if(mybrothername[i] == "话题")
				{
					//do nothing
				}
				else
				{
					$("#m-menu-container2").append('<div class="m-menu-son2" id="mt' + mybrotherid[i] + '"><a href="' + mybrotherurl[i] + '">' + mybrothername[i] + '</a></div>');
				}
			}
		}

		$("#m-menu-container2").append('<div id="mthuati" style="position: relative;display: inline-block; display:none;"><span style="color:black;padding:5px;">话题</span><span style="color:#cd2026;">&#9658;</span></div>');
	</script>

	<script> 
		var athere;
		var fzarray = [13,25,2,4,5,6,7,88,26,27];
		var htarray = [2,3,4,5,9,7,6,8,10,11];
	</script>
	
					<script>
				for(var k=0;k<fzarray.length;k++)
				{
					if(5 == fzarray[k])
					{
						athere = htarray[k];
					}
				}
			</script>
			<script>
				for(var k=0;k<fzarray.length;k++)
				{
					if(78 == fzarray[k])
					{
						athere = htarray[k];
					}
				}
			</script>

	<script>gethotarticle(athere, "mthuati", 3)</script>
	
	
		<script>
							var element = document.getElementById("mt5");

				if(element != null)
				{
					element.classList.add("pc2bar");
				}
				var element = document.getElementById("mt78");

				if(element != null)
				{
					element.classList.add("pc2bar");
				}
		</script>

	
	<script>
		new PerfectScrollbar("#m-menu-container2", {
		  useBothWheelAxes: true
		});
	</script>
		<!-- 第二层 menubar END -->

		<!-- 快报 -->
		<!-- Demo styles -->
<style>
	.kuaibao-container
	{
		width:100%;
		position:relative;
		background-color:#cd2026;
	}

	#swiper-container 
	{
		height: auto;
		display:inline-block;
		vertical-align:middle;
		background-color:#cd2026;
		padding:5px;
	}

	.kuaibao-left-true
	{
		display:inline-block;
		vertical-align:middle;
		padding:5px;
		background-color:#cd2026;
		color:white;
		text-align: center;
	}

	.kuaibao-link
	{
		color:white;
		text-decoration:none;
	}

	.kuaibao-link:hover
	{
		color:white;
		text-decoration:none;
	}

	.swiper-button-next
	{
		outline:none;
	}

	.kuaibao-left-true
	{
		width:12%;
		font-size:18px;
	}
	
	#swiper-container
	{
		width:88%;
		font-size:18px;
	}
	
	.timeweather
	{
		display: inline-block;
	}
	
	.d-kuaibao-weather-container
	{
		max-width:1024px;
		margin:0 auto;
		text-align:center;
		display:inline-block;
		height:auto;
		vertical-align:middle;
	}

	.d-kuaibao
	{
		width:85%;
		text-align:left;
		display:inline-block;
		float:left;
		vertical-align:middle;
	}

	.d-weather
	{
		width:14%;
		text-align:left;
		display:inline-block;
		float:left;
		vertical-align:middle;
		padding: 7px 0px 7px 10px;
		height:auto;
	}

	.swiper-pagination
	{
		text-align:right;
		padding-right:10px;
	}
	
	#swiper-slide1, #swiper-slide2, #swiper-slide3, #swiper-slide4
	{
		text-align: left;
		background: #cd2026;
		color:white;

		/* Center slide text vertically */
		display: -webkit-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		/*-webkit-box-pack: center;
		-ms-flex-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		align-items: center;*/
		padding-left:8px;
	}
</style>
<!-- Swiper -->

<div class="d-kuaibao-weather-container">
	<div class="d-kuaibao">
		<div clss="kuaibao-container">
			<div class="kuaibao-left-true">快报 | </div><!--
		  --><div class="swiper-container" id="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" id="swiper-slide1"></div>
				<div class="swiper-slide" id="swiper-slide2"></div>
				<div class="swiper-slide" id="swiper-slide3"></div>
				<div class="swiper-slide" id="swiper-slide4"></div>
			</div>
			<!-- Add Arrows -->
			<div class="swiper-button-next"></div>
		  </div>
		</div>
	</div>
	
	<div class="d-weather">
		<div id="showtime" class="timeweather"></div>
	</div>
</div>	

<script>var i=1;</script>


			<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044606.html">' + text_truncate("《南华》：敦马参访释信号·华为争马5G市场 获“认可”",30) + '</a>';
			i++;
		</script>
		<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044786.html">' + text_truncate("复活节爆炸案罹难人数·斯里兰卡下修至253人",30) + '</a>';
			i++;
		</script>
		<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044791.html">' + text_truncate("​首季业绩报捷·大马回险涨4.37%·马股开高走低",30) + '</a>';
			i++;
		</script>
		<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044781.html">' + text_truncate("梁裕恆全删评论“安心偷吃事件”帖文",30) + '</a>';
			i++;
		</script>


<!-- Swiper JS -->
<script src="https://cdnpuc.sinchew.com.my/resource/swiper.js"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('.swiper-container', {
	loop: true,
  navigation: {
	nextEl: '.swiper-button-next',
  },
   autoplay: {
	delay: 2500,
	disableOnInteraction: false,
  },
});

function showTime()
{ 
	var show_day = new Array('星期日','星期一','星期二','星期三','星期四','星期五','星期六'); 
	var time = new Date(); 
	var year = time.getFullYear();
	var month = time.getMonth(); 
	var date = time.getDate(); 
	var day = time.getDay();
	//month<10?month='0'+month:month; 
	month=month+1;
	var now_time = year+'/'+month+'/'+date+' '+show_day[day];
	//alert(day);
	document.getElementById('showtime').innerHTML = now_time; 
}

showTime();
</script>
		<!-- 快报 END -->
	

		<div style="clear:both"><br></div>
	
		<!-- 1x1  -->
		<script src='https://www.googletagservices.com/tag/js/gpt.js'>
		  googletag.pubads().definePassback('/2365395/SC2(Mykampung_Daroufo)_1x1', [1, 1]).display();
		</script>
					
		<div class="neiwen-logn-guanggao">
			<!-- Lead Ad  -->	
			<div class="ad_words" style="max-width:970px;min-width:728px;margin:0 auto;text-align:center;">
			<div>
			<div id='div-gpt-ad-1543077840668-0'><br/>
				广告<div><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1543077840668-0');});</script></div>
			</div>
			</div>
		  </div>
		</div>

		<hr class="dneiwen-hr">
					
		<div class="neiwen-top-container">
			<div class="neiwen-top-title">
				
												<!-- do nothing -->
						<span class="nav1"><a href="https://www.sinchew.com.my/mykampung.html">地方</a></span>
									>
						<span class="nav1"><a href="https://www.sinchew.com.my/johor.html">大柔佛</a></span>

			</div>
			
			<div class="neiwen-top-share">
				
					分享到 :&emsp;
					<div id="forsharebutton" class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="https://www.sinchew.com.my/content/content_2044619.html" data-a2a-title="士乃中总及村民协会．6月23联办周年联欢宴">
						<a class="a2a_button_facebook shareArt"><img src="https://www.sinchew.com.my/resource/share-fb.png"></a><!--
						--><a class="a2a_button_twitter shareArt"><img src="https://www.sinchew.com.my/resource/share-twitter.png"></a><!--
						--><a class="a2a_button_google_plus shareArt"><img src="https://www.sinchew.com.my/resource/share-g+.png"></a><!--
						--><a class="a2a_button_email shareArt"><img src="https://www.sinchew.com.my/resource/share-email.png"></a><!--
						--><a class="a2a_button_whatsapp shareArt"><img src="https://www.sinchew.com.my/resource/share-whatapps.png"></a><!--
						--><a class="a2a_button_wechat shareArt"><img src="https://www.sinchew.com.my/resource/share-wechat.png"></a><!--
						--><a class="a2a_button_copy_link shareArt"><img src="https://www.sinchew.com.my/resource/copy-link.png"></a>
					</div>

			</div>
		</div>
		
		<hr class="dneiwen-hr">
		<!--  左边 -->
		<div id="articlenum" style="width:670px;text-align:left;float:left;margin-top:10px;border-right: 1px solid #ccc;">
			
				
				<!--<img id="image1" src="https://cdnpuc.sinchew.com.my/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" height="100%" width="100%" alt="" /> <br /><br />-->
				2019-04-26 10:25:25&emsp;
				<articleID>2044619</articleID>
				<div class="articleId" style="display:none;">2044619</div>
				<img src="https://www.sinchew.com.my/resource/newview.png" width="18" alt="" />
				<div id="clicknum"><span id="spid2044619"></span></div>&emsp;
				<img src="https://www.sinchew.com.my/resource/newcomment.png" width="14" alt="" />
				<div id="disnum"><span id="spid2044619"></span></div>&emsp;
				<img src="https://www.sinchew.com.my/resource/newshare.png" width="15" alt="" />
				<div id="sharenum"><span id="spid2044619"></span></div>&emsp;
				<div class="neiwen-top-bookmark">
					<button class="hasFav"></button>
					<span class="hasFav_des" style="display:none;">收藏</span>
				</div>
				<div style="color:black;font-size:34px;">士乃中总及村民协会．6月23联办周年联欢宴</div>
				<div style="color:#cd2026;font-size:16px">大柔佛 </div><br/>
				<div id="dirnum" style="font-size:16px;color:#464646;padding-right:15px;"><!--enpcontent--><p style="white-space: normal;"></p><figure class="imgComment_figure" id="1556245448045" style="text-align: center;position:relative;margin: 0px"><img id="../../xy/image.do?path=图片存储;xy/201904/25/6a5d051c-3214-43da-b330-e3270840440a.jpg" src="https://cdnpuc.sinchew.com.my/pic/2019-04/26/6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" title="1662NHK20194251822452336007.jpg" alt="1662NHK20194251822452336007.jpg" style="max-width: 100%; width: 900px; height: 327px;" data_ue_src="https://cdnpuc.sinchew.com.my/pic/2019-04/26/6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" width="900" height="327" border="0" imgtime="1556245448045"/><figcaption id="figures_1556245448045" class="imgComment_content" style="top:326px;line-height:20px;padding: 0px 0px 0px;font-size: 0.9em;margin: 5px auto 0px;color: gray;text-align: center;word-wrap: break-word;width:100%">士乃中华总商会董事合照。前排左起为刘国基、陈贵友、蔡明酒、唐亚进、黄苟等。</figcaption></figure><p></p><p><br/></p><p>（古来25日讯）士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。</p><p><br/></p><p>当晚，士乃中华总商会将庆祝成立70周年、士乃村民协会庆祝成立47周年，两会也将同时举行新届董事、理事、青年团及妇女组就职典礼。<br/></p><p><br/></p><p>席金每桌700令吉，捐1000令吉或以上者为大会彩剪人，2000令吉或以上者为大会鸣锣人。<br/></p><p><br/></p><p>有意购席或捐款者，可洽陈贵友（019-711 1289）、黄苟（016-715 8786）、唐亚进（012-738 0206）、蔡明酒（013-799 7288）、张伟成（012-737 1533）、刘国基（019-771 3211）或刘治平（016-763 5355）。<br/></p><p><br/></p><p>陈贵友:加强联系发展业务 <br/></p><p><br/></p><p>士乃中华总商会甫上任的会长陈贵友接受星洲日报《大柔佛》社区报访问时表示，他寄望在新董事及青商团领导的合作下，加强商务方面的发展，让会员能够通过商会的平台联系在一起，加强生意上的发展。<br/></p><p><br/></p><p>他披露，该会目前有140名会员，年轻会员的人数还在不断增加中，该会将加强会员间的联系及提供更多资讯给会员，让大家有机会加强业务的发展。<br/></p><p><br/></p><p>他指出，士乃是柔州唯一拥有机场的地区，希望政府能够提升飞机场的价值，包括开拓更多国际航线、引进更多外来投资及旅客，以带动士乃甚至是整个柔州的经济发展。<br/></p><p><br/></p><p>他也希望更多青年及商家能够加入该会成员会员，协助推动该会会务，并借此平台与全国各地的商家交流取经，以在商务发展上取得双赢的优势。&nbsp;</p><p><br/></p><p><strong>中华总商会新届董事名表：</strong><br/></p><p>会长：陈贵友，署理：黄苟，副：廖迪传、唐亚进；总务：蔡明酒，副：张伟成；财政：刘国基，副：温俊文；政府事务主任：张稼禄，副：黄复达；教育主任：林伟生，副：陈汶安；宣传主任：刘治平，副：陈祈升；文书：刘治平（中）、彭浚宏（英）；交际：黄传燊、蔡进朋、曾进兴；义山主任：黄苟，副：蔡明酒；福利主任：曾妙富；董事：林金树、蔡伟雄、蔡永德；查账：彭迎华、曾英强。 <br/><br/></p><p><br/></p><p><strong>士乃青商团理事名表：</strong><br/></p><p>团长: 黄传燊，副: 彭威能、郑清方、谢副麟；总务: 蔡明权，副: 林胜源，黄意雄；<br/></p><p>财政：钟慧贞；秘书: 管偲琪，副：陈姝璇；交际: 许时嘉、黄治崴、黄锦文、符致陖、张健声。<br/></p><p><br/></p><p><strong>士乃村民协会理事名表：</strong><br/></p><p>主席：蔡明酒，署理：廖迪传，副：林伟生、李玉英；总务：刘治平，副：张伟成；财政：黄苟，副：廖诗云；教育主任：李秀兰，副：温俊文；文书：叶清敬；交际：郑凌蓉，副：刘嫚莉；外丹功主任：冯国金，副：曾妙富；康乐／卡拉OK主任：马金英，副：彭美娇；理事：温洪、张雅妹、蔡傍花、邓吉临、张秀萍、黄亚香、蔡舍妹、邱义翔；查账：彭俊鸿、温祥益。 <br/></p><p><br/></p><p></p><figure class="imgComment_figure" id="1556245518136" style="text-align: center;position:relative;margin: 0px"><img id="../../xy/image.do?path=图片存储;xy/201904/25/efa9c1a3-54a3-4111-be5f-1d45855be670.jpg" src="https://cdnpuc.sinchew.com.my/pic/2019-04/26/efa9c1a3-54a3-4111-be5f-1d45855be670_zsize.jpg" style="max-width: 100%; width: 500px; height: 660px;" title="1662NHK20194251822462336008.jpg" alt="1662NHK20194251822462336008.jpg" data_ue_src="https://cdnpuc.sinchew.com.my/pic/2019-04/26/efa9c1a3-54a3-4111-be5f-1d45855be670_zsize.jpg" width="500" height="660" border="0" imgtime="1556245518136"/><figcaption id="figures_1556245518136" class="imgComment_content" style="top:660px;line-height:20px;padding: 0px 0px 0px;font-size: 0.9em;margin: 5px auto 0px;color: gray;text-align: center;word-wrap: break-word;width:100%">陈贵友呼吁政府，在士乃机场开拓更多国际航线。</figcaption></figure><p></p><p><br/></p><p><br/></p><!--/enpcontent--><!--enpproperty <articleid>2044619</articleid><date>2019-04-26 10:25:25:214</date><author></author><title>士乃中总及村民协会．6月23联办周年联欢宴</title><keyword></keyword><subtitle></subtitle><introtitle></introtitle><siteid>1</siteid><nodeid>78</nodeid><nodename>大柔佛</nodename><nodesearchname></nodesearchname><picurl>https://cdnpuc.sinchew.com.my/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picurl><picbig>https://cdnpuc.sinchew.com.my/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picbig><picmiddle>https://cdnpuc.sinchew.com.my/pic/2019-04/26/t1_(115X0X415X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picmiddle><picsmall>https://cdnpuc.sinchew.com.my/pic/2019-04/26/t0_(112X0X489X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picsmall><url>https://www.sinchew.com.my/content/content_2044619.html</url><urlpad>https://www.sinchew.com.my/pad/con/content_2044619.html</urlpad><liability></liability><sourcename>星洲日报</sourcename><abstract>士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。</abstract><channel>1</channel>/enpproperty--></div>
				<div class="wycamefrom">
					
<br/>
					
					文章来源 ： 
						星洲日报  2019-04-26
				</div>

			
			<hr class="dneiwen-hr">
			
			<div style="padding-right:10px;">
				
		<!-- ivideoplaylist plugin -->
		<!-- ivideosense plugin -->  
        {{-- <script id="ivsn" src="https://player.ivideosmart.com/ivideosense/player/js/ivsnload_v1.js?key=x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K&wid=441850eb-4927"></script> --}}

        <div>
            <ivs-video
                thumbnail="https://2.bp.blogspot.com/-xpgIOttwTTg/TWRni-cC-tI/AAAAAAAAA-o/9GMqFyMnQCY/s1600/african-elephant2.jpg"
                id="ivsplayer001"
                :id-i-frame="idIFrame"
              ></ivs-video>
              <ivs-playlist
                type="carousel"
                widget-id="441850eb-4927"
                api-key="e48e3f141bc2312a579dc37f38e655bf"
                :playlist-data="playlistData"
                ivsplayer-id="ivsplayer001"
              ></ivs-playlist>
        </div>
			
		</div>			
		
		<br>	
		</div>
	
		<script>
			var cpj = document.getElementById("dirnum").innerHTML;
			var newcpj = cpj.replace(/<p style=""><br><\/p>/g, "<p style='display:none'><br></p>");
			var new2cpj = newcpj.replace(/<p><br><\/p>/g, "<p style='display:none'><br></p>");
			document.getElementById("dirnum").innerHTML = new2cpj;
		</script>
	
		<!--  右边  -->
		<div style="width:320px;float:right;margin-top:10px;">
			
		<!-- Rectangle 1 -->	
			<div style="max-width:300px;margin:0 auto;text-align:left;">
			<div>
				<div id='div-gpt-ad-1543077896195-0'><div><span class="ad_words">广告 </span></div>
				<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1543077896195-0');});</script>
			</div>
			</div>
			</div>
			<br/>
		
		<!-- 其他新闻 -->	
		<div align="left" style="width:94%;height:auto;padding:10px;background:#e6e6e6;margin-top:10px;font-weight:bold;">
			<font color="#0b3831" size="+1"><b>其他新闻</b></font>
		</div>				
		<div class="other_news_nw" style="text-align:left;width:94%;margin-bottom:20px;margin-top:10px;">
			
				
		</div>	
			
		<!-- Rectangle 2 -->	
			<div style="max-width:300px;margin:0 auto;text-align:left;">
			<div>
				<div id='div-gpt-ad-1543078174365-0'><div><span class="ad_words">广告 </span></div>
				<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1543078174365-0');});</script>
			</div>
			</div>
			</div>
			<br/>
			
		<!-- 热门新闻  -->
			<script>
			function gethotnewslow(timetpe,ordtpe,gowhere)
			{
				var url = 'https://www.sinchew.com.my/app_if/newArticleHotList';
				$.ajax ({
						url:url,
						data:{
						"siteID":"1",
						"articleType":"1",//2:热门百格视频，1:热门新闻
						"timeType":timetpe,//1:过去6小时，2:过去24小时，3:过去3天
						"orderType":ordtpe,//1:点击量降序排序，2:留言数降序排序，3:分享数降序排序
						"channel":"0"//0:web稿件，1app稿件
						},
						type:'GET',
						async: false,
						cache: true,
						dataType:'text',
						success:function(data)
						{	 
							datalist = JSON.parse(data).list;

							var comparelist = (datalist.length > 6) ? 6 : datalist.length;

							for(var i=0;i<comparelist;i++)
							{
								var vid = i+1;
								var vtitle = datalist[i].title;
								var vcolname = datalist[i].colName;
								var vClick = datalist[i].countClick;
								var vShare = datalist[i].countShare;
								var vDiscuss = datalist[i].countDiscuss;
								var vpubtime = datalist[i].publishtime;	
								var vurl = datalist[i].url;

								if((i+1) == comparelist)
								{
									$("#"+ gowhere).append("<div class='top-view-list'><div class='top-number'>" + vid  + "</div><div class='top-view-title'><a href='" + vurl + "'>" + vtitle + "</a></div></div><div class='remen-more'><a href='https://www.sinchew.com.my/column/node_89.html'>&raquo; 更多热门</a></div>");
								}
								else
								{
									$("#"+ gowhere).append("<div class='top-view-list'><div class='top-number'>" + vid  + "</div><div class='top-view-title'><a href='" + vurl + "'>" + vtitle + "</a></div></div><hr class='top-view-hr'>");
								}


							}
						}
				}) 
			}

			function openmp_top(cityName,changestyle) 
			{
				var i;
				var x = document.getElementsByClassName("w3-container");
				for (i = 0; i < x.length; i++) 
				{
					x[i].style.display = "none";  
				}

				document.getElementById(cityName).style.display = "block";  
				document.getElementById(changestyle).style.color = "white";  
				document.getElementById(changestyle).style.backgroundColor = "#CD2026";

				if(changestyle == "cmview")
				{
					document.getElementById('cmshare').style.color = "#464646";  
					document.getElementById('cmshare').style.backgroundColor = "white";
					document.getElementById('cmcom').style.color = "#464646";
					document.getElementById('cmcom').style.backgroundColor = "white";
				}
				else if(changestyle == "cmshare")
				{
					document.getElementById('cmview').style.color = "#464646";  
					document.getElementById('cmview').style.backgroundColor = "white";
					document.getElementById('cmcom').style.color = "#464646";  
					document.getElementById('cmcom').style.backgroundColor = "white";
				}
				else if(changestyle == "cmcom")
				{
					document.getElementById('cmshare').style.color = "#464646";  
					document.getElementById('cmshare').style.backgroundColor = "white";
					document.getElementById('cmview').style.color = "#464646";
					document.getElementById('cmview').style.backgroundColor = "white";
				}
			}
			</script>

			<style>
			.mp-jingxuan-right
			{
				-webkit-box-sizing: border-box;
				-moz-box-sizing: border-box;
				box-sizing: border-box;
				border:1px solid #CCC;
				display:inline-block;
				vertical-align:top;
				height:500px;
				padding: 5px 15px 10px 15px;
				width:300px;
				position:relative;
				text-align:left;
			}

			.mp-remen-title
			{
				font-size: 22px;
				font-weight: bold;
				padding-bottom: 15px;
				text-align:left;
			}

			.w3-bar-item
			{
				display:inline-block;
				padding: 5px 14px;
				vertical-align:middle;
				cursor: pointer;
			}

			.top-view-title
			{
				display: inline-block;
				padding-left: 15px;
				font-size: 14px;
				vertical-align: middle;
				/* margin-top: -5px; */
				height: 45px;
				display:table-cell;
			}

			.top-view-title a, .top-view-title a:hover
			{
				text-decoration:none;
				color:black;
			}

			.top-number
			{
				font-size:24px;
				color:#CD2026;
				font-weight:bold;
				vertical-align:middle;
				display:table-cell;
			}

			.top-view-list
			{
				width: 90%;
				/* text-align: center; */
				margin: 0 auto;
			}

			.top-view-hr
			{
				margin-top:8px;
				margin-bottom:8px;
				border-color:#cd2026;
			}

			.hr-under-remen
			{
				margin-top:0;
				margin-bottom:8px;
				border-color: #cd2026;
			}

			.w3-bar
			{
				margin: 0 auto;
				text-align: center;
			}

			.w3-container
			{
				padding-top:10px;
			}

			.sline-between
			{
				border-left: 1px solid #cd2026;
				min-width: 1px;
				/* width: 2px; */
				min-height: 15px;
				line-height: 10px;
				/* position: absolute; */
				vertical-align: middle;
				display: inline-block;
			}

			.remen-more
			{
				text-align:right;
				width:100%;
				position: absolute;
				bottom: 1%;
				right: 6%;
			}

			.remen-more a, .remen-more a:hover
			{
				text-decoration:none;
				color:#464646;
			}
			</style>

			<div class="mp-jingxuan-right">
				<div class="mp-remen-title">热门新闻</div>
				<div class="w3-bar w3-black">
					<div class="w3-bar-item" id="cmview" onclick="openmp_top('mview','cmview')" style="background-color:#CD2026;color:white;">最高浏览</div><!--
					--><div class="sline-between"></div><!--
					--><div class="w3-bar-item" id="cmshare" onclick="openmp_top('mshare','cmshare')" style="background-color:white;color:#464646;">最多分享</div><!--
					--><div class="sline-between"></div><!--
					--><div class="w3-bar-item" id="cmcom" onclick="openmp_top('mcom','cmcom')" style="background-color:white;color:#464646;">最多评论</div>
				</div>

				<hr class="hr-under-remen">

				<div id="mview" class="w3-container"></div>
				<script>gethotnewslow(1,1,"mview");</script>

				<div id="mshare" class="w3-container" style="display:none"></div>
				<script>gethotnewslow(3,3,"mshare");</script>

				<div id="mcom" class="w3-container" style="display:none"></div>
				<script>gethotnewslow(3,2,"mcom");</script>
			</div>
			
			
		<!-- Rectangle 3 -->	
			<div style="max-width:300px;margin:0 auto;text-align:left;clear:both;padding-top:15px;">
			<div>
				<div id='div-gpt-ad-1543078238595-0'><div><span class="ad_words">广告 </span></div>
				<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1543078238595-0');});</script>
			</div>
			</div>
			</div>
			<br/>			
		</div>
	
	
		<div style="clear:both;width:1024px;">
			
			<!-- Bottom Banner ROS Ad  -->	
			<div class="ad_words" style="max-width:970px;min-width:728px;margin:0 auto;text-align:center;">
			<div>
			<div id='div-gpt-ad-1542969361433-0'><br/>
				广告<div><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1542969361433-0');});</script></div>
			</div>
			</div>
		  </div>

		<hr class="dneiwen-hr">
	
		<!-- Tenmax 有兴趣的新闻  -->
		<div class="neiwen-interest" style="text-align:left;">你也可能感兴趣...</div>
<div class="_pvmax_recommend" data-widget-id="ba7b862c-9851-4406-b8f0-64b0d9b57413">&nbsp;</div>
					
		<br>
		<hr class="dneiwen-hr">
			

		<br>
					
		<div class="neiwen_comment">
			<div class="comment_left">评论</div>
		</div>
			
		<div class="clearall"><br></div>
		
		<div id="comment_dropdown">
			
				<div id="discuss"></div>
				<script type="text/javascript">
					var discuss = {
						rooturl: "www.sinchew.com.my",
						apiroot: "www.sinchew.com.my/app_if/",
						articleID: "2044619",
						siteID: "1",
						channel: 0
					}
				</script>
				<script charset="utf-8" type="text/javascript" src="https://www.sinchew.com.my/amucsite/discuss/js/discussMain.js"></script>
				<script charset="utf-8" type="text/javascript" src="https://www.sinchew.com.my/amucsite/discuss/js/jquery.reveal.js"></script>
				<link rel="stylesheet" href="https://www.sinchew.com.my/amucsite/discuss/css/reveal.css">

            
            
<!--<script type="text/javascript" src="https://www.sinchew.com.my/amucsite/stat/jquery.min.js"></script>-->
<script type="text/javascript" src="https://www.sinchew.com.my/amucsite/stat/WebClick.js"></script>
<input  type="hidden" id="DocIDforCount" name="DocIDforCount" value="2044619">


		</div>
			
		</div>
</div>
	<!-- footer -->
<!-- GDPR Start  -->

<style>
.eupopup-container {
background-color: rgba(25, 25, 25, 0.9);
color: #efefef;
padding: 5px 20px;
font-size: 14px;
line-height: 1.4em;
text-align: center;
display: none;
z-index: 9999999;
}

.eupopup-container-top,
.eupopup-container-fixedtop {
position: absolute;
top: 0; left: 0; right: 0;
}

.eupopup-container-fixedtop {
position: fixed;
}

.eupopup-container-bottom {
position: fixed;
bottom: 0; left: 0; right: 0;
}

.eupopup-container-bottomleft {
position: fixed;
bottom: 10px;
left: 10px;
width: 300px;
}

.eupopup-container-bottomright {
position: fixed;
bottom: 10px;
right: 10px;
width: 300px;
}

.eupopup-closebutton {
font-size: 16px;
font-weight: 100;
line-height: 1;
color: #a2a2a2;
filter: alpha(opacity=20);
position: absolute;
font-family: helvetica, arial, verdana, sans-serif;
top: 0; right: 0;
padding: 5px 10px;
}
.eupopup-closebutton:hover,
.eupopup-closebutton:active {
color: #fff;
text-decoration: none;
}

.eupopup-head {
font-size: 1.2em;
font-weight: bold;
padding: 7px;
color: #fff;
}

.eupopup-body {
color: #a2a2a2;
}

.eupopup-buttons {
padding: 7px 0 5px 0;
}

.eupopup-button_1 {
color: #ff8533;
font-weight: bold;
font-size: 16px;
        margin-top:10px;
}

.eupopup-button_2 {
color: #f6a21d;
font-weight: normal;
font-size: 12px;
}

.eupopup-button {
margin: 0 10px;
}

.eupopup-button:hover,
.eupopup-button:focus {
text-decoration: underline;
color: #f6a21d;
}

body .eupopup-color-inverse {
color: #000;
background-color: rgba(255, 255, 255, 0.9);
}

body .eupopup-color-inverse .eupopup-head {
color: #000;
}

body .eupopup-style-compact {
text-align: left;
padding: 8px 30px 7px 20px;
line-height: 15px;
}

body .eupopup-style-compact .eupopup-head,
body .eupopup-style-compact .eupopup-body,
body .eupopup-style-compact .eupopup-buttons {
display: inline;
padding: 0;
margin: 0;
}

body .eupopup-style-compact .eupopup-button {
margin: 0 5px;
}
</style>

<script type="text/javascript">
/**
 *
 * JQUERY EU COOKIE LAW POPUPS
 * version 1.1.1
 *
 * Code on Github:
 * https://github.com/wimagguc/jquery-eu-cookie-law-popup
 *
 * To see a live demo, go to:
 * http://www.wimagguc.com/2018/05/gdpr-compliance-with-the-jquery-eu-cookie-law-plugin/
 *
 * by Richard Dancsi
 * http://www.wimagguc.com/
 *
 */

(function($) {

// for ie9 doesn't support debug console >>>
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };
// ^^^

$.fn.euCookieLawPopup = (function() {

var _self = this;

///////////////////////////////////////////////////////////////////////////////////////////////
// PARAMETERS (MODIFY THIS PART) //////////////////////////////////////////////////////////////
_self.params = {
cookiePolicyUrl : 'http://archieve.sinchew.com.my/privacy',
popupPosition : 'bottom',
colorStyle : 'default',
compactStyle : false,
popupTitle : 'This website is using cookies',
popupText : 'We use cookies to ensure that we give you the best experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on this website.',
buttonContinueTitle : '同意',
buttonLearnmoreTitle : '',
buttonLearnmoreOpenInNewWindow : true,
agreementExpiresInDays : 30,
autoAcceptCookiePolicy : false,
htmlMarkup : null
};

///////////////////////////////////////////////////////////////////////////////////////////////
// VARIABLES USED BY THE FUNCTION (DON'T MODIFY THIS PART) ////////////////////////////////////
_self.vars = {
INITIALISED : false,
HTML_MARKUP : null,
COOKIE_NAME : 'EU_COOKIE_LAW_CONSENT'
};

///////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE FUNCTIONS FOR MANIPULATING DATA ////////////////////////////////////////////////////

// Overwrite default parameters if any of those is present
var parseParameters = function(object, markup, settings) {

if (object) {
var className = $(object).attr('class') ? $(object).attr('class') : '';
if (className.indexOf('eupopup-top') > -1) {
_self.params.popupPosition = 'bottom';
}
else if (className.indexOf('eupopup-fixedtop') > -1) {
_self.params.popupPosition = 'fixedtop';
}
else if (className.indexOf('eupopup-bottomright') > -1) {
_self.params.popupPosition = 'bottomright';
}
else if (className.indexOf('eupopup-bottomleft') > -1) {
_self.params.popupPosition = 'bottomleft';
}
else if (className.indexOf('eupopup-bottom') > -1) {
_self.params.popupPosition = 'bottom';
}
else if (className.indexOf('eupopup-block') > -1) {
_self.params.popupPosition = 'block';
}
if (className.indexOf('eupopup-color-default') > -1) {
_self.params.colorStyle = 'default';
}
else if (className.indexOf('eupopup-color-inverse') > -1) {
_self.params.colorStyle = 'inverse';
}
if (className.indexOf('eupopup-style-compact') > -1) {
_self.params.compactStyle = true;
}
}

if (markup) {
_self.params.htmlMarkup = markup;
}

if (settings) {
if (typeof settings.cookiePolicyUrl !== 'undefined') {
_self.params.cookiePolicyUrl = settings.cookiePolicyUrl;
}
if (typeof settings.popupPosition !== 'undefined') {
_self.params.popupPosition = settings.popupPosition;
}
if (typeof settings.colorStyle !== 'undefined') {
_self.params.colorStyle = settings.colorStyle;
}
if (typeof settings.popupTitle !== 'undefined') {
_self.params.popupTitle = settings.popupTitle;
}
if (typeof settings.popupText !== 'undefined') {
_self.params.popupText = settings.popupText;
}
if (typeof settings.buttonContinueTitle !== 'undefined') {
_self.params.buttonContinueTitle = settings.buttonContinueTitle;
}
if (typeof settings.buttonLearnmoreTitle !== 'undefined') {
_self.params.buttonLearnmoreTitle = settings.buttonLearnmoreTitle;
}
if (typeof settings.buttonLearnmoreOpenInNewWindow !== 'undefined') {
_self.params.buttonLearnmoreOpenInNewWindow = settings.buttonLearnmoreOpenInNewWindow;
}
if (typeof settings.agreementExpiresInDays !== 'undefined') {
_self.params.agreementExpiresInDays = settings.agreementExpiresInDays;
}
if (typeof settings.autoAcceptCookiePolicy !== 'undefined') {
_self.params.autoAcceptCookiePolicy = settings.autoAcceptCookiePolicy;
}
if (typeof settings.htmlMarkup !== 'undefined') {
_self.params.htmlMarkup = settings.htmlMarkup;
}
}

};

var createHtmlMarkup = function() {

if (_self.params.htmlMarkup) {
return _self.params.htmlMarkup;
}

var html =
'<div class="eupopup-container' +
    ' eupopup-container-' + _self.params.popupPosition +
    (_self.params.compactStyle ? ' eupopup-style-compact' : '') +
' eupopup-color-' + _self.params.colorStyle + '">' +
'<div class="eupopup-head">' + _self.params.popupTitle + '</div>' +
'<div class="eupopup-body">' + _self.params.popupText + '</div>' +
'<div class="eupopup-buttons">' +
  '<a href="#" class="eupopup-button eupopup-button_1">' + _self.params.buttonContinueTitle + '</a>' +
  '<a href="' + _self.params.cookiePolicyUrl + '"' +
 	(_self.params.buttonLearnmoreOpenInNewWindow ? ' target=_blank ' : '') +
' class="eupopup-button eupopup-button_2">' + _self.params.buttonLearnmoreTitle + '</a>' +
  '<div class="clearfix"></div>' +
'</div>' +
'<a href="#" class="eupopup-closebutton">x</a>' +
'</div>';

return html;
};

// Storing the consent in a cookie
var setUserAcceptsCookies = function(consent) {
var d = new Date();
var expiresInDays = _self.params.agreementExpiresInDays * 24 * 60 * 60 * 1000;
d.setTime( d.getTime() + expiresInDays );
var expires = "expires=" + d.toGMTString();
document.cookie = _self.vars.COOKIE_NAME + '=' + consent + "; " + expires + ";path=/";

$(document).trigger("user_cookie_consent_changed", {'consent' : consent});
};

// Let's see if we have a consent cookie already
var userAlreadyAcceptedCookies = function() {
var userAcceptedCookies = false;
var cookies = document.cookie.split(";");
for (var i = 0; i < cookies.length; i++) {
var c = cookies[i].trim();
if (c.indexOf(_self.vars.COOKIE_NAME) == 0) {
userAcceptedCookies = c.substring(_self.vars.COOKIE_NAME.length + 1, c.length);
}
}

return userAcceptedCookies;
};

var hideContainer = function() {
// $('.eupopup-container').slideUp(200);
$('.eupopup-container').animate({
opacity: 0,
height: 0
}, 200, function() {
$('.eupopup-container').hide(0);
});
};

///////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC FUNCTIONS  //////////////////////////////////////////////////////////////////////////
var publicfunc = {

// INITIALIZE EU COOKIE LAW POPUP /////////////////////////////////////////////////////////
init : function(settings) {

parseParameters(
$(".eupopup").first(),
$(".eupopup-markup").html(),
settings);

// No need to display this if user already accepted the policy
if (userAlreadyAcceptedCookies()) {
        $(document).trigger("user_cookie_already_accepted", {'consent': true});
return;
}

// We should initialise only once
if (_self.vars.INITIALISED) {
return;
}
_self.vars.INITIALISED = true;

// Markup and event listeners >>>
_self.vars.HTML_MARKUP = createHtmlMarkup();

if ($('.eupopup-block').length > 0) {
$('.eupopup-block').append(_self.vars.HTML_MARKUP);
} else {
$('BODY').append(_self.vars.HTML_MARKUP);
}

$('.eupopup-button_1').click(function() {
setUserAcceptsCookies(true);
hideContainer();
return false;
});
$('.eupopup-closebutton').click(function() {
setUserAcceptsCookies(true);
hideContainer();
return false;
});
// ^^^ Markup and event listeners

// Ready to start!
$('.eupopup-container').show();

// In case it's alright to just display the message once
if (_self.params.autoAcceptCookiePolicy) {
setUserAcceptsCookies(true);
}

}

};

return publicfunc;
});

$(document).ready( function() {
if ($(".eupopup").length > 0) {
$(document).euCookieLawPopup().init({
'info' : 'YOU_CAN_ADD_MORE_SETTINGS_HERE',
'popupTitle' : '',
'popupText' : '我们使用cookies来了解您如何使用和浏览我们的网站，并藉此提升您的使用体验，包括个性化的内容和广告。<br/>如您继续使用和浏览我们的网站，即表示您同意我们以上使用cookies的目的、更新的<a style="color:yellow;" href="http://archive.sinchew.com.my/privacy" target="blank">隐私条款</a> 和 <a style="color:yellow;" href="http://archieve.sinchew.com.my/terms" target="blank">使用条款</a>。'
});
}
});

$(document).bind("user_cookie_consent_changed", function(event, object) {
console.log("User cookie consent changed: " + $(object).attr('consent') );
});

}(jQuery));
</script>

<div class="eupopup eupopup-top"></div>


<!-- GDPR End  -->


<style>
.footer_title, .footer_title:hover
{
text-decoration:none;
color:#cd2026;
}

.footer_middle_link, .footer_middle_link:hover
{
text-decoration:none;
color:#464646;
}

.footer_lanmu_title, .footer_lanmu_title:hover
{
text-decoration:none;
color:#464646;
}
</style>

<div class="footer_container">
<div class="footer_adjust_size">
<div class="footer_lanmu">

<a class="footer_lanmu_title" href="https://www.sinchew.com.my/nation.html">全国</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/world.html">国际</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/business.html">财经</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/opinion.html">言路</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/mykampung.html">地方</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/entertainment.html">娱乐</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/sport.html">体育</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/life.html">副刊</a>
<a class="footer_lanmu_title" href="https://www.sinchew.com.my/pocketimes.html">百格</a>
<a class="footer_lanmu_title" href="http://classifieds.sinchew.com.my/content.phtml" target="blank">分类</a>

</div>

<br/>

<div class="footer_middle">
<div class="footer_middle_logo">
<img src="https://cdnpuc.sinchew.com.my/resource/footerlogo.png" width="183" height="120" alt=""/> 
<br/><br/>
<font size="2rem">世华多媒体有限公司◆版权所有◆不得转载<br/>
Copyright © 2019 MCIL Multimedia Sdn Bhd (515740-D). All rights reserved.</font>
</div>

<div class="footer_middle_first">
<span style="font-weight:bold;font-size:20px;color:#cd2026;">星洲集团</span>
<hr style="height: 1px;background-color:#afafaf;margin-top:10px;margin-bottom:10px;">
<a class="footer_middle_link" href="http://epaper.sinchew.my" target="_blank"><font size="3rem">星洲电子报</font></a><br/> 
<a class="footer_middle_link" href="http://www.mediachinese.com/" target="_blank"><font size="3rem">世华网</font></a><br/>
<a class="footer_middle_link" href="https://www.sinchew.com.my/sarawak.html" target="_blank"><font size="3rem">砂拉越星洲日报</font></a><br/>
<a class="footer_middle_link" href="http://www.guangming.com.my/" target="_blank"><font size="3rem">光明日报</font></a><br/>
<a class="footer_middle_link" href="http://indonesia.sinchew.com.my" target="_blank"><font size="3rem">印尼星洲日报</font></a><br/> 
<a class="footer_middle_link" href="http://www.mysinchew.com/" target="_blank"><font size="3rem">MySinchew</font></a><br/> 
<a class="footer_middle_link" href="http://www.logon.my/" target="_blank"><font size="3rem">Logon Online Shopping</font></a><br/>
<a class="footer_middle_link" href="http://www.xuehaiblog.com/" target="_blank"><font size="3rem">学海</font></a><br/>
<a class="footer_middle_link" href="http://archive.sinchew.com.my/node/1705896" target="_blank"><font size="3rem">小星星</font></a>
</div>

<div class="footer_middle_second">
<span style="font-weight:bold;font-size:20px;color:#cd2026;">友情连接</span>
<hr style="height: 1px;background-color:#afafaf;margin-top:10px;margin-bottom:10px;">
<a class="footer_middle_link" href="https://www.chinapress.com.my" target="_blank"><font size="3rem">中国报</font></a><br/>
<a class="footer_middle_link" href="https://www.enanyang.my" target="_blank"><font size="3rem">e南洋商报</font></a><br/> 
<a class="footer_middle_link" href="https://www.yunnan.cn" target="_blank"><font size="3rem">云南网</font> </a><br/>
<a class="footer_middle_link" href="https://www.asianews.network/" target="_blank"><font size="3rem">ANN ASIAN NEWSNETWORK</font></a><br/>
<a class="footer_middle_link" href="https://www.rhhotels.com.my/" target="_blank"><font size="3rem">RH Hotel, sibu, sarawak</font></a><br/>
<!--<a class="footer_middle_link" href="https://news.sinchew.com.my/topic/taxonomy/term/255" target="_blank"><font size="3rem">爱家</font></a>-->
</div>

<div class="footer_middle_third">
<a class="footer_title" href="http://lifemagazines.com.my/" target="_blank">生活杂志</a>
<hr style="height: 1px;background-color:#afafaf;margin-top:10px;margin-bottom:10px;">
<a class="footer_middle_link" href="https://www.newtide.com.my/" target="_blank"><font size="3rem">新潮</font></a><br/>
<a class="footer_middle_link" href="http://feminine.com.my/my-wellness/" target="_blank"><font size="3rem">My Wellness</font></a><br/>
<a class="footer_middle_link" href="http://feminine.com.my/" target="_blank"><font size="3rem">风采</font></a><br/>
<a class="footer_middle_link" href="http://feminine.com.my/oriental-cuisine/" target="_blank"><font size="3rem">美味风采</font></a>
</div>
 
<div class="footer_middle_fourth">
<a class="footer_title" href="http://archive.sinchew.com.my/intro" target="_blank">关于我们</a>	
<hr style="height: 1px;background-color:#afafaf;margin-top:10px;margin-bottom:10px;">
<a class="footer_middle_link" href="http://archive.sinchew.com.my/other/intro/index.php" target="_blank"><font size="3rem">公司简介</font></a><br/> 
<a class="footer_middle_link" href="http://archive.sinchew.com.my/feedback " target="_blank"><font size="3rem">联络我们</font></a><br/> 
<a class="footer_middle_link" href="https://www.sinchew.com.my/job.html"><font size="3rem">求才若渴</font></a><br/>
<a class="footer_middle_link" href="http://archive.sinchew.com.my/faq" target="_blank"><font size="3rem">常见问题</font></a><br/>
<a class="footer_middle_link" href="https://www.sinchew.com.my/SubscriptionPress" target="_blank"><font size="3rem">订阅服务</font></a><br/>
<a class="footer_middle_link" href="https://www.sinchew.com.my/ratecard" target="_blank"><font size="3rem">刊登广告</font></a><br/>
<a class="footer_middle_link" href="http://archive.sinchew.com.my/Disclaimer" target="_blank"><font size="3rem">Disclaimer</font></a><br/> 
<a class="footer_middle_link" href="http://archive.sinchew.com.my/privacy" target="_blank"><font size="3rem">私隐政策声明</font></a><br/>
<a class="footer_middle_link" href="http://archive.sinchew.com.my/terms" target="_blank"><font size="3rem">使用者条例</font></a>
</div>
</div>

<div style="clear:both;"></div>

<hr style="height:1px;background-color:#afafaf;margin-top:10px;margin-bottom:10px;">

<div class="footer_social_app">
<div class="footer_social">
<a href="https://www.sinchew.com.my/intro" class="footer_title" target="_blank">关注我们</a>
<br/><br/>
<a href="https://www.facebook.com/SinChewDaily/" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-fb.png"></a>
<a href="https://twitter.com/SinChewPress" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-twitter.png"></a>
<a href="https://www.youtube.com/channel/UCKC1q1zc_CG2MtOCEHNUt1Q" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-youtube.png"></a>
<a href="https://www.instagram.com/sinchewdaily/" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-instagram.png"></a>
<a href="https://www.weibo.com/sinchewi" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-weibo.png"></a>
<a href="http://archive.sinchew.com.my/wechat" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-wechat.png"></a>
</div>

<div class="footer_app">
<a href="http://archive.sinchew.com.my/intro" class="footer_title">现在就下载星洲网APP</a>
<br><br>
<a href="https://itunes.apple.com/my/app/sin-chew-%E6%98%9F%E6%B4%B2%E6%97%A5%E6%8A%A5/id1077727843?mt=8"><img src="https://cdnpuc.sinchew.com.my/resource/apple-app.png"></a>
<a href="https://play.google.com/store/apps/details?id=com.sinchewnews&hl=en_US"><img src="https://cdnpuc.sinchew.com.my/resource/google-app.png"></a>
</div>
</div>
<div style="clear:both;padding-bottom:10px;"></div>
</div><!-- size class -->
</div>

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "10028553" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>

<noscript>
  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=10028553&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WC3H2D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

{!! $options['{v3_script_tag}'] !!}
</body>

</html>
