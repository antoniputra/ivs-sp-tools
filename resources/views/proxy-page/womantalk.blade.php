<html lang="en"><head>

    <title>KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa - Womantalk</title>
  <script src="https://securepubads.g.doubleclick.net/gpt/pubads_impl_rendering_2019061301.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vfll-F3yY/www-widgetapi.js" async=""></script><script type="text/javascript" async="" src="https://d31qbv1cthcecs.cloudfront.net/atrk.js"></script><script src="//www.youtube.com/iframe_api"></script><script async="" src="https://sb.scorecardresearch.com/beacon.js"></script><script src="https://connect.facebook.net/signals/plugins/inferredEvents.js?v=2.8.51" async=""></script><script src="https://connect.facebook.net/signals/config/1506122303025458?v=2.8.51&amp;r=stable" async=""></script><script async="" src="https://connect.facebook.net/en_US/fbevents.js"></script><script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script><script src="https://connect.facebook.net/en_US/sdk.js?hash=7eb1e77785ca3ac2bc9f57161b19db4c&amp;ua=modern_es6" async="" crossorigin="anonymous"></script><script id="facebook-jssdk" src="//connect.facebook.net/en_US/sdk.js"></script><script async="" src="https://www.googletagmanager.com/gtm.js?id=GTM-MZ97CPR"></script><script>
dataLayer = [{
  'content_title': 'KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa',
  'content_source': window.location.hostname,
  'content_category': 'video',
  'content_subcategory': 'Nutrition',
  'content_creator': 'katharina.menge',
  'creator_details': 'Writer',
  'content_type': 'Text',
  'content_length': 361,
  'content_publisheddate': '21-05-2019 19.00',
  'article_tags': 'good lifestyle, nutrition, kepop, puasa',
  'user_id': 'null'
}]
</script>






  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-MZ97CPR');</script>

  
  <!-- End Google Tag Manager -->
  <meta charset="utf-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width, initial-scale=1, user-scalable=0, minimum-scale=1, maximum-scale=1" name="viewport">
  <link rel="stylesheet" href="https://womantalk.com/web/vendor/grid-horizon.css" type="text/css"><link rel="stylesheet" href="https://womantalk.com/web/vendor/grid.css" type="text/css"><link rel="stylesheet" href="https://womantalk.com/web/vendor/carousel.css" type="text/css"><link href="/web/assets/icon/favicon.ico" rel="icon" type="image/x-icon">

  <link href="https://womantalk.com/web/css/all-min.css?v=1.1.3.07052018" rel="stylesheet" type="text/css">
  
    

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
  
  <script async="" src="https://womantalk.com/web/vendor/masonry.pkgd.min.js"></script>
  <script src="https://womantalk.com/web/vendor/imagesloaded.pkgd.min.js"></script>
  
  <script src="https://womantalk.com/web/script.js?v=1.1.3.20190208"></script>
  <script src="https://womantalk.com/web/lazyload.js?v=1.1.3.20190219"></script>

  <meta name="_csrf_header" content="X-CSRF-TOKEN">
  <meta name="_csrf" content="c8573ec0-30c0-4b22-918f-60ce7d445025">

  

  
  
  
  <meta name="description" content="Ketty mencoba puasa untuk pertama kalinya dan ini yang terjadi!">
  
  
  
  
  
  <noscript></noscript><style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_dialog_advanced{border-radius:8px;padding:10px}.fb_dialog_content{background:#fff;color:#373737}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{left:5px;right:auto;top:5px}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{height:100%;left:0;margin:0;overflow:visible;position:absolute;top:-10000px;transform:none;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{background:none;height:auto;min-height:initial;min-width:initial;width:auto}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{clear:both;color:#fff;display:block;font-size:18px;padding-top:20px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .4);bottom:0;left:0;min-height:100%;position:absolute;right:0;top:0;width:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_mobile .fb_dialog_iframe{position:sticky;top:0}.fb_dialog_content .dialog_header{background:linear-gradient(from(#738aba), to(#2c4987));border-bottom:1px solid;border-color:#1d3c78;box-shadow:white 0 1px 1px -1px inset;color:#fff;font:bold 14px Helvetica, sans-serif;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:linear-gradient(from(#4267B2), to(#2a4887));background-clip:padding-box;border:1px solid #29487d;border-radius:3px;display:inline-block;line-height:18px;margin-top:3px;max-width:85px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{background:none;border:none;color:#fff;font:bold 12px Helvetica, sans-serif;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #4a4a4a;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f5f6f7;border:1px solid #4a4a4a;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-position:50% 50%;background-repeat:no-repeat;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}</style><link rel="preload" href="https://adservice.google.co.id/adsid/integrator.js?domain=womantalk.com" as="script"><script type="text/javascript" src="https://adservice.google.co.id/adsid/integrator.js?domain=womantalk.com"></script><link rel="preload" href="https://adservice.google.com/adsid/integrator.js?domain=womantalk.com" as="script"><script type="text/javascript" src="https://adservice.google.com/adsid/integrator.js?domain=womantalk.com"></script><script src="https://securepubads.g.doubleclick.net/gpt/pubads_impl_2019061301.js" async=""></script><link rel="prefetch" href="https://tpc.googlesyndication.com/safeframe/1-0-33/html/container.html"></head><body class="background-white" style="min-height: 100%; display: flex; flex-direction: column"><img alt="" height="1" src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=gXDCp1IWh910fn" style="display:none" width="1">

    <meta content="1496803663950240" property="fb:app_id">

    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">

    <meta content="telephone=no" name="format-detection">

    <meta content="summary_large_image" name="twitter:card">

    <meta content="@womantalk_com" name="twitter:site">

    <meta type="title" content="KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa">

    <meta name="keywords" content="puasa, puasa ramadan, mencoba puasa, efek puasa, efek puasa bagi tubuh, 13 jam puasa, ini efek puasa bagi tubuh, efek puasa bagi tubuh menurut pakar">

    <link rel="canonical" href="https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj">

    <meta content="website" property="og:type">

    <meta content="Womantalk.com | Good lifestyle, pop culture, and good relationship for Indonesian women" itemprop="image" property="og:site_name">

    <meta content="id_ID" property="og:locale">

    <meta property="og:url" content="https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj">

    <meta property="og:title" content="KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa">

    <meta property="og:description" content="Ketty mencoba puasa untuk pertama kalinya dan ini yang terjadi!">

    <meta property="og:image" content="https://d3hctp6gkh4e3f.cloudfront.net/prod/5e3vjrsj5hocw81af87s">

    <meta content="summary_large_image" name="twitter:card">

    <meta content="@womantalk_com" name="twitter:site">

    <meta name="twitter:creator" content="katharina.menge">

    <meta name="twitter:title" content="KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa">

    <meta name="twitter:description" content="Ketty mencoba puasa untuk pertama kalinya dan ini yang terjadi!">

    <meta name="twitter:image" content="https://d3hctp6gkh4e3f.cloudfront.net/prod/5e3vjrsj5hocw81af87s">

    

    

    

    

    

    

    

    

    

    

    

    


  <!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe height="0" src="https://www.googletagmanager.com/ns.html?id=GTM-MZ97CPR" style="display:none;visibility:hidden" width="0"></iframe>
    
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="full-height" style="flex-grow: 1">
      <div class="full-height" onclick="void(0)">
          <div class="section-top">
              <header id="header">


<div class="nav-wrapper" id="nav-wrapper">
  <div class="main-header" style="max-width: 1250px; margin: 0 auto; position: relative">
    <div class="header-wt-logo clickable">
      <a href="/">
        <img alt="womantalk" src="https://womantalk.com/web/assets/img/img_logo_hitam.png">
      </a>
    </div>
    <div class="pull-right no-burger">
      <div class="header-main-menu"><div><a href="https://womantalk.com/relationship/articles">RELATIONSHIP</a></div><div><a href="https://womantalk.com/entertainment/articles">ENTERTAINMENT</a></div><div><a href="https://womantalk.com/good-lifestyle/articles">GOOD LIFESTYLE</a></div><div><a href="https://womantalk.com/newest/lounges">WOMAN LOUNGE</a></div><div><a href="https://womantalk.com/perempuanbisa">PEREMPUANBISA</a></div><div class="relative color-white">
                      <img class="header-portal clickable" onclick="togglePortalChannel()" src="https://dcywrb6nqrsdp.cloudfront.net/icon/channel.svg">
                      <div class="header-portal-channel collapse muli"><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/9/?target=opini.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Opini" src="//s.kaskus.id/images/2016/11/29/6498705_201611291043150120.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/21/?target=bolalob.com&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Bolalob" src="//s.kaskus.id/images/2016/11/29/6498705_201611291043400966.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/33/?target=www.kaskus.co.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Kaskus" src="//s.kaskus.id/images/2016/11/29/6498705_201611291047400692.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/39/?target=beritagar.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Beritagar" src="//s.kaskus.id/images/2017/01/11/4501355_201701110710070180.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/42/?target=www.kincir.com&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Kincir" src="//s.kaskus.id/images/2017/03/06/6498705_201703060945090923.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/45/?target=garasi.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Garasi" src="//s.kaskus.id/images/2017/08/23/51917_201708231035140391.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/48/?target=app.adjust.com&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Kurio" src="//s.kaskus.id/images/2017/11/28/4501355_201711280443200972.png">
                    </a>
                  </li><li style="border-bottom: 1px solid #dedede;" class="portal-images">
                    <a href="https://widget.kaskus.co.id/kaskusnetworks/r/51/?target=historia.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" target="_blank" rel="noopener" title="">
                      <img alt="Historia" src="//s.kaskus.id/images/2018/03/01/4501355_201803010731100208.png">
                    </a>
                  </li><li class="portal-images">
                    <a href="http://bobotoh.id/?utm_campaign=GDP&amp;utm_source=womantalk.com&amp;utm_medium=channel" target="_blank" rel="noopener" title="">
                      <img alt="Bobotoh" src="https://s3-ap-southeast-1.amazonaws.com/album.bunda/portal.image/Bobotoh.png">
                    </a>
                  </li></div>
                      .
                    </div></div>
      <div class="header-search" onclick="toggleHeaderSearchOverlay();">
        <img src="https://dcywrb6nqrsdp.cloudfront.net/icon/search.svg">
      </div>
      
      <div class="header-user-info muli collapse" style="right: -95px;">
        <div>
          <div class="header-user-name-container">
            <div class="header-user-fullname">
              <a href="">
                
              </a>
            </div>
            <div class="header-user-username">
              <a href="">
                
              </a>
            </div>
          </div>
          <div class="header-user-profile">
            <a href="">
              LIHAT PROFIL
            </a>
          </div>
          <div class="clearfix p-bottom-20"></div>

          <div class="header-user-verification-message">
            <div class="verify-cta hide" onclick="showVerificationPopUp()">
              Verifikasi email kamu! Klik disini!
            </div>
            <div class="complete-cta hide">
              <div class="complete-cta-text">
                <a href="">
                  Lengkapi profile dan dapatkan <span>50</span> point!
                </a>
              </div>
              <div class="complete-cta-bar">
                <div style="width: 75%">
                  
                </div>
              </div>
              <div style="position: relative; float: left; top: -23px;">
                Profil <span class="complete-cta-amount">75</span>% lengkap
              </div>
            </div>
          </div>
        </div>

        <div class="header-user-notif">
          <a href="">
            NOTIFICATION
            <span class="wt-notif">( </span>
            <span class="collapse user-notif-count" id="user-notif-count">8</span>
            <span class="wt-notif">) </span>
          </a>
        </div>
        <div class="header-user-invite-friend">
          <a href="">
            UNDANG TEMAN
          </a>
        </div>
        <div class="header-user-gpoin">
          <a href="">
            GPOIN
            <span>New</span>
          </a>
        </div>

        <div class="header-user-setting">
          <a href="">
            PENGATURAN
          </a>
        </div>
        <div class="header-user-signout clickable" onclick="logoutEventHandler()">
          KELUAR
        </div>
        <form action="/logout" id="" method="POST" style="display: none">
          <input type="hidden" name="_csrf" value="c8573ec0-30c0-4b22-918f-60ce7d445025">
          <button id="logout-button-1" type="submit">KELUAR</button>
        </form>
      </div>
      <div class="header-user logged-out">
        <a href="/login" style="color: #000000">
          Login
        </a>
      </div>
    </div>
    <div class="pull-right burger-only">
      <span onclick="toggleHeaderSearchOverlay();">
        <img alt="" class="search-search" src="https://dcywrb6nqrsdp.cloudfront.net/icon/search.svg">
        <img alt="" class="search-close collapse" src="https://dcywrb6nqrsdp.cloudfront.net/icon/close.svg">
      </span>
      <span onclick="toggleBurgerMenuOverlay()">
        <img alt="" class="burger-icon" src="https://dcywrb6nqrsdp.cloudfront.net/icon/burger.svg">
        <img alt="" class="close-icon collapse" src="https://dcywrb6nqrsdp.cloudfront.net/icon/close.svg">
      </span>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="secondary-nav-wrapper">
  <ul>
    <li>HOT TOPIC</li>
  <li>
                        <a href="https://womantalk.com/article/tag/cumbu-iin">CUMBU IIN</a>
                      </li><li>
                        <a href="https://womantalk.com/article/tag/BTS">BTS</a>
                      </li><li>
                        <a href="https://womantalk.com/search/result?keyword=blackpink&amp;current=topic">BLACKPINK</a>
                      </li><li>
                        <a href="https://womantalk.com/article/tag/royal-family">ROYAL FAMILY</a>
                      </li><li>
                        <a href="https://womantalk.com/article/tag/WT%20blog">WT BLOG</a>
                      </li><li>
                        <a href="https://womantalk.com/article/tag/kepop">KEPOP</a>
                      </li></ul>
</div>
<!-- <div class="secondary-nav-wrapper">
  <div class="secondary-header">
    <div style="max-width: 1250px; margin: 0 auto">
      <div class="secondary-header-scrollable header-popular-menu">
        <div class="secondary-header-title">HOT TOPIC</div>
      </div>
    </div>
  </div>
</div> -->

<div class="header-search-overlay collapse muli">
  <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
  <input name="fakeusernameremembered" style="display:none" type="text">
  <input name="fakepasswordremembered" style="display:none" type="password">
  <div>
    <div class="close-button no-burger clickable" onclick="toggleHeaderSearchOverlay()">
      CLOSE <span style=""><img src="https://dcywrb6nqrsdp.cloudfront.net/icon/close.svg"></span>
    </div>
    <div class="header-search-container">
      <div class="header-search-text">
        SEARCH
      </div>
      <input class="header-search-box" placeholder="Cari Di Sini" type="text">
      <span class="header-search-search-icon clickable"><img onclick="startSearch(this)" src="https://dcywrb6nqrsdp.cloudfront.net/icon/search.svg"></span>
      <hr>
    </div>
    <div>
      <div class="header-search-top-title">
        POPULAR SEARCH
      </div>
      <div class="header-search-top-content"><div>
                          <a href="/search/result?keyword=Diet&amp;current=topic">Diet</a>
                        </div><div>
                          <a href="/search/result?keyword=Ramalan Zodiak Minggu Ini&amp;current=topic">Ramalan Zodiak Minggu Ini</a>
                        </div><div>
                          <a href="/search/result?keyword=PerempuanBisa&amp;current=topic">PerempuanBisa</a>
                        </div><div>
                          <a href="/search/result?keyword=Cumbu Iin&amp;current=topic">Cumbu Iin</a>
                        </div><div>
                          <a href="/search/result?keyword=Meghan Markle&amp;current=topic">Meghan Markle</a>
                        </div></div>
    </div>

  </div>
</div>
<div class="header-burger-overlay collapse">
  <div class="burger-login logged-out">
    <div class="login-button">
      <a href="/login" style="color: #000000">
        Login
      </a>
    </div>
    <div>
      <img alt="" onclick="headerLoginFb()" src="https://dcywrb6nqrsdp.cloudfront.net/icon/share-fb.svg">
    </div>
    <div>
      <img alt="" onclick="headerLoginTw()" src="https://dcywrb6nqrsdp.cloudfront.net/icon/share-tw.svg">
    </div>
    <hr>
  </div>
  
  <div class="burger-item"><a href="https://womantalk.com/relationship/articles">
                          <div class="clearfix m-bottom-30">
                            RELATIONSHIP<span class="pull-right"><img class="flip" src="https://dcywrb6nqrsdp.cloudfront.net/icon/arrow.svg"></span>
                          </div>
                       </a><a href="https://womantalk.com/entertainment/articles">
                          <div class="clearfix m-bottom-30">
                            ENTERTAINMENT<span class="pull-right"><img class="flip" src="https://dcywrb6nqrsdp.cloudfront.net/icon/arrow.svg"></span>
                          </div>
                       </a><a href="https://womantalk.com/good-lifestyle/articles">
                          <div class="clearfix m-bottom-30">
                            GOOD LIFESTYLE<span class="pull-right"><img class="flip" src="https://dcywrb6nqrsdp.cloudfront.net/icon/arrow.svg"></span>
                          </div>
                       </a><a href="https://womantalk.com/newest/lounges">
                          <div class="clearfix m-bottom-30">
                            WOMAN LOUNGE<span class="pull-right"><img class="flip" src="https://dcywrb6nqrsdp.cloudfront.net/icon/arrow.svg"></span>
                          </div>
                       </a><a href="https://womantalk.com/perempuanbisa">
                          <div class="clearfix m-bottom-30">
                            PEREMPUANBISA<span class="pull-right"><img class="flip" src="https://dcywrb6nqrsdp.cloudfront.net/icon/arrow.svg"></span>
                          </div>
                       </a><hr>
                      <span onclick="togglePortalChannel('mobile')" class="m-bottom-15 burger-channel">
                        <div class="clearfix m-bottom-30" style="padding-bottom: 60px;">
                          CHANNEL<span class="pull-right"><img class="flip-up" src="https://dcywrb6nqrsdp.cloudfront.net/icon/arrow.svg"></span>
                          <div class="header-portal-channel-mobile collapse muli m-top-15"><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/9/?target=opini.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Opini</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/21/?target=bolalob.com&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Bolalob</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/33/?target=www.kaskus.co.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Kaskus</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/39/?target=beritagar.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Beritagar</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/42/?target=www.kincir.com&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Kincir</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/45/?target=garasi.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Garasi</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/48/?target=app.adjust.com&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Kurio</a></li><li class="m-bottom-15">
                            <a href="https://widget.kaskus.co.id/kaskusnetworks/r/51/?target=historia.id&amp;utm_source=womantalk.com&amp;utm_campaign=kaskus-networks&amp;utm_medium=header_widget" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Historia</a></li><li class="m-bottom-15">
                            <a href="http://bobotoh.id/?utm_campaign=GDP&amp;utm_source=womantalk.com&amp;utm_medium=channel" class="menu-burger-item" style="color:#4c4c4c!important;" rel="noopener">Bobotoh</a></li></div>
                        </div>
                      </span></div>
</div>

<div class="header-toast-message" id="toast-message"><div>warning!</div></div>

<script src="https://womantalk.com/web/js/main/header.js?v=1.1.3.07052018" type="text/javascript"></script>
</header>


          </div>
          <div class="centering-layout content" style="font-family: 'Muli', sans-serif;">
      <div class="article-body-container video" id="article-detail-container"><div id="article-1-divider"></div>
            <div id="article-27777" style="min-height: 100vh"><div class="main-content-body"><div class="wrapper-article no-padding-bottom article-body"><div style="display: none" id="article-img-header-27777" src="https://d3hctp6gkh4e3f.cloudfront.net/prod/5e3vjrsj5hocw81af87s"></div><div class=""><div class="video-container" style="margin-top: 110px"><iframe class="video-youtube" width="100%" src="https://www.youtube.com/embed/SUCcnDd59ko?autoplay=0;rel=0&amp;controls=1&amp;showinfo=0&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fwomantalk.com" frameborder="0" encrypted-media"="" allowfullscreen="" data-gtm-yt-inspected-6246307_92="true" id="227588867"></iframe></div><div class="article-video-body"><div class="categories"><a onclick="eventClickNonInteraction('Event Category','Topic Stream')" style="color:#67c125!important; font-size: 0.8em; margin-top: 5px;" href="/good-lifestyle/articles">GOOD LIFESTYLE</a><span style="line-height: 0.8em; font-size: 1em; margin: 0 5px; color: #67c125">•</span><a class="" id="article-category-27777" onclick="eventClickNonInteraction('Event Category','Topic Stream')" style="color:#67c125!important; font-size: 0.8em; margin-top: 5px" href="/nutrition/articles">NUTRITION</a></div><div class="title" id="article-title-27777" style="padding-top: 5px; margin-bottom: 8px; line-height: 2.2em" data-id="27777"><h1>KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa </h1></div><div><div id="time-published-27777" class="time-emote video-time-emote" style="font-size: 10px">4 minggu lalu, 19:00</div><div class="video-share-wrapper"><div class="video-share">
              <div class="video-share-item video-like" onclick="likeVideo(this)" data-id="27777" data-status="0" data-total="3">
                <img style="width: 30px; height: 31px" alt="like video" src="https://womantalk.com/web/assets/icon/bookmark/like-circle.svg" class="">
              </div>
              <div class="video-share-item video-bookmark" onclick="bookmarkVideo(this)" data-id="27777" data-status="false">
                  <img style="width: 30px; height: 31px" alt="add bookmark" src="https://womantalk.com/web/assets/icon/bookmark/bookmark-circle.svg" class="">
              </div>
              <div id="video-share-27777" style="display: inline-block; margin: 0px" class="relative"><div id="video-share-fb" class="video-share-item fb" data-target="fb" data-texttoshare="">
              <img alt="share fb" src="https://womantalk.com/web/assets/img/helaw-mantan/share-fb.svg" class="">
            </div><div id="video-share-line" class="video-share-item line no-web" data-target="line" data-texttoshare="https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj">
              <img alt="share line" src="https://womantalk.com/web/assets/img/helaw-mantan/share-line.svg" class="">
            </div><div id="video-share-cp" class="video-share-item copy" data-target="copy" data-texttoshare="">
              <img alt="share copy" src="https://womantalk.com/web/assets/img/helaw-mantan/share-link.svg" class="">
            </div><div id="video-share-wa" class="video-share-item wa no-web" data-target="wa" data-texttoshare="https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj">
              <img alt="share wa" src="https://womantalk.com/web/assets/img/helaw-mantan/share-wa.svg" class="">
            </div><div id="video-share-tw" class="video-share-item tw" data-target="tw" data-texttoshare="KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj">
              <img alt="share tw" src="https://womantalk.com/web/assets/img/helaw-mantan/share-tw.svg" class="">
            </div><div id="video-share-bbm" class="video-share-item bbm no-web" data-target="bbm" data-texttoshare="https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj&amp;userCustomMessage=KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa">
              <img alt="share bbm" src="https://womantalk.com/web/assets/img/helaw-mantan/share-bbm.svg" class="">
            </div><div id="video-share-email" class="video-share-item email" data-target="email" data-texttoshare="mailto:?subject=Womantalk - KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa&amp;body=KePOP #6: Menurut Pakar, Ini yang Sebenarnya Terjadi Pada Tubuh Saat Sedang Puasa https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj">
              <img alt="share email" src="https://womantalk.com/web/assets/img/helaw-mantan/share-email.svg" class="">
            </div></div>
              </div></div><div class="clearfix"></div></div><div style="font-family: 'Source Serif Pro', serif;"><p class="p-space">Menahan lapar dan haus selama 13 jam wajib dilakukan oleh Anda yang beragama Muslim setiap tahunnya, sebelum Lebaran. Kebiasaan makan dan aktivitas Anda akhirnya berubah demi mengikuti jadwal sahur dan berbuka. Banyak yang baik-baik saja, tetapi ada juga yang banyak mengalami masalah kesehatan. Mulai dari pusing, keliyengan, mulut bau, sampai kuku jari dingin. Memang, sebenarnya apa, sih, perubahan yang terjadi pada tubuh saat sedang puasa?</p>

<p class="p-space">Di KePOP episode baru ini saya mencoba untuk ikut puasa seperti teman dan rekan kerja saya yang beragama Muslim.&nbsp;</p>

{{-- <div class="youtube-embed-wrapper" style="position:relative;padding-bottom:56.25%;padding-top:30px;height:0;overflow:hidden;"><iframe allowfullscreen="" frameborder="0" height="360" src="//www.youtube.com/embed/SUCcnDd59ko?rel=0&amp;enablejsapi=1&amp;origin=https%3A%2F%2Fwomantalk.com" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;" width="640" data-gtm-yt-inspected-6246307_92="true" id="795716870"></iframe></div> --}}

<div rss="rssdel" style="border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;
                          width:auto;
                          max-width:640px;
                          margin-left:auto;
                          margin-right:auto;
                          margin-bottom:30px">
<div style="font-size: 10pt;margin:8px 0px;color: #ccc;line-height: 1.1em;">Baca juga</div>

<figure style="clear:left;margin:0px;padding:0px 8px 8px 0px; display:inline-block;">
<div style="background-image: url(https://dcywrb6nqrsdp.cloudfront.net/attachment/08602119094954305540.small);
                  float: left;
                  margin-right: 8px;
                  width:144px;
                  height: 85px;
                  background-size: cover;
                  background-position: center center;
                  background-repeat: no-repeat;">&nbsp;</div>

<figcaption style="
                  font-size:12pt;
                  text-align:left;
                  overflow: hidden;
                  text-overflow: ellipsis;
                  display: -webkit-box;
                  line-height: 16px;     /* fallback */
                  max-height: 64px;      /* fallback */
                  -webkit-line-clamp: 3; /* number of lines to show */
                  -webkit-box-orient: vertical;"><a class="ckeditor-text-url" href="https://womantalk.com/nutrition/articles/kepop-5-pentingnya-mengolah-sisa-bahan-makanan-untuk-lingkungan-AnGvb" style="line-height: 1.1em; text-align:left; color: #000; 
              text-decoration:underline solid red; font-weight:bold;">KePOP #5: Pentingnya Mengolah Sisa Bahan Makanan Untuk Lingkungan</a></figcaption>
</figure>

<div style="clear:left; height:8px">&nbsp;</div>
</div>

<p class="p-space">Penasaran dengan apa yang sebenarnya terjadi pada tubuh selama puasa, akhirnya saya mencoba berpuasa. Mulai dari dari bangun pagi untuk sahur, menahan lapar dan haus, sampai akhirnya berbuka. Sepanjang puasa, tentu saja banyak godaan untuk diam-diam makan dan minum. Untungnya, saya tidak tergoda.&nbsp;</p>

<p class="p-space">Namun, baru setengah hari puasa, ternyata apa yang dikatakan oleh teman-teman saya itu benar. Kepala saya mulai pusing, keliyengan, dan perut panas. Jam 12 biasanya saya makan, akhirnya saya tahan-tahan dengan terus bekerja sambil beberapa kali mata saya terpejam. Jelas sore, bibir saya mulai mengering.</p>

<p class="p-space">Akhirnya, saya punya kesempatan bertanya kepada dr. Abdul Haris&nbsp;Tri Prasetyo, Sp.PD mengenai apa yang sebenarnya terjadi pada tubuh ketika kita sedang berpuasa. Menurut dokter&nbsp;Haris saat menjalani puasa pasti tubuh akan menjalani adaptasi awal.</p>

<p class="p-space">"Adaptasi awal pasti ada. Untuk Anda yang belum pernah puasa sama sekali biasanya akan beradaprasi selama seminggu pertama. Anda akan merasa sedikit lapar karena dia pola makan agak bergeser," jelas dokter&nbsp;Haris.</p>

<p class="p-space">Sedangkan untuk perubahan fisik yang terjadi, seperti bau mulut, ujung kuku dingin, sampai keliyengan, semua itu pasti ada penyebabnya. "Kalau tanda-tanda ujung jari dingin, keliyengan, itu tanda hipoglikemi. Hipoglikemi itu artinya tubuh kekurangan gula. Mungkin itu artinya Anda ada penyakit diabetes," papar dokter&nbsp;Haris.&nbsp;</p>

<p class="p-space">Beliau mengatakan jika Anda tidak ada penyakit diabetes tetapi mengalami kuku jari dingin, itu artinya sahur Anda kurang atau Anda sama sekali tidak sahur. Dokter&nbsp;Haris pun menyarankan jika Anda memiliki penyakit diabetes, sebaiknya temui dokter untuk mengatur dosis obat Anda supaya kesehatan Anda tetap terjaga.</p>

<p class="p-space">Masih banyak fakta tentang perubahan tubuh yang terjadi selama puasa ini dan bisa Anda cek sendiri dalam video KePOP terbaru di atas! Selamat menonton!</p>
</div><div class="video-tag-wraper"><a class="video-tag" href="/article/tag/good-lifestyle">
                <span style="border-bottom: 2px solid #67c125">good lifestyle</span>
                  <span class="video-tag-separator">.</span>
              </a><a class="video-tag" href="/article/tag/nutrition">
                <span style="border-bottom: 2px solid #67c125">nutrition</span>
                  <span class="video-tag-separator">.</span>
              </a><a class="video-tag" href="/article/tag/kepop">
                <span style="border-bottom: 2px solid #67c125">kepop</span>
                  <span class="video-tag-separator">.</span>
              </a><a class="video-tag" href="/article/tag/puasa">
                <span style="border-bottom: 2px solid #67c125">puasa</span>
                  
              </a></div>
              <div class="video-comment-button-wrapper">
                  <div class="video-comment-button" data-id="27777" data-notif="false">KOMENTAR (<span id="comment-total-27777">0</span>)</div>
                </div>
              <hr class="m-bottom-30" style="border-color: rgba(0, 0, 0, 0.20)">
                <div style="width:100%">
                    <script id="ivsn" src="https://player.ivideosmart.com/ivideosense/player/js/ivsnload_v1.js?key=x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K&wid=31adb5e8-5010">
                    </script>
                </div>
            </div></div></div><div class="related-video-wrapper first-item" style=""><div class="related-video-caption">WATCH NEXT</div><div class=""><a href="https://womantalk.com/news-update/articles/ladies-roompi-4-netizen-vs-video-lucinta-luna-berhijab-yQXkE"><div class="related-video-item"><div class="related-video-image"><img src="https://dcywrb6nqrsdp.cloudfront.net/attachment/06901788577574850476.medium" style="height: 111.938px; width: 199px;"><div class="duration-label">05:00</div></div><div class="related-video-title" style="color: #4c4c4c">Ladies RoomPi #4: Netizen VS Video Lucinta Luna Berhijab</div><div class="clearfix"></div></div></a><a href="https://womantalk.com/parenting/articles/ladies-roompi-3-ini-persyaratan-jadi-babysitter-meghan-markle-dengan-gaji-hingga-1-5-m-AYaQ7"><div class="related-video-item"><div class="related-video-image"><img src="https://dcywrb6nqrsdp.cloudfront.net/attachment/55269950401539823566.medium" style="height: 111.938px; width: 199px;"><div class="duration-label">04:12</div></div><div class="related-video-title" style="color: #4c4c4c">Ladies Roompi #3 Ini Persyaratan Jadi Babysitter Meghan Markle Dengan Gaji Hingga 1,5 M!</div><div class="clearfix"></div></div></a><a href="https://womantalk.com/friendship/articles/ladies-roompi-2-5-foodcourt-bukan-mal-kekinian-jakarta-yang-cocok-untuk-bukber-reuni-yaQO7"><div class="related-video-item"><div class="related-video-image"><img src="https://dcywrb6nqrsdp.cloudfront.net/attachment/53895691501491012520.medium" style="height: 111.938px; width: 199px;"><div class="duration-label">06:02</div></div><div class="related-video-title" style="color: #4c4c4c">Ladies RoomPi #2: 5 Foodcourt (Bukan Mal) Kekinian Jakarta Yang Cocok Untuk Bukber Reuni</div><div class="clearfix"></div></div></a></div></div><div class="article-video-body"><hr class="no-desktop" style="border-color: rgba(0, 0, 0, 0.20); max-width: 900px; margin: 0 auto 30px"></div></div><div class="clearfix"></div></div></div>

      <div class="comment-overlay wrapper-article collapse" id="article-detail-comment">

</div>

      <div class="email-overlay collapse" id="overlay-delete-lounge">
<div class="new-modal-wt" id="delete-lounge">
  <div class="inner" id="delete-lounge-inner">
    <span class="close-modal new-close-modal" onclick="hideDeleteLounge()" style="top: 5px!important; float: right">
      <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
    </span>
    <div class="new-modal-content text-center m-bottom-10">
      <img alt="delete lounge" src="https://womantalk.com/web/assets/img/delete-lounge.svg" style="height: 100px; width: 100px">
    </div>
    <div class="bold text-center new-modal-caption m-bottom-20" style="padding: 0 20px">
      Apakah Anda yakin ingin menghapus konten ini?
    </div>
    <div class="text-center" style="margin: 0 10% 50px">
      <div class="pull-left color-white clickable" onclick="deleteLounge()" style="width: 70px; padding: 5px 10px; background-color: #55cfc4">
        Ya
      </div>
      <div class="pull-right color-white clickable" onclick="hideDeleteLounge()" style="width: 70px; padding: 5px 10px; background-color: #55cfc4">
        Tidak
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="modal-wt-error">
  <div class="inner">error message</div>
</div>
</div>
<script src="https://womantalk.com/web/js/popup/delete_lounge.js?v=1.1.3.07052018" type="text/javascript"></script>

      
<div class="email-overlay collapse" id="overlay-report-lounge">
<div class="new-modal-wt" id="report-lounge">
  <div class="inner" id="report-lounge-inner">
    <span class="close-modal new-close-modal" onclick="hideReportLounge()" style="top: 5px!important; float: right">
      <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
    </span>
    <div class="text-xs text-center new-modal-title m-bottom-20" style="line-height: 10px">REPORT</div>

    <div class="text-center new-modal-caption m-bottom-20" style="padding: 0 20px">
      Alasan Anda melapor?
    </div>
    <div class="new-modal-content text-center m-bottom-10" style="font-size: 0.85em;">
      <div class="report-option select full-width text-center clickable m-bottom-10 inactive" onclick="selectReportOption(this, 0)" style="">
        Mengandung SARA
      </div>
      <div class="report-option select full-width text-center clickable m-bottom-10 inactive" onclick="selectReportOption(this, 1)" style="">
        Mengandung Pornografi
      </div>
      <div class="report-option select full-width text-center clickable m-bottom-10 inactive" onclick="selectReportOption(this, 2)" style="">
        Mengandung Hal yang tidak pantas
      </div>
      <div class="full-width">
        <div class="text-center">
          Lainnya
        </div>
        <div>
          <textarea class="report-option text full-width inactive" name="" onclick="selectReportOption(this, 3)" placeholder="Tulis di sini..." rows="3" style="padding: 5px;"></textarea>
        </div>
      </div>
    </div>
    <div class="text-center m-bottom-10">
      <div class="color-white clickable" onclick="reportLounge()" style="padding: 5px 20px; width: fit-content; background-color: #55cfc4; margin: 0 auto;">
        REPORT
      </div>
    </div>
  </div>
</div>
<div class="modal-wt-error">
  <div class="inner">error message</div>
</div>
</div>
<script src="https://womantalk.com/web/js/popup/report_lounge.js?v=1.1.3.07052018" type="text/javascript"></script>

      <div id="Article_Baloon" style="height:1px; width:1px;"></div>
  </div>
      </div>
  </div>

<div></div>
<div class="overlay collapse" id="overlay-forgot-pass">
  <div class="new-modal-wt" id="forgot-pass">
      <div class="inner" id="forgot-pass-inner">

        <span class="close-modal new-close-modal">
          <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
        </span>
        <div class="text-xs text-center new-modal-title m-bottom-20">Forget password</div>

        <div class="text-sm text-center new-modal-caption m-bottom-20">
          Masukkan email yang Anda daftarkan di Womantalk
        </div>

        <div class="edit-profile new-modal-content">

          <div class="form-group full-width m-bottom-30">
            <span class="pull-left edit-profile-data-icon">
              <span aria-hidden="true" class="new-icon-email" data-fonticon="" style=""></span>
            </span>
            <input id="forget-pass-email" name="email" placeholder="E-mail" type="text">
            <div class="clearfix"></div>
          </div>
          <div class="modal-error-message" id="forget-pass-email-message" style=""></div>

          <div class="" style="height: 43px">
            <div class="m-bottom-30 text-center modal-message" id="forget-pass-message"></div>
          </div>

          <div class="new-modal-button inactive m-bottom-10" id="forget-pass-button">
            Send
          </div>

        </div>

      </div>
  </div>
  <div class="modal-wt-error">
    <div class="inner">error message</div>
  </div>
</div>

<script async="" defer="" src="https://womantalk.com/web/js/popup/forgot_pass.js?v=1.1.3.07052018" type="text/javascript"></script>

<div class="overlay collapse" id="overlay-verify-code">
  <div class="new-modal-wt" id="verify-code">
      <div class="inner">
          <span class="close-modal new-close-modal">
              <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
          </span>
          <div class="text-xs text-center new-modal-title m-bottom-20">Forget password</div>
          <div class="text-sm text-center new-modal-caption m-bottom-20">
            kode verifikasi telah dikirim ke email Anda
          </div>

          <div class="new-modal-content">
              <form id="verify-code-form">
                  <div class="field-area m-bottom-30">
                      <div class="input-group m-bottom-10">
                          <input maxlength="6" name="code" placeholder="Masukkan Kode" type="text" value="">
                          <div class="clearfix"></div>
                      </div>
                  </div>

                  <div class="" style="height: 43px">
                    <div class="m-bottom-30 text-center modal-message" id="verify-code-message"></div>
                  </div>

                  <div class="action">
                      <div class="act-btn">
                          <div class="new-modal-button active m-bottom-10" id="verify-code-button">
                              Kirim
                          </div>
                          <!-- <button type="button" class="button btn-primary color-white text-md">Kirim</button> -->
                      </div>
                      <div class="clearfix"></div>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <div class="modal-wt-error">
    <div class="inner">error message</div>
  </div>
</div>

<script async="" defer="" src="https://womantalk.com/web/js/popup/verify_code.js?v=1.1.3.07052018" type="text/javascript"></script>

<div class="overlay collapse" id="overlay-reset-pass">
  <div class="new-modal-wt" id="reset-pass">
      <div class="inner">
          <span class="close-modal new-close-modal">
              <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
          </span>
          <div class="text-xs text-center new-modal-title m-bottom-20">Ganti Kata Sandi</div>
          <div class="new-modal-content">
              <form id="reset-pass-form">
                  <div class="field-area m-bottom-30">
                      <div class="input-group">
                          <span class="pull-left">
                              <span aria-hidden="true" class="new-icon-re-password" data-fonticon="" style="font-size:20px;color:#55cfc4; position:relative;margin:2px 0; height: 24px"></span>
                          </span>
                          <input class="reset-pass-password" id="forgot-new-password" name="password" placeholder="New password" style="line-height: 24px; padding: 0px 8px" type="password" value="">
                          <i class="icon-wt-view pull-right" style="padding: 6px 0"></i>
                          <div class="clearfix"></div>
                      </div>
                      <div class="forgot-password-message m-bottom-10" id="forgot-new-password-message"></div>
                      <div class="input-group">
                          <span class="pull-left">
                              <img alt="confirm password" src="https://womantalk.com/web/assets/icon/profile/confirm-password.svg" style="height: 20px; width: 20px; margin: 2px 0">
                          </span>
                          <input class="reset-pass-password" id="forgot-confirmation-new-password" name="password" placeholder="Confirm new password" style="line-height: 24px; padding: 0px 8px" type="password" value="">
                          <i class="icon-wt-view pull-right" style="padding: 6px 0"></i>
                          <div class="clearfix"></div>
                      </div>
                      <div class="forgot-password-message m-bottom-10" id="forgot-confirmation-password-message"></div>
                  </div>

                  <div class="" style="height: 43px">
                    <div class="m-bottom-30 text-center modal-message" id="reset-pass-message"></div>
                  </div>

                  <div class="action">
                      <div class="new-modal-button inactive m-bottom-10" id="reset-pass-button">
                          Kirim
                      </div>
                      <!-- <div class="act-btn" style="text-align: center;">
                          <button type="button" class="button-forgot-password btn-primary color-white text-md button-inactive" id="button-forgot-password">Kirim</button>
                      </div> -->
                      <div class="clearfix"></div>
                  </div>
              </form>
          </div>
      </div>
  </div>
  <div class="modal-wt-error">
    <div class="inner">error message</div>
  </div>
</div>

<script async="" defer="" src="https://womantalk.com/web/js/popup/reset_password.js?v=1.1.3.07052018" type="text/javascript"></script>

<div class="collapse email-overlay" id="overlay-verify-email">
  <div class="new-modal-wt" id="verify-email">
      <div class="inner" id="verify-email-inner">
        <span class="close-modal new-close-modal">
          <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
        </span>
        <div class="text-xs text-center new-modal-title m-bottom-20">Verify your e-mail</div>

        <div class="text-sm text-center new-modal-caption m-bottom-20">
          Masukkan kode verifikasi yang telah dikirimkan ke email Anda
        </div>

        <div class="edit-profile new-modal-content">

          <div class="form-group full-width m-bottom-20">
            <span class="pull-left edit-profile-data-icon">
              <span aria-hidden="true" class="new-icon-re-password" data-fonticon="" style=""></span>
            </span>
            <input id="verification_code" name="verification_code" placeholder="Verification code" type="text">
            <div class="clearfix"></div>
          </div>

          <div class="full-width m-bottom-20">
            <div class="modal-menu" onclick="goChangeEmail();">
              <span><img alt="change email" src="https://womantalk.com/web/assets/icon/modal/change.svg"></span>
              Change e-mail
            </div>
            <div class="modal-menu text-right" onclick="resendEmail();">
              <span><img alt="resend email" src="https://womantalk.com/web/assets/icon/modal/resend.svg"></span>
              Resend code
            </div>
          </div>

          <div class="" style="height: 33px">
            <div class="m-bottom-20 text-center modal-message" id="verify-email-message"></div>
          </div>

          <div class="new-modal-button inactive m-bottom-10" onclick="verifyEmail()">
            Verify
          </div>

        </div>

      </div>
  </div>
  <div class="modal-wt-error">
    <div class="inner">error message</div>
  </div>
</div>

<script async="" defer`="" src="https://womantalk.com/web/js/popup/verify_email.js?v=1.1.3.07052018" type="text/javascript"></script>

<div class="email-overlay collapse" id="overlay-change-email">
  <div class="new-modal-wt" id="change-email">
      <div class="inner" id="change-email-inner">

        <span class="close-modal new-close-modal">
          <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
        </span>
        <div class="text-xs text-center new-modal-title m-bottom-20">Change e-mail</div>

        <div class="m-bottom-20 text-center modal-message" id="reset-email-message"></div>

        <div class="edit-profile new-modal-content">
          <div class="form-group full-width m-bottom-30">
            <span class="pull-left edit-profile-data-icon">
              <span aria-hidden="true" class="new-icon-password" data-fonticon="" style=""></span>
            </span>
            <div class="">
              <input id="reset-email-password" name="password" placeholder="Password" type="password">
              <i class="icon-wt-view pull-right" style="bottom: -7px; right: 1px"></i>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="modal-error-message" id="reset-email-password-message" style=""></div>

          <div class="form-group full-width m-bottom-30">
            <span class="pull-left edit-profile-data-icon">
              <span aria-hidden="true" class="new-icon-email" data-fonticon="" style=""></span>
            </span>
            <input id="reset-email-new-email" name="email" placeholder="E-mail" type="text">
            <div class="clearfix"></div>
          </div>
          <div class="modal-error-message" id="reset-email-new-email-message" style=""></div>

          <div class="form-group full-width m-bottom-30">
            <span class="pull-left edit-profile-data-icon">
              <span aria-hidden="true" class="new-icon-email" data-fonticon="" style=""></span>
            </span>
            <input id="reset-email-repeat-email" name="re-email" placeholder="Confirm e-mail" type="email">
            <div class="clearfix"></div>
          </div>
          <div class="modal-error-message" id="reset-email-repeat-email-message" style=""></div>

          <div class="new-modal-button inactive m-bottom-10" id="change-email-button" onclick="changeEmail()">
            Change E-mail
          </div>

        </div>

      </div>
  </div>
  <div class="modal-wt-error">
    <div class="inner">error message</div>
  </div>
</div>

<script async="" defer="" src="https://womantalk.com/web/js/popup/change_email.js?v=1.1.3.07052018" type="text/javascript"></script>

<div class="overlay collapse" id="overlay-login-redirect">
  <div class="new-modal-wt" id="login-redirect">
      <div class="inner login-landing-inner" id="login-redirect-inner">

        <span class="close-modal">
          <span aria-hidden="true" class="fonticon-close main-menu-icon" data-fonticon=""></span>
        </span>
        <div class="text-center new-modal-caption large m-bottom-10 m-top-20">
          You must login first
        </div>

        <div class="row row-no-margin login-redirect-buttons" id="socmed-login">
          <div class="col-xs-12 p-l-r-0 m-bottom-10">
            <div class="text-center register-socmed-btn fb">
              <span class="fa fa-facebook p-right-10"></span> Facebook
            </div>
          </div>
          <div class="col-xs-12 p-l-r-0 m-bottom-10">
            <div class="text-center register-socmed-btn tw">
              <span class="fa fa-twitter p-right-10"></span> Twitter
            </div>
          </div>
          <div class="col-xs-12 p-l-r-0 text-center">
            <a class="color-white" href="/login?page=/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj" id="overlay-login-button">
              <div class="full-width m-bottom-20 register-socmed-btn email-2">
                Log in
              </div>
            </a>
          </div>
          <div class="text-center text-md">
            <b>Are you new here?</b>
            <a class="color-primary" href="/register?page=/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj" id="overlay-register-button">Register now!</a>
          </div>

          <form action="/signin/twitter" id="login-form-tw" method="POST"></form>
        </div>

      </div>
  </div>
  <div class="modal-wt-error">
    <div class="inner">error message</div>
  </div>
</div>

<script async="" src="https://womantalk.com/web/js/popup/login_redirect.js?v=1.1.3.07052018" type="text/javascript"></script>


<div class="quiz-overlay collapse" id="overlay-get-lucky">
<div class="modal-luckiness modal-small text-center" id="get-lucky">
  <div class="init-luckiness congrats">
    SELAMAT!
  </div>
  <div class="init-luckiness">
    Anda mendapatkan <b>extra Kisses</b>
  </div>
  <div class="next-luckiness collapse congrats" id="congrats">
    CONGRATS!
  </div>
  <div class="lucky-box" onclick="getLucky()">

  </div>
  <div class="init-luckiness">
    Klik untuk membuka hadiah
  </div>
  <div class="next-luckiness collapse" id="thanks" onclick="closeLuckiness()">
    <div class="campaign-button-container redeem-leaderboard-button" style="bottom: initial">
      <div class="color-white campaign-button">
        THANKS!
        <span>
          <span aria-hidden="true" class="new-icon-next-circle-yellow campaign-button-next-icon" data-fonticon="" style="font-size:20px;position:relative; vertical-align:middle;"><span class="path1"></span><span class="path2"></span></span>
        </span>
      </div>
    </div>
  </div>
</div>
</div>

<script async="" defer="" src="https://womantalk.com/web/js/popup/luckiness.js?v=1.1.1.12202017" type="text/javascript"></script>

<div class="modal-zoom-image collapse" id="overlay-zoom-image" onclick="closeZoomImage()">
<div class="relative">
  <span aria-hidden="true" class="new-icon-close m-top-10"></span>
</div>
  <img alt="popup image" id="popup-image" src="" style="">
</div>

<script async="" defer="" src="https://womantalk.com/web/js/popup/zoom_in.js?v=1.1.3.07052018" type="text/javascript"></script>


<script type="text/javascript">
  $(document).ready(function () {

    // $('.carousel').carousel({
    //     interval: 3000
    // });

    // $('.carousel').carousel('cycle');

    $(document).click(function(event) {
      // if(!$(event.target).closest('.category-list-container').length && !$(event.target).closest('#icon-svg-cat').length) {
      //   if($('.category-list-container').hasClass('show')){
      //      $('.category-list-container').toggleClass('hide').toggleClass('show');
      //   }
      // }
      // if(!$(event.target).closest('.user-dd').length && !$(event.target).closest('#user-profil').length) {
      //   if($('.user-dd').hasClass('animated')){
      //     $('.user-dd').toggleClass('collapse').toggleClass('animated');
      //   }
      // }
      if(!$(event.target).closest('#menu-burger').length && !$(event.target).closest('.svg-burger').length) {
        if($('#menu-overlay-container').hasClass('show')){
          toggleMenuOverlay()
        }
      }
      if(!$(event.target).closest('#user-notif-list-content').length && !$(event.target).closest('#user-notif').length) {
        if($('#user-notif-list-content').hasClass('show')){
          $('#user-notif-list-content').toggleClass('hide').toggleClass('show');
        }
      }
      if(!$(event.target).closest('#search-all-container').length && !$(event.target).closest('#search-all').length) {
        if($('#search-all-container').hasClass('show')){
          $('#search-all-container').toggleClass('hide').toggleClass('show');
        }
      }
      if(!$(event.target).closest('#expert-dropdown').length && !$(event.target).closest('#expert-dropdown-init').length) {
        if(!$('#expert-dropdown').hasClass('hide')){
          $('#expert-dropdown').addClass('hide')
        }
      }
      if(!$(event.target).closest('.create-lounge-category-dropdown-toggler').length && !$(event.target).closest('.create-lounge-category').length) {
        if(!$('.create-lounge-category').hasClass('hide')){
          $('.create-lounge-category').addClass('hide')
        }
      }
      if(!$(event.target).closest('.create-lounge-expert-dropdown-toggler').length && !$(event.target).closest('.create-lounge-expert').length) {
        if(!$('.create-lounge-expert').hasClass('hide')){
          $('.create-lounge-expert').addClass('hide')
        }
      }
    })
  });
  // function goArticle(id) {
  //   window.location = '/article/'+id
  // }
  function goTopic(name) {
    ga('send', 'event', 'Category Label', 'Click', 'Topic Stream')
    window.event.cancelBubble = true
    window.event.stopPropagation()
    window.location = '/'+name+'/articles'
    return false;
  }
  function goLounge(name) {
    window.event.cancelBubble = true
    window.event.stopPropagation()
    window.location = '/'+name+'/lounges'
    return false;
  }
  // function goUser(name) {
  //   window.event.cancelBubble = true
  //   window.event.stopPropagation()
  //   window.location = '/@'+name
  //   return false;
  // }
</script>

<link as="font" href="https://fonts.googleapis.com/css?family=Montserrat" rel="preload">
<link as="font" href="https://fonts.googleapis.com/css?family=Lora:400,700" rel="preload">
<link as="font" href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,700" rel="preload">

<link href="/web/assets/font-ico/wtico.css" rel="stylesheet" type="text/css">
<link href="/web/assets/font/wtfont.css" rel="stylesheet" type="text/css">
<link href="/web/assets/font-icon/font-icon.css" rel="stylesheet" type="text/css">
<!-- Font Awesome -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<script src="https://womantalk.com/web/vendor/fingerprint2.min.js"></script>
<script type="text/javascript" id="" src="https://www.googletagmanager.com/gtag/js?id=UA-74928173-1"></script><script type="text/javascript" id="" src="https://ad.crwdcntrl.net/5/c=10877/pe=y/var=ccauds"></script>
<script type="text/javascript" id="">!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version="2.0",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,"script","https://connect.facebook.net/en_US/fbevents.js");fbq("init","1506122303025458");fbq("set","agent","tmgoogletagmanager","1506122303025458");fbq("track","PageView");</script>
<script type="text/javascript" id="">var _comscore=_comscore||[];_comscore.push({c1:"2",c2:"20767306"});(function(){var a=document.createElement("script"),b=document.getElementsByTagName("script")[0];a.async=!0;a.src=("https:"==document.location.protocol?"https://sb":"http://b")+".scorecardresearch.com/beacon.js";b.parentNode.insertBefore(a,b)})();</script>
<noscript>
<img src="https://sb.scorecardresearch.com/p?c1=2&amp;c2=20767306&amp;cv=2.0&amp;cj=1">
</noscript><script type="text/javascript" id="gtm-youtube-tracking">(function(h,f,k){function m(){var b=[].slice.call(h.getElementsByTagName("iframe")).concat([].slice.call(h.getElementsByTagName("embed"))),a;for(a=0;a<b.length;a++){var d=n(b[a]);if(d){d=b[a];var e=f.location,c=h.createElement("a");c.href=d.src;c.hostname="www.youtube.com";c.protocol=e.protocol;var g="/"===c.pathname.charAt(0)?c.pathname:"/"+c.pathname;-1<c.search.indexOf("enablejsapi")||(c.search=(0<c.search.length?c.search+"\x26":"")+"enablejsapi\x3d1");if(!(-1<c.search.indexOf("origin"))&&-1===
e.hostname.indexOf("localhost")){var w=e.port?":"+e.port:"";e=e.protocol+"%2F%2F"+e.hostname+w;c.search=c.search+"\x26origin\x3d"+e}"application/x-shockwave-flash"===d.type&&(e=h.createElement("iframe"),e.height=d.height,e.width=d.width,g=g.replace("/v/","/embed/"),d.parentNode.parentNode.replaceChild(e,d.parentNode),d=e);c.pathname=g;d.src!==c.href+c.hash&&(d.src=c.href+c.hash);p(d)}}"addEventListener"in h&&h.addEventListener("load",x,!0)}function n(b){b=b.src||"";return-1<b.indexOf("youtube.com/embed/")||
-1<b.indexOf("youtube.com/v/")?!0:!1}function p(b){var a=YT.get(b.id);a||(a=new YT.Player(b,{}));"undefined"===typeof b.pauseFlag&&(b.pauseFlag=!1,a.addEventListener("onStateChange",function(a){y(a,b)}))}function z(b){var a={};g.events["Watch to End"]&&(a["100%"]=Math.min(b-3,Math.floor(.99*b)));if(g.percentageTracking){var d=[],e;g.percentageTracking.each&&(d=d.concat(g.percentageTracking.each));if(g.percentageTracking.every){var c=parseInt(g.percentageTracking.every,10),f=100/c;for(e=1;e<f;e++)d.push(e*
c)}for(e=0;e<d.length;e++)f=d[e],c=f+"%",f=b*f/100,a[c]=Math.floor(f)}return a}function y(b,a){var d=b.data,e=b.target,c=e.getVideoUrl();c=c.match(/[?&]v=([^&#]*)/)[1];var f=e.getPlayerState(),g=Math.floor(e.getDuration()),h=z(g);g={1:"Play",2:"Pause"};g=g[d];a.playTracker=a.playTracker||{};1!==f||a.timer?(clearInterval(a.timer),a.timer=!1):(clearInterval(a.timer),a.timer=setInterval(function(){var b=e,d=h,c=a.videoId,g=b.getCurrentTime(),f;b[c]=b[c]||{};for(f in d)d[f]<=g&&!b[c][f]&&(b[c][f]=!0,
q(c,f))},1E3));1===d&&(a.playTracker[c]=!0,a.videoId=c,a.pauseFlag=!1);if(!a.playTracker[a.videoId])return!1;if(2===d){if(a.pauseFlag)return!1;a.pauseFlag=!0}r[g]&&q(a.videoId,g)}function q(b,a){var d="https://www.youtube.com/watch?v\x3d"+b,e=f.GoogleAnalyticsObject;if("undefined"===typeof f[t]||g.forceSyntax)if("function"===typeof f[e]&&"function"===typeof f[e].getAll&&2!==g.forceSyntax)f[e]("send","event","Videos",a,d);else"undefined"!==typeof f._gaq&&1!==A&&f._gaq.push(["_trackEvent","Videos",
a,d]);else f[t].push({event:"youTubeTrack",attributes:{videoUrl:d,videoAction:a}})}function u(b,a,d){if(b.addEventListener)b.addEventListener(a,d);else if(b.attachEvent)b.attachEvent("on"+a,function(a){a.target=a.target||a.srcElement;d.call(b,a)});else if("undefined"===typeof b["on"+a]||null===b["on"+a])b["on"+a]=function(a){a.target=a.target||a.srcElement;d.call(b,a)}}function x(b){b=b.target||b.srcElement;var a=n(b);"IFRAME"===b.tagName&&a&&-1<b.src.indexOf("enablejsapi")&&-1<b.src.indexOf("origin")&&
p(b)}var g=k||{},A=g.forceSyntax||0,t=g.dataLayerName||"dataLayer",r={Play:!0,Pause:!0,"100%":!0},l;k=h.createElement("script");k.src="//www.youtube.com/iframe_api";var v=h.getElementsByTagName("script")[0];v.parentNode.insertBefore(k,v);for(l in g.events)g.events.hasOwnProperty(l)&&(r[l]=g.events[l]);f.onYouTubeIframeAPIReady=function(){var b=f.onYouTubeIframeAPIReady;return function(){b&&b.apply(this,arguments);navigator.userAgent.match(/MSIE [67]\./gi)||("loading"!==h.readyState?m():h.addEventListener?
u(h,"DOMContentLoaded",m):u(f,"load",m))}}()})(document,window,{events:{Play:!0,Pause:!0,"100%":!0},percentageTracking:{every:25,each:[10,90]}});</script><script type="text/javascript" id="">_atrk_opts={atrk_acct:"gXDCp1IWh910fn",domain:"womantalk.com",dynamic:!0};(function(){var a=document.createElement("script");a.type="text/javascript";a.async=!0;a.src="https://d31qbv1cthcecs.cloudfront.net/atrk.js";var b=document.getElementsByTagName("script")[0];b.parentNode.insertBefore(a,b)})();</script><script type="text/javascript" id="LOTCC_10871" src="https://tags.crwdcntrl.net/c/10871/cc_ajax.js?ns=_cc10871"></script>
<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.13.0/firebase-messaging.js"></script>
<script type="text/javascript" id="">window.dataLayer=window.dataLayer||[];function gtag(){dataLayer.push(arguments)}gtag("js",new Date);gtag("config","UA-74928173-1",{send_page_view:!1});</script>

<script async="" defer="" src="https://womantalk.com/web/js/main/browser_notif.js"></script>

<noscript>
  <link href="https://womantalk.com//web/vendor/carousel.css" rel="stylesheet" type="text/css" />
  <link href="https://womantalk.com//web/vendor/grid.css" rel="stylesheet" type="text/css" />
  <link href="https://womantalk.com/web/vendor/grid-horizon.css" rel="stylesheet" type="text/css" />
</noscript>

<script type="text/javascript">
  var giftofspeed = document.createElement('link');
  giftofspeed.rel = 'stylesheet';
  giftofspeed.href = 'https://womantalk.com//web/vendor/carousel.css';
  giftofspeed.type = 'text/css';
  var godefer = document.getElementsByTagName('link')[0];
  godefer.parentNode.insertBefore(giftofspeed, godefer);
  
  var giftofspeed2 = document.createElement('link');
  giftofspeed2.rel = 'stylesheet';
  giftofspeed2.href = 'https://womantalk.com/web/vendor/grid.css';
  giftofspeed2.type = 'text/css';
  var godefer2 = document.getElementsByTagName('link')[0];
  godefer2.parentNode.insertBefore(giftofspeed2, godefer2);

  var giftofspeed3 = document.createElement('link');
  giftofspeed3.rel = 'stylesheet';
  giftofspeed3.href = 'https://womantalk.com/web/vendor/grid-horizon.css';
  giftofspeed3.type = 'text/css';
  var godefer3 = document.getElementsByTagName('link')[0];
  godefer3.parentNode.insertBefore(giftofspeed3, godefer3);
</script>


  <script src="https://womantalk.com/web/js/video/video_detail_page.js?v=2.0.0.20190118" type="text/javascript"></script><div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://staticxx.facebook.com/connect/xd_arbiter.php?version=44#channel=f70911def30548&amp;origin=https%3A%2F%2Fwomantalk.com" style="border: none;" data-gtm-yt-inspected-6246307_92="true"></iframe></div><div></div></div></div>
  <script src="https://womantalk.com/web/js/article/article_detail_comment.js?v=2.0.0.20190701" type="text/javascript"></script>
<script type="text/javascript" id="">_cc10871.bcp();</script>



<script type="text/javascript" id="">var isAnyPopupShowing=!1,seenArticleIds="";if("undefined"!==typeof Storage){var seenArticle=localStorage.getItem("seenArticle");seenArticle?seenArticle=removeOldSeenArticle(seenArticle,$("#article-title").attr("data-id")):(localStorage.setItem("seenArticle",[]),seenArticle=[]);addArticleToSeenList(seenArticle,$("#article-title").attr("data-id"))}else isAnyPopupShowing=!0;
function removeOldSeenArticle(a,c){var e=(new Date).getTime();a=JSON.parse(a);return a=jQuery.grep(a,function(a,b){return 6048E5>e-a.timeStamp&&a.id!=c})}function addArticleToSeenList(a,c){var e={id:c,timeStamp:(new Date).getTime()};a.push(e);localStorage.setItem("seenArticle",JSON.stringify(a))}function getSeenArticleListString(a){var c=[];$.each(JSON.parse(a),function(a,d){c.push(d.id)});return c.join()}
function generateArticleRecommendation(){seenArticleIds=getSeenArticleListString(localStorage.getItem("seenArticle"));var a={except_ids:seenArticleIds};actionWebApiRestrict("/v1/article/trending",a,"POST").done(function(a){if("600"==a.status&&a.data[0]&&!isAnyPopupShowing){isAnyPopupShowing=!0;a=a.data[0];var c=1200<$(window).width()?"10px":"0px",d=767<$(window).width()?"180px":"140px",b='\x3cdiv id\x3d"xAtcRcmd" data-id\x3d"'+a.id+'" style\x3d"position: fixed; opacity: 0; max-width: 450px; width: 100%; z-index: 100000; background: white; border: 1px solid #f2f2f2; bottom: '+
c+"; right: "+c+'"\x3e';b+='\x3cdiv style\x3d"padding: 10px"\x3e';b+='\x3cdiv class\x3d"full-width"\x3e';b+='\x3cdiv class\x3d"m-bottom-10" style\x3d""\x3e';b+="Anda mungkin juga suka artikel ini";b+='\x3cspan aria-hidden\x3d"true" data-fonticon\x3d"\x26#xe001;" id\x3d"xAtcRcmdClose" class\x3d"fonticon-close main-menu-icon" style\x3d"float: right; top: -5px; left: 5px; position: relative; font-size: 20px;"\x3e\x3c/span\x3e';b+="\x3c/div\x3e";b+="\x3c/div\x3e";b+='\x3cdiv class\x3d"row row-no-margin clickable" id\x3d"xAtcRcmdOpen" onclick\x3d"rcmdTrckRdrct(\''+
a.url+"')\"\x3e";b+='\x3cdiv class\x3d"col-xs-5 p-l-r-0 center-crop" style\x3d"background-image:url('+a.cover.medium+"); height: "+d+'"\x3e';b+="\x3c/div\x3e";b+='\x3cdiv class\x3d"col-xs-7 p-l-r-0" style\x3d"height: '+d+'"\x3e';b+='\x3cdiv style\x3d"background-color: '+a.category.color+'; display: inline-block; padding: 3px 10px; font-size: 0.8em; color: #ffffff; font-weight: bold;"\x3e';b+=a.category.name;b+="\x3c/div\x3e";b+="\x3cdiv\x3e";b+='\x3cdiv class\x3d"bold" style\x3d"height: 5em; font-family: \'Lora\',serif; font-size: 1.2em; line-height: 1.4em; padding-left: 20px; padding-top: 15px; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden;"\x3e';
b+=a.title;b+="\x3c/div\x3e";b+='\x3cdiv class\x3d""\x3e';b+='\x3cdiv style\x3d"padding-left: 20px; font-size: 1em; font-weight: bold; color: #55cfc4; position: absolute; bottom: 10px;"\x3e';b+="Baca Disini \x3e";b+="\x3c/div\x3e";b+="\x3c/div\x3e";b+="\x3c/div\x3e";b+="\x3c/div\x3e";b+="\x3c/div\x3e";b+="\x3c/div\x3e";b+="\x3cdiv\x3e";$("body").append(b);$("#xAtcRcmd").animate({bottom:"-400px",opacity:"0"},100).animate({bottom:c,opacity:"1"},1E3)}})}
var isRecommendationShown=!1,recommendationShowingThreshold=(isMobile?.8:.5)*$(document).height();$(window).scroll(function(){$(window).scrollTop()+$(window).height()/2>recommendationShowingThreshold&&!isRecommendationShown&&!isAnyPopupShowing&&(generateArticleRecommendation(),isRecommendationShown=!0)});$("body").on("click","#xAtcRcmd .fonticon-close",function(a){a=JSON.parse(localStorage.getItem("seenArticle"));addArticleToSeenList(a,$("#xAtcRcmd").attr("data-id"));isAnyPopupShowing=!1;$("#xAtcRcmd").remove()});
function rcmdTrckRdrct(a){var c=JSON.parse(localStorage.getItem("seenArticle"));addArticleToSeenList(c,$("#xAtcRcmd").attr("data-id"));location.href=a};</script><script type="text/javascript" id="">var winnerBanner='\x3cdiv style\x3d"margin-top: 10px;"\x3e\x3cimg class\x3d"no-web-v2 full-width" src\x3d"https://dcywrb6nqrsdp.cloudfront.net/image/winner-mweb.gif"\x3e\x3cimg class\x3d"no-mobile-v2 full-width" src\x3d"https://dcywrb6nqrsdp.cloudfront.net/image/winner-web.gif"\x3e\x3c/div\x3e';
$("#article-title").parent().prepend('\x3ca href\x3d"https://womantalk.com/world/articles/kampanye-perempuan-bisa-kembali-raih-penghargaan-tingkat-asia-DKlJO?utm_campaign\x3dwinner-perempuan-bisa\x26utm_medium\x3dbanner\x26utm_source\x3dwebsite\x26utm_content\x3dbanner-top\x26utm_term\x3dwinner-perempuan-bisa"\x3e'+winnerBanner+"\x3c/a\x3e");</script><script type="text/javascript" id="">var lotameArticleCategory=articleData[userCurrentLocation-1].category.name,lotameArticleTags=articleData[userCurrentLocation-1].tags_name.split(", "),_cc10871_ajax1=new _cc10871.ajax;_cc10871_ajax1.add("int","womantalk.com : Category : "+lotameArticleCategory);for(var i=0;i<lotameArticleTags.length;i++)_cc10871_ajax1.add("int","womantalk.com : Article Tag : "+lotameArticleTags[i]);_cc10871_ajax1.bcpa(!0,!0,!0);</script><iframe id="LOTCCFrame_1560837206373" src="https://bcp.crwdcntrl.net/5/c=10871/rand=870913468/pv=y/int=womantalk.com%20%3A%20Category%20%3A%20Nutrition/int=womantalk.com%20%3A%20Article%20Tag%20%3A%20good%20lifestyle/int=womantalk.com%20%3A%20Article%20Tag%20%3A%20nutrition/int=womantalk.com%20%3A%20Article%20Tag%20%3A%20kepop/int=womantalk.com%20%3A%20Article%20Tag%20%3A%20puasa/int=%23OpR%2377819%23womantalk.com%20%3A%20Total%20Site%20Traffic/rt=ifr" title="empty" tabindex="-1" role="presentation" aria-hidden="true" data-gtm-yt-inspected-6246307_92="true" style="border: 0px; width: 0px; height: 0px; display: block;"></iframe><div style="display: none; visibility: hidden;"><script type="text/javascript">(function(){var a="https:"==document.location.protocol;a=(a?"https:":"http:")+"//www.googletagservices.com/tag/js/gpt.js";document.write('\x3cscript src\x3d"'+a+'"\x3e\x3c/script\x3e')})();</script><script src="https://www.googletagservices.com/tag/js/gpt.js"></script>

<script type="text/javascript">var dartCCKey="aud_wt_test",dartCC="";if("undefined"!=typeof ccauds)for(var cci=0;cci<ccauds.Profile.Audiences.Audience.length;cci++)0<cci&&(dartCC+=","),dartCC+=ccauds.Profile.Audiences.Audience[cci].abbr;googletag.cmd.push(function(){googletag.pubads().setTargeting(dartCCKey,[dartCC])});</script>
<script type="text/javascript">window.googletag=window.googletag||{};googletag.cmd=googletag.cmd||[];googletag.cmd.push(function(){googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs(!0);googletag.pubads().disableInitialLoad();googletag.enableServices()});var numSlotId=1;function generateIdSlot(){var a=numSlotId++;return a}
function infinitesElementAds(){$(".dfp_leaderboard1").each(function(){if(1>=$(this).html()){var a=googletag.sizeMapping().addSize([800,600],[[728,90],[468,60],[1,1]]).addSize([0,0],[[320,50],[300,100],[234,60],[1,1]]).build(),b=generateIdSlot(),c="\x3cdiv id\x3d'WT_Homepage_Leaderboard1_"+b+"' style\x3d'text-align:center;'\x3e\x3c/div\x3e";$(this).append(c);a=googletag.defineSlot("/118200697/Womantalk/WT_Homepage_Leaderboard",[[728,90],[468,60],[1,1],[320,50],[300,100],[234,60]],"WT_Homepage_Leaderboard1_"+
b).setTargeting("AudienceWomantalk",["GoodLifestyle"]).defineSizeMapping(a).addService(googletag.pubads());googletag.display("WT_Homepage_Leaderboard1_"+b);googletag.pubads().refresh([a])}});$(".dfp_leaderboard2").each(function(){if(1>=$(this).html()){var a=googletag.sizeMapping().addSize([800,600],[[728,90],[468,60],[1,1]]).addSize([0,0],[[250,250],[300,250],[320,50],[300,100],[234,60],[1,1]]).build(),b=generateIdSlot(),c="\x3cdiv id\x3d'WT_Homepage_Leaderboard2_"+b+"' style\x3d'text-align:center;'\x3e\x3c/div\x3e";
$(this).append(c);a=googletag.defineSlot("/118200697/Womantalk/WT_Homepage_Middleboard",[[250,250],[300,250],[320,50],[300,100],[234,60],[1,1],[728,90],[468,60]],"WT_Homepage_Leaderboard2_"+b).setTargeting("AudienceWomantalk",["GoodLifestyle"]).defineSizeMapping(a).addService(googletag.pubads());googletag.display("WT_Homepage_Leaderboard2_"+b);googletag.pubads().refresh([a])}});$(".dfp_retangle").each(function(){if(1>=$(this).html()){var a=googletag.sizeMapping().addSize([800,600],[[300,250],[336,
280],[250,250],[320,50],[300,100],[234,60],[200,200],[120,240],[1,1]]).addSize([0,0],[[320,50],[300,100],[300,250],[300,600],[240,400],[250,250],[234,60],[200,200],[120,600],[160,600],[120,240],[180,150],[1,1]]).build(),b=generateIdSlot(),c="\x3cdiv id\x3d'WT_Homepage_Rectangle1_"+b+"' style\x3d'text-align:center;'\x3e\x3c/div\x3e";$(this).append(c);a=googletag.defineSlot("/118200697/Womantalk/WT_Homepage_Rectangle1",[[320,50],[300,100],[300,250],[300,600],[240,400],[250,250],[234,60],[200,200],[120,
600],[160,600],[120,240],[180,150],[300,250],[336,280],[250,250],[320,50],[300,100],[234,60],[200,200],[120,240]],"WT_Homepage_Rectangle1_"+b).defineSizeMapping(a).addService(googletag.pubads());googletag.display("WT_Homepage_Rectangle1_"+b);googletag.pubads().refresh([a])}})}
function firstElementAds(){var a=googletag.sizeMapping().addSize([800,600],[1,1]).addSize([600,800],[1,1]).addSize([0,0],[1,1]).build(),b=googletag.sizeMapping().addSize([800,600],[1,1]).addSize([0,0],[]).build(),c=googletag.sizeMapping().addSize([800,600],[]).addSize([0,0],[1,1]).build(),d="\x3cdiv class\x3d'overlay-desktop' id\x3d'WT_Popup_Desktop' style\x3d'width: 0px;height: 0px;display: none;'\x3e\x3c/div\x3e",h="\x3cdiv class\x3d'overlay-mobile' id\x3d'WT_Popup_Mobile' style\x3d'width: 0px;height: 0px;display: none;'\x3e\x3c/div\x3e",
k="\x3cdiv id\x3d'WT_Article_Baloon' style\x3d'height: 1px; width: 1px;'\x3e\x3c/div\x3e",e="\x3cdiv id\x3d'WT_Parallax_Mobile' class\x3d'parallax-mobile' style\x3d'height: 1px; width: 1px;'\x3e\x3c/div\x3e",f="\x3cdiv id\x3d'WT_Parallax_Desktop' class\x3d'parallax-desktop' style\x3d'height: 1px; width: 1px;'\x3e\x3c/div\x3e",g="\x3cdiv id\x3d'WT_Inread' style\x3d'height: 1px; width: 1px;'\x3e\x3c/div\x3e";$(d).insertAfter(".dfp_leaderboard2:first");$(h).insertAfter(".dfp_leaderboard2:first");$(k).insertAfter(".dfp_retangle:first");
d=Math.ceil($(".article-page-container:first p").length/2);$(".article-page-container:first p:nth-child("+d+")").append(e);$(".article-page-container:first p:nth-child("+d+")").append(f);$(".article-page-container:first p:nth-child(2)").append(g);e=googletag.defineSlot("/118200697/Womantalk/WT_InRead",[1,1],"WT_Inread").defineSizeMapping(a).addService(googletag.pubads());a=googletag.defineSlot("/118200697/Womantalk/WT_Article_Baloon",[1,1],"WT_Article_Baloon").defineSizeMapping(a).addService(googletag.pubads());
f=googletag.defineOutOfPageSlot("/118200697/Womantalk/WT_Article_Popup_Desktop","WT_Popup_Desktop").defineSizeMapping(b).addService(googletag.pubads());b=googletag.defineOutOfPageSlot("/118200697/Womantalk/WT_Article_Parallax_Desktop","WT_Parallax_Desktop").defineSizeMapping(b).addService(googletag.pubads());g=googletag.defineOutOfPageSlot("/118200697/Womantalk/WT_Article_Popup_Mobile","WT_Popup_Mobile").defineSizeMapping(c).addService(googletag.pubads());c=googletag.defineOutOfPageSlot("/118200697/Womantalk/WT_Article_Parallax_Mobile",
"WT_Parallax_Mobile").defineSizeMapping(c).addService(googletag.pubads());googletag.display("WT_Parallax_Mobile");googletag.display("WT_Parallax_Desktop");googletag.display("WT_Popup_Desktop");googletag.display("WT_Popup_Mobile");googletag.display("WT_Article_Baloon");googletag.display("WT_Inread");googletag.pubads().refresh([a,f,g,b,c,e])}googletag.cmd.push(function(){infinitesElementAds();firstElementAds()});</script></div></body></html>