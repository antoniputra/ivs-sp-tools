
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible">
	<meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1, user-scalable=yes"/>
	  
	 
	<meta property="fb:app_id" content="469667836553383" />
	<meta property="fb:pages" content="255057814552667" />
	<meta name="robots" content="follow, index"/>
	<meta name="title" content="士乃中总及村民协会．6月23联办周年联欢宴" />
	<meta name="description" content="士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。" />
	<meta name="twitter:site" content="https://www.sinchew.com.my/content/content_2044619.html" />
	<meta name="twitter:title" content="士乃中总及村民协会．6月23联办周年联欢宴" />
	<meta name="twitter:description" content="士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。" />
		
	<meta property="og:image" content="https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" />
	<meta name="twitter:image" content="https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg" />
		
	<meta property="og:image:width" content="1200"/>	
	<meta property="og:image:height" content="630"/>
	<meta property="og:url" content="https://www.sinchew.com.my/content/content_2044619.html" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="士乃中总及村民协会．6月23联办周年联欢宴" />
	<meta property="og:description" content="士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。" />	
	
		 
	<title>
	士乃中总及村民协会．6月23联办周年联欢宴 - 地方 - 大柔佛 | 
	大柔佛 | 
	星洲网 Sin Chew Daily
	</title>
	<script type="text/javascript">
if (location.protocol != 'https:')
{
 location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}	
</script>

<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Cache" content="no-cache">

<!-- jquery library -->
<script src="https://cdnpuc.sinchew.com.my/resource/jquery.min.js"></script>

<!-- detect user device -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.4.3/mobile-detect.min.js"></script>

<!-- redirect to correct url -->
<script src="https://www.sinchew.com.my/resource/detect.js?a3"></script>

<!-- bootstrap css and js -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- sinchew logo -->
<link rel="shortcut icon" type="image/png" href="https://www.sinchew.com.my/resource/sinchewlogo.png"/>

<!-- mainpage css -->
<link rel="stylesheet" type="text/css" href="https://cdnpuc.sinchew.com.my/resource/mobile-css.css">

<!-- mainpage slider css -->
<link rel="stylesheet" type="text/css" href="https://cdnpuc.sinchew.com.my/resource/lightslider.css">

<!-- menubar css -->
<link href="https://cdnpuc.sinchew.com.my/resource/perfect-scrollbar.css" rel="stylesheet">

<!-- comment css -->
<link href="https://cdnpuc.sinchew.com.my/resource/comment.css" rel="stylesheet">

<!-- footer css -->
<link href="https://cdnpuc.sinchew.com.my/resource/footer.css" rel="stylesheet">

<!-- swiper css -->
<link rel="stylesheet" href="https://cdnpuc.sinchew.com.my/resource/swiper.css">

<!-- swiper css -->
<link rel="stylesheet" href="https://cdnpuc.sinchew.com.my/resource/fav.css">
<script src="https://cdnpuc.sinchew.com.my/resource/fav.js"></script>

<!-- display time for IE and Safari -->
<script src="https://cdnpuc.sinchew.com.my/resource/moment.js"></script>

<!-- change display time to more neatly -->
<script src="https://cdnpuc.sinchew.com.my/resource/md5.js"></script>
<script src="https://cdnpuc.sinchew.com.my/resource/mypersonalcommon.js"></script>
<script src="https://cdnpuc.sinchew.com.my/resource/personalpadmessage.js"></script>

<!-- menubar js -->
<script src="https://cdnpuc.sinchew.com.my/resource/perfect-scrollbar.min.js"></script>

<!-- mainpage slider js -->
<script src="https://cdnpuc.sinchew.com.my/resource/lightslider.js"></script>

<!-- share button js -->
<script async src="https://static.addtoany.com/menu/page.js"></script>

<!-- sharecount js -->
<script src="https://cdnpuc.sinchew.com.my/amucsite/stat/PadShare.js"></script>

<!-- swiper js -->
<script src="https://cdnpuc.sinchew.com.my/resource/swiper.js"></script>

<!-- js for clickcount time and etc -->
<!-- 所有window onload 请放这 -->
<script src="https://cdnpuc.sinchew.com.my/resource/m-specialtime.js"></script>


		 
	<!-- admanager code -->
	<div><!-- 大柔佛详情页  -->
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_1x1', [1, 1], 'div-gpt-ad-1543077774044-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_Leaderboard', [320, 50], 'div-gpt-ad-1543077840668-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_RectangleI', [300, 250], 'div-gpt-ad-1543077896195-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_RectangleII(Inner)', [300, 250], 'div-gpt-ad-1543078174365-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2(Mykampung_Daroufo)_RectangleIII(Inner)', [300, 250], 'div-gpt-ad-1543078238595-0').addService(googletag.pubads());
	googletag.defineSlot('/2365395/SC2_BottomBanner_ROS', [[320, 100], [320, 50]], 'div-gpt-ad-1542969361433-0').addService(googletag.pubads());
	googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script></div>  		 
		
	<script>
		$(document).ready(function() {
			$('#show_news_click').click(function(){
				$('#othernew_dropdown').toggle();
				$("#show_news_click").toggleClass("icon-circle-arrow-up icon-circle-arrow-down");
				if ($("#show_news_click").hasClass("icon-circle-arrow-up")) 
				{
					$("#show_news_click").html("&#9660;");
				}
				else 
				{
					$("#show_news_click").html("&#9650;");
				}
			});
			
			$('#show_comment_click').click(function(){
				$('#comment_dropdown').toggle();
				$("#show_comment_click").toggleClass("icon-circle-arrow-up icon-circle-arrow-down");
				if ($("#show_comment_click").hasClass("icon-circle-arrow-up")) 
				{
					$("#show_comment_click").html("&#9660;");
				}
				else 
				{
					$("#show_comment_click").html("&#9650;");
				}
			});
		});
	
		var _pvmax = {"region": "my", "siteId": "fb6c2f5d-ef4e-40f8-a264-88f50da0f92c"};
		(function(d, s, id) {
  		var js, pjs = d.getElementsByTagName(s)[0];
  		if (d.getElementById(id)) return;
  		js = d.createElement(s); js.id = id; js.async = true;
  		js.src = "//api.pvmax.net/v1.0/pvmax.js";
  		pjs.parentNode.insertBefore(js, pjs);
		}(document, 'script', 'pvmax-jssdk'));

		!function(e,t,n,a,o,s,c){e.tpq||(o=e.tpq=function(){o.callMethod?o.callMethod.apply(o,arguments):o.queue.push(arguments)},o.queue=[],o.callMethods=[],s=t.createElement(n),c=t.getElementsByTagName(n)[0],s.async=!0,s.src=a,c.parentNode.insertBefore(s,c))}(window,document,"script","//t.funp.com/js/loader.js","tpq");
		tpq('init', '134018364875');
		tpq('track', 'PageView');
		tpq('trackCustom', 'retargeting', {"tag_id":"403229476961"});
</script>

<style>
	.ad_words
	{
		font-size:13px;
		color:#808080;
		text-align:left;
	}	
	.nw-easy-guanggao
	{
		width:300px;
		margin:0 auto;
		text-align:left;
		padding-bottom: 8px;
	}
	
	
	.hidelasthr:last-of-type
	{
		display:none;
	}
</style>

<!-- 文中插入广告  -->
 <script type="text/javascript"> 
 
	//var ads1 = ('<div class="nw-easy-guanggao"><div class="guanggaoword">广告</div><img src="https://www.sinchew.com.my/founder_rec.jpg"></div>');
	 
	//var ads2 = ('<div class="nw-easy-guanggao"><div class="guanggaoword">广告</div><img src="https://www.sinchew.com.my/founder_rec.jpg"></div>');
	 
	//var ads3 = ('<div class="nw-easy-guanggao"><div class="guanggaoword">广告</div><img src="https://www.sinchew.com.my/founder_rec.jpg"></div>');
 
	 //Rec 1,2,3
	var ads1 = ('<div class="nw-easy-guanggao"><span class="ad_words">广告 </span><div id="div-gpt-ad-1543077896195-0"><scr'+'ipt>googletag.cmd.push(function(){googletag.display("div-gpt-ad-1543077896195-0"); });</scr'+'ipt></div>');
	
	var ads2 = ('<div class="nw-easy-guanggao"><span class="ad_words">广告 </span><div id="div-gpt-ad-1543078174365-0"><scr'+'ipt>googletag.cmd.push(function(){googletag.display("div-gpt-ad-1543078174365-0"); });</scr'+'ipt></div>');

	var ads3 = ('<div class="nw-easy-guanggao"><span class="ad_words">广告 </span><div id="div-gpt-ad-1543078238595-0"><scr'+'ipt>googletag.cmd.push(function(){googletag.display("div-gpt-ad-1543078238595-0"); });</scr'+'ipt></div>');
	
	function advert(i)
	{
		if(i== 3)
		{
			$("#dirnum p").eq(i).after(ads1)
		}
		else if(i ==9)
		{
			$("#dirnum p").eq(i).after(ads2)
		}
		else if(i ==15)
		{
			$("#dirnum p").eq(i).after(ads3)
		}	
	}  	  
</script> 
	  
	<style>
		ul, li
		{
			list-style-type: none;
		}

		.neiwen_daohang
		{
			width: 95%;
			text-align: left;
			font-size: 16px;
			padding-top: 10px;
			margin: 0 auto;
		}

		.neiwen_hr
		{
			margin-top:10px;
			margin-bottom:10px;
		}

		.neiwen_container
		{
			margin:0 auto;
			width:95%;
			text-align:left;
		}

		.neiwen_interest_title
		{
			width:100%;
			height:auto;
			padding:10px;
			background:#e6e6e6;
			font-size:20px;
			margin-top:10px;
			font-weight:bold;
		}

		.neiwen_daohang a, .neiwen_daohang a:hover
		{
			text-decoration:none;
			color:#000000;
		}

		.neiwen_dashijian a, .neiwen_dashijian a:hover
		{
			text-decoration:none;
			color:black;
		}

		.othersnew_left, .comment_left
		{
			text-align:left;
			width:50%;
			color:#cd2026;
			font-weight:bold;
			display:inline-block;
			float:left;
			padding-bottom:15px;
			font-size:20px;
		}

		.othersnew_right, .comment_right
		{
			text-align:right;
			width:50%;
			font-weight:bold;
			display:inline-block;
			float:left;
			padding-bottom:15px;
			font-size:14px;
		}
		
		#forsharebutton img
		{
			width: 75px;
   			padding: 10px;
		}
		
		
		.a2a_default_style a 
		{
			float: left;
			line-height: normal !important;
			padding: 0px !important;
		}
		
		.neiwen-content img
		{
			max-width:100%;
			height:auto !important;
			text-align:center;
		}
		
		iframe
		{
			max-width:100% !important;
		}
		
		.neiwen-logn-guanggao, .neiwen-logn-guanggao img
		{
			width:100%;
		}
		
		video
		{
			max-width: 100% !important;
    		height: auto !important;
		}
		
		p 
		{
			margin: 0 0 24px;
		}
		
		.m-menu-son 
		{
			padding: 9px 9px 9px 9px !important;
			font-size: 18px;
			border-width: 1px 0px 1px 0px;
		}
		
		.m-menu-son2 
		{
    		font-size: 15px !important;
			padding: 6px 9px 6px 9px !important;
		}
		
		.ps__thumb-x
		{
			height:2px;
		}
	</style>
    {!! $options['{v3_style_tag}'] !!}
  </head>
  <body>
	<!-- 首页商标 -->
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WC3H2D');</script>
<!-- End Google Tag Manager -->

<script>
var script_tag = document.createElement('script'); script_tag.type = 'text/javascript'; script_tag.src = '//storage.googleapis.com/adasia-ad-network-origin/atm/library/avmLibrary.js'; script_tag.async = true; script_tag.onload = function() { var adAsiaTM = window.adAsiaTM || adAsiaTM, zoneList = []; adAsiaTM.init(zoneList);}; document.head.appendChild(script_tag);
</script>

<script>
	function showthirdlanmu()
	{
		var checkstatusofcollapse = document.getElementById("thirdlanmu_hideit1st").style.display;

		if (checkstatusofcollapse == "none" || checkstatusofcollapse == "")
			document.getElementById("thirdlanmu_hideit1st").style.display = "block";
		else
			document.getElementById("thirdlanmu_hideit1st").style.display = "none";
	}
</script>

<style>
	.headspinning, .headspinning:hover
	{
		text-decoration:none !important;
		padding:0 !important;
		font-size:14px !important;
		color:white !important;
	}
	
	.search-column
	{
		width:80%;
		margin:0 auto;
		text-align:center;
	}
	
	#toptenhuati
	{
		text-align:left;
		font-size:20px;
		margin: 0 auto;
		padding-top:15px;
		padding-bottom:15px;
	}
	
	.toptenhr
	{
		margin-bottom:10px;
		margin-top:10px;
	}
	
	.toptenlist
	{
		width: 70%;
		margin: 0 auto;
	}
	
	.toptenlist a, .toptenlist a:hover
	{
		padding: 3px;
		display: inline-block;
		font-size: 20px;
		color:black;
		text-decoration:none;
	}
	
	.header-top-topic
	{
		width: 72%;
		margin: 0 auto;
		text-align: left;
	}
	
	.header-top-topic a, .header-top-topic a:hover
	{
		text-decoration:none;
		display:inline-block;
		font-size: 18px;
		padding: 0px;
	}
	
	.top-left-hottopic-time
	{
		color:grey;
		display:inline-block;
		font-size: 12px;
		float: right;
	}
	
	.column_container
	{
		width:78%;
	}
	
	.column_container a, .column_container a:hover
	{
		font-size:21px;
		padding-bottom: 0px;
	}
	
	.pl-line
	{
		margin-bottom:0px;
		margin-top:0px;
		height:1px;
	}
	
	#show_column13, #show_column25, #show_column2, #show_column4, #show_column5, #show_column6, #show_column7, #show_column26, #show_column27, #show_column87, #show_column147, #show_column1491, #show_column1492, #show_column1494, #show_column1495, #show_column1496, #show_column1497, #show_column14926, #show_column14927
	{
		width: 7%;
	}
	
	#show_column13, #show_column25, #show_column2, #show_column4, #show_column5, #show_column6, #show_column7, #show_column26, #show_column27, #show_column87, #show_column147, #show_column1491, #show_column1492, #show_column1494, #show_column1495, #show_column1496, #show_column1497, #show_column14926, #show_column14927
	{
	    margin-top: -5px;
		vertical-align: middle;
	}
	
	.column-name
	{
		padding-bottom:0px;
	}
	
	.collapse.in 
	{
		padding-top: 8px;
	}
	
	.slideleftbarlistall
	{
		width: 70%;
		margin: 0 auto;
	}
	
	.top-left-hottopic-hr
	{
		margin-top:10px;
		margin-bottom:10px;
	}
	
	.pl-smallline
	{
		background-color:#f0f0f0;
	}
	
	.slideleftbarlistall a, .slideleftbarlistall a:hover 
	{
		font-size: 18px !important;
		padding: 2px !important;
	}
	
	#last-hr
	{
		margin-bottom:0px;
	}
	
	.mh-middle a
	{
		padding:5px 0px !important;
		height:65px !important;;
	}
	
	.background-f, .background-f img
	{
		width:100%;
		height:240px;
	}
	
	.goingup-f
	{
		margin-top :-240px;
		min-height:240px;
	}
	
	.background-ff, .background-ff img
	{
		width:100%;
		height:220px;
	}
	
	.goingup-ff
	{
		margin-top :-220px;
		min-height:220px;
	}
	
	#loginaftercheck
	{
		color:white;
	}
	
	.profileimgz
	{
		width: 100px;
		height: 100px;
		vertical-align: middle;
		margin: 0 auto;
		line-height: 100px;
	}
	
	.profileimgz img
	{
		max-height:100px;
		max-width:100px;
		border-radius: 50%;
	}
	
	.nav
	{
		font-size: 21px;
		padding-bottom: 0px;
		text-decoration: none;
		color: #cd2026;
	}
	
	.tab button
	{
		font-size:21px;
	}
	
	#topparis
	{
		width:65%;
		text-align:left !important;
	}
	
	.search-wording li
	{
		list-style-type: disc;
		padding-bottom:8px;
	}
	
	.search-wording ul
	{
		margin-block-start: 1em;
		margin-block-end: 1em;
		margin-inline-start: 0px;
		margin-inline-end: 0px;
		padding-inline-start: 40px;
	}
</style>

<div id="myNav" class="overlay"> <!-- All Overlay -->
	<div class="overlay-content"><!-- London and Paris and Content -->
		<div class="new-shangbiao">
			<div class="mh-topleft">
				<a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src="https://cdnpuc.sinchew.com.my/resource/iconxclose.png" style="width:27px;vertical-align:top;"></a>
			</div><!--
			--><div class="mh-middle">
				<a href="https://www.sinchew.com.my/pad/col/index.html" target="_self"><img src="https://cdnpuc.sinchew.com.my/resource/sinchew_logo_mobile.png" alt="" id="mh-button"></a>
			</div><!--
			--><div class="mh-topright">
				<img src="https://cdnpuc.sinchew.com.my/resource/nnsearch.png" id="mh-search" onclick="closeNav(),openNav2()">
			</div>
		</div>
		
		<div class="tab" style="margin-top:15px;">
			<div class="leftbuttonofburger"><button id="toplondon" class="tablinks active" onclick="openCity(event, 'London')">新闻</button></div><!--
			--><div class="rightbuttonofburger"><button id="topparis" class="tablinks" onclick="openCity(event, 'Paris')">大事件</button></div>
		</div>

<div id="London" class="tabcontent" style="overflow: hidden;">
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/nation.html">全国</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column13" data-toggle="collapse" data-target="#demo13" onclick="javascript:changearrow('13')">
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo13" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7075.html">即时</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_15.html">封面头条</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_16.html">暖势力</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_17.html">热点</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_18.html">政治</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_20.html">教育</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_19.html">社会</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_21.html">全国综合</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_22.html">求真</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7215.html">国会</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_23.html">专题</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_24.html">我们</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_6830.html">华社</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_4651.html">图说大马</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/world.html">国际</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column25" data-toggle="collapse" data-target="#demo25"
			onclick="javascript:changearrow('25')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo25" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7076.html">即时</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_29.html">天下事</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_30.html">国际拼盘</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_31.html">带你看世界</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/business.html">财经</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column2" data-toggle="collapse" data-target="#demo2"
			onclick="javascript:changearrow('2')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo2" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_33.html">即时</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_181.html">财经封面</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_34.html">市场盘点</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_35.html">企业动向</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_36.html">国际财经</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_37.html">投资法则</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_42.html">活用理财</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_206.html">产业兵法</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!--<div id="demo2" class="collapse">
		
								<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_33.html">即时</a>
					</div>
					<hr class="pl-smallline">
					<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_181.html">财经封面</a>
					</div>
					<hr class="pl-smallline">
					<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_34.html">市场盘点</a>
					</div>
					<hr class="pl-smallline">
					<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_35.html">企业动向</a>
					</div>
					<hr class="pl-smallline">
					<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_36.html">国际财经</a>
					</div>
					<hr class="pl-smallline">
					<!-- 这个二级栏目有三级栏目所以移除 并在下一个Div呈现 
					<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_42.html">活用理财</a>
					</div>
					<hr class="pl-smallline">
					<div class="slideleftbarlistall">
						<a href="https://www.sinchew.com.my/pad/col/node_206.html">产业兵法</a>
					</div>
					<hr class="pl-smallline">

				
		<!-- 在这边呈现3级栏目 列子 = 财经—>投资致富 ID 37 
		<div class="slideleftbarlistall">
			
									<div class="testthirdlanmu"><a href="https://www.sinchew.com.my/pad/col/node_37.html">投资法则</a></div>

		</div>
		
		<div id="showhidethirdlanmu" class="thirdlanmu-collapse" onclick="showthirdlanmu()"><span class="caret"></span></div>
		<div id="thirdlanmu_hideit1st">
			
				
		</div>
	</div>-->
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/opinion.html">言路</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column4" data-toggle="collapse" data-target="#demo4"
			onclick="javascript:changearrow('4')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo4" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_127.html">总编时间</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_119.html">风起波生</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_118.html">非常常识</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_4738.html">骑驴看本</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_129.html">亮剑</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_128.html">风雨看潮生</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_123.html">星期天拿铁</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_117.html">星．观点</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_115.html">纯粹诚见</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7250.html">开门见山</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7257.html">天马行空</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7254.html">一派胡言</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_4739.html">每日漫画</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/mykampung.html">地方</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column5" data-toggle="collapse" data-target="#demo5"
			onclick="javascript:changearrow('5')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo5" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/metro.html">大都会</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/johor.html">大柔佛</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/perak.html">大霹雳</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/sarawak.html">砂拉越</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/sabah.html">沙巴</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/northern.html">大北马</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/sembilan.html">花城</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/melaka.html">古城</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/m/eastcoast.html">东海岸</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_6951.html">年轻人</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_6952.html">有故事的人</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7183.html">地方味</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/entertainment.html">娱乐</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column6" data-toggle="collapse" data-target="#demo6"
			onclick="javascript:changearrow('6')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo6" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_66.html">国外娱乐</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_68.html">影视</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_65.html">大马娱乐</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_64.html">即时娱乐</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_67.html">星人物</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_72.html">娱乐圈喜事</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_69.html">明星二代</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_71.html">艺人八卦</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_70.html">星视镜</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/sport.html">体育</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column7" data-toggle="collapse" data-target="#demo7"
			onclick="javascript:changearrow('7')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo7" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7136.html">即时</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7137.html">封面</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_4757.html">论点</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_54.html">大马</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_55.html">羽球</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_56.html">足球</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_60.html">篮球</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_4758.html">水上</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_61.html">综合</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_58.html">聚焦人物</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_62.html">场外花絮</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_63.html">沿图细说</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/life.html">副刊</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column26" data-toggle="collapse" data-target="#demo26"
			onclick="javascript:changearrow('26')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo26" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_91.html">副刊专题</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_97.html">优质生活</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_105.html">旅游</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_106.html">美食</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_177.html">专栏</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_92.html">东西</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_94.html">新教育</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_95.html">e潮</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_96.html">艺文Show</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_98.html">护生</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_99.html">看车</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_100.html">养生</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_101.html">快乐家庭</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_103.html">文艺春秋</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_104.html">星云</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_107.html">非常人物</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_108.html">影音</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_109.html">读家</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7325.html">教育特辑</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name">
			<span class="nav"><a href="https://www.sinchew.com.my/m/pocketimes.html">百格</a></span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column27" data-toggle="collapse" data-target="#demo27"
			onclick="javascript:changearrow('27')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo27" class="collapse">
		
							<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_44.html">百格新闻</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7142.html">百格晨报</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7143.html">百格大事纪</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7144.html">财经Espresso</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7145.html">百格大家讲</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7146.html">On The Hot Seat</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7151.html">笔记本</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_7152.html">百格人物</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_46.html">百格纷</a>
				</div>
				<hr class="pl-smallline">
				<div class="slideleftbarlistall">
					<a href="https://www.sinchew.com.my/pad/col/node_47.html">百格特别企划</a>
				</div>
				<hr class="pl-smallline">

	</div>
	
	<!-------------------------------------------------------------------------------------------->
</div><!-- London End -->

<!-- Paris -->
<div id="Paris" class="tabcontent" style="overflow: hidden;">
	<div class="column_container">
		<div class="column_name" style="padding: 13px 13px 5px 13px;">
			<span class="nav">全国</span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-up.png" id="show_column147" data-toggle="collapse" data-target="#demo147"
			onclick="javascript:changearrow('147')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo147" class="collapse in" aria-expanded="true">
		
	</div>
	
	<script>headerhottopic(2,"demo147",5);</script>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name" style="padding: 13px 13px 5px 13px;">
			<span class="nav">国际</span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column1491" data-toggle="collapse" data-target="#demo1491"
			onclick="javascript:changearrow('1491')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo1491" class="collapse">
		
	</div>
	
	<script>headerhottopic(3,"demo1491",5);</script>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name" style="padding: 13px 13px 5px 13px;">
			<span class="nav">财经</span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column1492" data-toggle="collapse" data-target="#demo1492"
			onclick="javascript:changearrow('1492')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo1492" class="collapse">
		
	</div>
	
	<script>headerhottopic(4,"demo1492",5);</script>
	
	<!-------------------------------------------------------------------------------------------->
	
	<div class="column_container">
		<div class="column_name" style="padding: 13px 13px 5px 13px;">
			<span class="nav">体育</span>
		</div><!--
		--><div class="column_button">
			<img src="https://cdnpuc.sinchew.com.my/resource/icon-down.png" id="show_column1497" data-toggle="collapse" data-target="#demo1497"
			onclick="javascript:changearrow('1497')" >
		</div>
	</div>
	
	<hr class="pl-line">
	
	<div id="demo1497" class="collapse">
		
	</div>
	
	<script>headerhottopic(6,"demo1497",5);</script>
	
	<!-------------------------------------------------------------------------------------------->
</div><!-- Paris End -->	

	<div class="allz" id="changetocorrectuser" style="margin-top:10px;"></div><!--
	
	--><div class="container_for_container">
		<div class="bottom_social">
			<div class="social_title" style="color:black;">关注我们<br><br></div>
			<div class="social_img">
				<a href="https://www.facebook.com/SinChewDaily/" class="social_ahref" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-fb.png" class="social_image_size"></a>
				<a href="https://twitter.com/SinChewPress" class="social_ahref" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-twitter.png" class="social_image_size"></a>
				<a href="https://www.youtube.com/channel/UCKC1q1zc_CG2MtOCEHNUt1Q" class="social_ahref" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-youtube.png" class="social_image_size"></a>
				<a href="https://www.instagram.com/sinchewdaily/" class="social_ahref" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-instagram.png" class="social_image_size"></a>
				<a href="https://www.weibo.com/sinchewi" class="social_ahref" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-weibo.png" class="social_image_size"></a>
				<a href="https://www.sinchew.com.my/wechat" class="social_ahref" target="_blank"><img src="https://cdnpuc.sinchew.com.my/resource/icon-wechat.png" class="social_image_size"></a>
			</div>
		</div>
		
		<div class="downloadmattop">
			<div style="line-height:35px;" >
				<div class="social_title" style="color:black;">现在就下载星洲网APP</div>
			</div>
			<div class="ham_b" style="width:100%;display:inline-block;height:63px;">	
				<div style="width:45%;display:inline-block;height:63px;">
					<a href="https://itunes.apple.com/my/app/sin-chew-%E6%98%9F%E6%B4%B2%E6%97%A5%E6%8A%A5/id1077727843?mt=8" target="_blank">
					<img src="https://cdnpuc.sinchew.com.my/resource/apple-app.png" style="width:100%;"></a>&emsp;
				</div>
				<div style="width:45%;display:inline-block;vertical-align:top;">
					<a href="https://play.google.com/store/apps/details?id=com.sinchewnews&hl=en_US" target="_blank">
					<img src="https://cdnpuc.sinchew.com.my/resource/google-app.png" style="width:100%;"></a>
				</div>
				<div style="clear:both;"></div>
			</div>
			<div style="width:100%;text-align:center;margin:0 auto;font-weight:bold;margin-top:-15px;">
				<a href="https://www.sinchew.com.my/m/newsletter.html" style="color:black;text-decoration:none;font-size:18px;">订阅Newsletter (电邮新闻)</a>
			</div>
		</div>
	</div>
</div><!-- Paris and London and Content -->
</div><!-- Overlay End -->
	
<div id="myNav2" class="overlay2"> <!-- All Overlay -->
	<div class="overlay-content2">
		<div class="new-shangbiao">
			<div class="mh-topleft">
				<img src="https://cdnpuc.sinchew.com.my/resource/nnham.png" id="mh-logo" onclick="openNav(),closeNav2()">
			</div><!--
			--><div class="mh-middle">
				<a href="https://www.sinchew.com.my/pad/col/index.html" target="_self"><img src="https://cdnpuc.sinchew.com.my/resource/sinchew_logo_mobile.png" alt="" id="mh-button"></a>
			</div><!--
			--><div class="mh-topright">
				<a href="javascript:void(0)" class="closebtn" onclick="closeNav2()"><img src="https://cdnpuc.sinchew.com.my/resource/iconxclose.png" style="width:27px;vertical-align:top;"></a>
			</div>
		</div>
		
		<hr style="margin-top: 10px;margin-bottom: 10px;">

		<div class="clearall"></div>
		
		<div class="search-column">
			<!--<form name="form1" action="https://www.sinchew.com.my/search/servlet/SearchServlet.do" method="POST" style="margin:0px; padding:0px;display: inline-block;">-->
				<div class="col-lg-6">
					<div class="input-group">
						<input type="text" class="form-control" name="contentKey3" id="contentKey3">
						<span class="input-group-btn">
						<button class="btn btn-default" id="enterkeydetect2" type="button" onclick="javascript:search()" class="searchbutton">搜索</button>
						</span>
					</div><!-- /input-group -->
				</div><!-- /.col-lg-6 -->
				<input type="hidden" name="op" value="single">
				<input type="hidden" name="siteID" value="">
				<input type="hidden" name="changeChannel" value="">
			<!--</form>-->
		</div>
		
		<div id="toptenhuati" style="display:block;"></div>
		<script>gettopten("toptenhuati",10)</script>
		
		<div class="search-wording">
			<ul>
				<li>请用简体字进行搜索，以便得出更精准的搜索结果。</li>
				<li>请检查你的搜索字眼是否正确。</li>
				<li>去掉搜索字眼周围的引号来进行单个单词的匹配: "blue drop" 匹配度会比 blue drop低.</li>
			</ul>
		</div>
	</div>
</div>

<script>
	function openNav() 
	{
	  document.getElementById("myNav").style.display = "block";
	}

	function closeNav() 
	{
	  document.getElementById("myNav").style.display = "none";
	}
	
	function openNav2() 
	{
	  document.getElementById("myNav2").style.display = "block";
	}

	function closeNav2() 
	{
	  document.getElementById("myNav2").style.display = "none";
	}

	function changearrow(id)
	{
		var sourceofimg = $('#show_column'+id).attr('src');
		if(sourceofimg == "https://cdnpuc.sinchew.com.my/resource/icon-up.png")
			$('#show_column'+id).attr("src","https://cdnpuc.sinchew.com.my/resource/icon-down.png");
		else
			$('#show_column'+id).attr("src","https://cdnpuc.sinchew.com.my/resource/icon-up.png");
	}
	
	var input = document.getElementById("contentKey3");

	input.addEventListener("keyup", function(event) {
	  event.preventDefault();
	  if (event.keyCode === 13)
	  {
		document.getElementById("enterkeydetect2").click();
	  }
	});
	
	function openCity(evt, cityName) 
	{
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) 
		{
			tabcontent[i].style.display = "none";
		}
		
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) 
		{
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}
	
	function search()
	{
		if(document.getElementById("contentKey3").value == "")
			alert("搜索不能为空！");
		else
		{
			var contentKey = document.getElementById("contentKey3").value;
			window.location.href = "https://www.sinchew.com.my/pad/col/search.html?searchkey=" + contentKey;
		}
	}
	
	var userID = doGetCookie("uid_fouNder");
	
	function doGetCookie(uid)
	{
		var aCookie = document.cookie.split("; ");
		for (var i = 0; i < aCookie.length; i++) 
		{
		  var aCrumb = aCookie[i].split("=");
		  if (uid == aCrumb[0])
			return unescape(aCrumb[1]);
		}
		return "false";
	}
	
	function ssologin()
	{
		window.open("https://sso.sinchew.com.my:1443/SSOv2/user/ssoLogin?code=" + myFav.authCode + "&redirectUrl=" + myFav.login_out + "sso/setCookie.html?&anyUrl=" + location.href, "newwindow", myFav.window_site2);
	}
	
	function getuserinfo(userIDfromcookie)
	{
		var url = 'https://sso.sinchew.com.my:1443/SSOv2/api/user/info?uid='+userIDfromcookie;
		  $.ajax ({
			url:url,
			type:'GET',
			dataType:'json',
			success:function(data)
			{	
				//data.username;
				//document.getElementById("changetoname").innerHTML = data.username;
				//document.getElementById("changetonameagain").innerHTML = "你好 " + data.username;
				
				var realimg;
				var realname;
				
				data.head == "" ? (realimg = "https://cdnpuc.sinchew.com.my/resource/icon-avatar.png") : (realimg = data.head);
				data.nickname == "" ? (realname = "电邮 : " + data.email) : (realname = "姓名 : " + data.nickname);
				
				
				document.getElementById("changetocorrectuser").innerHTML = "<div class='background-ff'><img src='https://cdnpuc.sinchew.com.my/resource/bg.png'></div><div class='goingup-ff'><br><span style='color:white;'>" + realname + "</span><br><br><div class='profileimgz'><img src='" + realimg + "'></div><br><a href='https://www.sinchew.com.my/pad/col/personal.html?collect' class='headspinning' style='padding-bottom:10px;padding-top:10px;display:inline-block;'>我的收藏</a>&#9;&#9;<a href='https://www.sinchew.com.my/pad/col/personal.html' class='headspinning' style='padding-bottom:10px;padding-top:10px;display:inline-block;'>编辑个人资料</a>&#9;&#9;<a href='#' onclick='logoutnow()'  class='headspinning' style='padding-bottom:10px;padding-top:10px;display:inline-block;'>登出</a><br></div><!-- login inform -->";
			}
		}) 
	}
	
	function logoutnow()
	{
		window.open("https://sso.sinchew.com.my:1443/SSOv2/user/ssoLogout?code=" + personalMessage.authCode + "&uid=" + personalMessage.uid + "&sso_token=" + personalMessage.doGetCookie("sso_token") + "&from=" + personalMessage.login_out + "sso/delCookie.html", "newwindow", personalMessage.window_site2);
		
		setTimeout(function () {
			location.reload();
		}, 3000);
	}
	
	function registermeme()
	{
		window.open("https://sso.sinchew.com.my:1443/SSOv2/user/register?siteId=", "newwindow", personalMessage.window_site2);
	}
	
	if(userID != "false" && userID != "")
	{
		//登陆了
		getuserinfo(userID);
	}
	else
	{
		//$("#changetocorrectuser").html("<div id='loginaftercheck' onclick='javascript:ssologin();'>登录 |</div>");
		$("#changetocorrectuser").html("<div class='background-ff'><img src='https://cdnpuc.sinchew.com.my/resource/bg.png'></div><div class='goingup-ff'><br><div id='loginaftercheck' onclick='javascript:ssologin();'>点击登录</div><br><img src='https://cdnpuc.sinchew.com.my/resource/icon-avatar.png' style='border-radius: 50%;width:100px;height:100px;'><br><br><a href='#' onclick='registermeme()' class='headspinning' style='padding-bottom:10px;padding-top:10px;display:inline-block;'>没有账号？点击这里注册</a><br></div>");
	}
</script>

<div class="header1">
	<div class="mh-topleft">
	<img src="https://cdnpuc.sinchew.com.my/resource/nnham.png" id="mh-logo" onclick="openNav(),closeNav2()">
	</div><!--
	--><div class="mh-middle">
		<a href="https://www.sinchew.com.my/pad/col/index.html" target="_self"><img src="https://cdnpuc.sinchew.com.my/resource/sinchew_logo_mobile.png" id="mh-button"></a>
	</div><!--
	--><div class="mh-topright">
		<img src="https://cdnpuc.sinchew.com.my/resource/nnsearch.png" id="mh-search" onclick="openNav2(),closeNav()">
		
	</div>
</div>
	  
	<!-- Menubar 第一层 -->
	<script>
	//when touch or click hide this
	$(document).on('click touchstart', function(){
	  $("#dropdown-menu").hide();
	});
	
	$(document).ready(function() {
		//skip it when touch or click this 2 div
		$("#dropdown-menu").on('click touchstart', function(e){
		  e.stopPropagation();
		});
		
		$("#skipthis").on('click touchstart', function(e){
		  e.stopPropagation();
		});
	});
	
	//normal show hide js
	function openmylist()
	{
		var checkdisplay_kuaibao = document.getElementById("dropdown-menu").style.display;
		
		if(checkdisplay_kuaibao == "none" || checkdisplay_kuaibao == "")
		{
			document.getElementById("dropdown-menu").style.display = "block";
		}
		else
		{
			document.getElementById("dropdown-menu").style.display = "none";
		}
	}
</script>

<style>
.m-menu-son
{
	cursor:pointer;
}

.m-menu-son 
{
	padding: 9px 9px 9px 9px !important;
	font-size: 18px;
	border-width: 1px 0px 1px 0px;
}

.m-menu-son2 
{
	font-size: 15px !important;
	padding: 6px 9px 6px 9px !important;
}
</style>

<div id="m-menu-container">
	<div class="m-menu-son" id="menuson135">
		<a href="https://www.sinchew.com.my/pad/col/index.html" id="af135">首页</a>
	</div><!--
	--><div class="m-menu-son" id="menuson6908">
		<a href="https://www.sinchew.com.my/pad/col/node_6908.html" id="af6908">最新</a>
	</div><!--
	--><div class="m-menu-son" id="menuson89">
		<a href="https://www.sinchew.com.my/pad/col/node_89.html" id="af89">热门</a>
	</div><!--
	--><div class="m-menu-son" id="menuson13">
		<a href="https://www.sinchew.com.my/m/nation.html" id="af13">全国</a>
	</div><!--
	--><div class="m-menu-son" id="menuson25">
		<a href="https://www.sinchew.com.my/m/world.html" id="af25">国际</a>
	</div><!--
	--><div class="m-menu-son" id="menuson27">
		<a href="https://www.sinchew.com.my/m/pocketimes.html" id="af27">百格</a>
	</div><!--
	--><div class="m-menu-son" id="menuson2">
		<a href="https://www.sinchew.com.my/m/business.html" id="af2">财经</a>
	</div><!--
	--><div class="m-menu-son" id="menuson4">
		<a href="https://www.sinchew.com.my/m/opinion.html" id="af4">言路</a>
	</div><!--
	--><div class="m-menu-son" id="menuson5">
		<a href="https://www.sinchew.com.my/m/mykampung.html" id="af5">地方</a>
	</div><!--
	--><div class="m-menu-son" id="menuson6">
		<a href="https://www.sinchew.com.my/m/entertainment.html" id="af6">娱乐</a>
	</div><!--
	--><div class="m-menu-son" id="menuson7">
		<a href="https://www.sinchew.com.my/m/sport.html" id="af7">体育</a>
	</div><!--
	--><div class="m-menu-son" id="menuson26">
		<a href="https://www.sinchew.com.my/m/life.html" id="af26">副刊</a>
	</div><!--
	--><div class="m-menu-son" id="menuson88">
		<a href="https://www.sinchew.com.my/pad/col/node_88.html" id="af88">图辑</a>
	</div><!--
	--><div class="m-menu-son" id="menuson9">
		<a href="https://www.sinchew.com.my/pad/col/node_9.html" id="af9">资讯</a>
	</div><!--
	--><div class="m-menu-son" id="menuson106">
		<a href="https://www.sinchew.com.my/pad/col/node_106.html" id="af106">美食</a>
	</div>

    <script>
   		var ps = new PerfectScrollbar('#m-menu-container', {
      		suppressScrollY: true
    	});
    </script>
</div>

	
		<script>
							var element = document.getElementById("af5");
				var scrolltomidba = ['2','4','5','6','7','26','88','9','106'];

				if(element != null)
				{
					element.classList.add("changeahref");
				}

				for (var a=0; a<scrolltomidba.length; a++)
				{
					if(5 == scrolltomidba[a])
					{
						var getwidth = (a+6)*38; //直接算因为 1st menubar 的 div width 一样
						$('#m-menu-container').scrollLeft(getwidth);
					}
				}
		</script>

	<!-- 第一层 menubar END -->
	 
	<!-- 第二层 menubar -->
	<!-- 当前栏目 显示红色 -->
	
			<style>
			#mt5 a, #mt5 a:hover
			{
				color:#cd2026 !important;
			}
		</style>

	
	<style>
		#m-menu-container2
		{
			height: auto;
			width: 100%;
			overflow: hidden;
			position: relative;
			white-space:nowrap;
			background-color:#e2e2e2;
		}

		.m-menu-son2
		{
			position:relative;
			display:inline-block;
			padding:5px 5px 5px 5px;
			color:black;
		}

		.m-menu-son2 a, .m-menu-son2 a:hover
		{
			color:black;
			text-decoration:none;
		}

		#dirnum
		{
			font-size: 21px;
			line-height: 33px;
		}
	</style>
	 
	<script>
		var mybrotherurl = new Array;
		var mybrothername = new Array;
		var mybrotherid = new Array;
		var mysonurl = new Array;
		var mysonname = new Array;
		var mysonid = new Array;
	</script>
	  
	
		<script>
					var difang = "大柔佛";
			var difangurl = "https://www.sinchew.com.my/m/johor.html";
		</script>

	  
	
		<script>
					mybrotherurl.push("https://www.sinchew.com.my/m/metro.html");
			mybrothername.push("大都会");
			mybrotherid.push("74");
			mybrotherurl.push("https://www.sinchew.com.my/m/johor.html");
			mybrothername.push("大柔佛");
			mybrotherid.push("78");
			mybrotherurl.push("https://www.sinchew.com.my/m/perak.html");
			mybrothername.push("大霹雳");
			mybrotherid.push("77");
			mybrotherurl.push("https://www.sinchew.com.my/m/sarawak.html");
			mybrothername.push("砂拉越");
			mybrotherid.push("86");
			mybrotherurl.push("https://www.sinchew.com.my/m/sabah.html");
			mybrothername.push("沙巴");
			mybrotherid.push("84");
			mybrotherurl.push("https://www.sinchew.com.my/m/northern.html");
			mybrothername.push("大北马");
			mybrotherid.push("75");
			mybrotherurl.push("https://www.sinchew.com.my/m/sembilan.html");
			mybrothername.push("花城");
			mybrotherid.push("81");
			mybrotherurl.push("https://www.sinchew.com.my/m/melaka.html");
			mybrothername.push("古城");
			mybrotherid.push("80");
			mybrotherurl.push("https://www.sinchew.com.my/m/eastcoast.html");
			mybrothername.push("东海岸");
			mybrotherid.push("79");
			mybrotherurl.push("https://www.sinchew.com.my/pad/col/node_6951.html");
			mybrothername.push("年轻人");
			mybrotherid.push("6951");
			mybrotherurl.push("https://www.sinchew.com.my/pad/col/node_6952.html");
			mybrothername.push("有故事的人");
			mybrotherid.push("6952");
			mybrotherurl.push("https://www.sinchew.com.my/pad/col/node_7183.html");
			mybrothername.push("地方味");
			mybrotherid.push("7183");
		</script>

	  
	
		<script>
				</script>

	
	<div id="m-menu-container2"></div>
	
	<script>
		$("#m-menu-container2").append('<div style="display:inline-block;padding:6px 0px 6px 9px;font-size:15px;"><a style="text-decoration:none;color:black;" href="' + difangurl + '">' + difang + '</a> &#9658;</div>');
		
		if (mysonurl.length != "") 
		{
			for (var i = 0; i < mysonurl.length; i++) 
			{
				if(mysonname[i] == "话题")
				{
					//do nothing
				}
				else
				{
					$("#m-menu-container2").append('<div class="m-menu-son2" id="mt' + mysonid[i] + '"><a href="' + mysonurl[i] + '">' + mysonname[i] + '</a></div>');
				}

			}
		}
		else 
		{
			for (var i = 0; i < mybrotherurl.length; i++) 
			{
				if(mybrothername[i] == "话题")
				{
					//do nothing
				}
				else
				{
					$("#m-menu-container2").append('<div class="m-menu-son2" id="mt' + mybrotherid[i] + '"><a href="' + mybrotherurl[i] + '">' + mybrothername[i] + '</a></div>');
				}
			}
		}

		$("#m-menu-container2").append('<div id="mthuati" style="position: relative;display: inline-block; display:none;"><span style="color:black;padding:5px;">话题</span><span style="color:#207062;">&#9658;</span></div>');
	</script>
	
	<script> 
		var athere;
		var fzarray = [13,25,2,4,5,6,7,88,26,27];
		var htarray = [2,3,4,5,9,7,6,8,10,11];
	</script>
	
					<script>
				for(var k=0;k<fzarray.length;k++)
				{
					if(5 == fzarray[k])
					{
						athere = htarray[k];
					}
				}

				if (mysonid.length != "")
				{
					var get2width;

					for (var i = 0; i < mysonid.length; i++)
					{
						if(5 == mysonid[i])
						{
							$('#m-menu-container2').scrollLeft(get2width);
						}
						else
						{
							var addupallwidth = "mt5";
							var exampleelement = document.getElementById(addupallwidth);

							if(exampleelement != null)
							{
								get2width = get2width + exampleelement.offsetWidth;
							}
						}
					}
				}
				else
				{
					var get2width = 0;

					for (var i = 0; i < mybrotherid.length; i++)
					{
						if(5 == mybrotherid[i]) //如果对应 则 scroll 去 get2width
						{
							$('#m-menu-container2').scrollLeft(get2width);
						}
						else
						{
							var addupallwidth = "mt5";
							var exampleelement = document.getElementById(addupallwidth);

							if(exampleelement != null) //如果有这个栏目
							{
								get2width = get2width + exampleelement.offsetWidth; //提取当前的div的width 并相加 去get2width
							}
						}
					}
				}
			</script>

	<script>gethotarticle(athere,"mthuati",3)</script>
	  
	<script>
		var ps = new PerfectScrollbar('#m-menu-container2');
	</script>
	<!-- 第二层 menubar END -->

	<!-- 快報 -->
	  <script>
  function text_truncate(str, length, ending) {
    if (length == null) {
      length = 100;
    }
    if (ending == null) {
      ending = '...';
    }
    if (str.length > length) {
      return str.substring(0, length - ending.length) + ending;
    } else {
      return str;
    }
  };
  </script>

  <!-- Demo styles -->
	<style>

	.kuaibao-container
	{
		width:100%;
		position:relative;
		background-color:#cd2026;
	}

	#swiper-container 
	{
		height: auto;
		display:inline-block;
		vertical-align:middle;
		background-color:#cd2026;
		padding:5px;
	}

	#swiper-slide1, #swiper-slide2, #swiper-slide3, #swiper-slide4
	{
		text-align: left;
		background: #cd2026;
		color:white;

		/* Center slide text vertically */
		display: -webkit-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		/*-webkit-box-pack: center;
		-ms-flex-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		align-items: center;*/
		padding-left:8px;
	}

	.kuaibao-left-true
	{
		display:inline-block;
		vertical-align:middle;
		padding:5px;
		background-color:#cd2026;
		color:white;
		text-align: center;
	}

	.kuaibao-link
	{
		color:white;
		text-decoration:none;
	}

	.kuaibao-link:hover
	{
		color:white;
		text-decoration:none;
	}

	.swiper-button-next
	{
		outline:none;
	}
	
	@media screen and (min-width: 351px) and (max-width: 400px) 
	{
		.swiper-slide
		{
			font-size:15px;
		}
		
		.kuaibao-left-true
		{
			font-size:15px;
		}
	}
		
	@media only screen and (min-width: 401px) and (max-width: 450px) 
	{
		.swiper-slide
		{
			font-size:18px;
		}
		
		.kuaibao-left-true
		{
			font-size:18px;
		}
	}
		
	@media only screen and (min-width: 451px) and (max-width: 500px) 
	{
		.swiper-slide
		{
			font-size:20px;
		}
		
		.kuaibao-left-true
		{
			font-size:20px;
		}
	}
		
	@media only screen and (min-width: 501px) and (max-width: 600px) 
	{
		.swiper-slide
		{
			font-size:22px;
		}
		
		.kuaibao-left-true
		{
			font-size:22px;
		}
	}
	</style>
 <!-- Swiper -->
 
 <div clss="kuaibao-container">
	<div class="kuaibao-left-true">快报 | </div><!--
  --><div class="swiper-container" id="swiper-container">
    <div class="swiper-wrapper">
		 <div class="swiper-slide" id="swiper-slide1"></div>
		 <div class="swiper-slide" id="swiper-slide2"></div>
		 <div class="swiper-slide" id="swiper-slide3"></div>
		 <div class="swiper-slide" id="swiper-slide4"></div>
    </div>
    <!-- Add Arrows -->
	<div class="swiper-button-next"></div>
  </div>
</div>

<script>var i=1;</script>


			<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044865.html">' + text_truncate("手套能源公用事业领跌·马股先盛后衰",16) + '</a>';
			i++;
		</script>
		<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044606.html">' + text_truncate("《南华》：敦马参访释信号·华为争马5G市场 获“认可”",16) + '</a>';
			i++;
		</script>
		<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044786.html">' + text_truncate("复活节爆炸案罹难人数·斯里兰卡下修至253人",16) + '</a>';
			i++;
		</script>
		<script>
			var testvariable = "swiper-slide"+i;
			document.getElementById(testvariable).innerHTML = '<a class="kuaibao-link" href="https://www.sinchew.com.my/content/content_2044791.html">' + text_truncate("​首季业绩报捷·大马回险涨4.37%·马股开高走低",16) + '</a>';
			i++;
		</script>


<!-- Swiper JS -->
<script src="https://www.sinchew.com.my/resource/swiper.js?"></script>

<!-- Initialize Swiper -->
<script>
var swiper = new Swiper('#swiper-container', {
	loop: true,
  navigation: {
	nextEl: '.swiper-button-next',
  },
   autoplay: {
	delay: 2500,
	disableOnInteraction: false,
  },
});
</script>

<div class="clearall"></div>
	  
	 
	<!-- 导航 列子 娱乐->即时娱乐 -->
	<div class="neiwen_daohang">
		
								<!-- do nothing -->
				<span class="nav1"><a href="https://www.sinchew.com.my/mykampung.html" >地方</a></span>

	</div>
	<!-- 导航 END -->

	<hr class="neiwen_hr">
	  
    <div class="neiwen_container"> <!-- neiwen container -->
     	<div class="neiwen_time_title">
			
				<div id="articlenum">
				2019-04-26 10:25:25
				<articleID>2044619</articleID>
				<div class="articleId" style="display:none;">2044619</div>
				<img src="https://www.sinchew.com.my/resource/icon2-views.png" class="dianji-img"><!--
				--><div id="clicknum"><span id="spid2044619"></span></div><!--
				--><img src="https://www.sinchew.com.my/resource/icon2-comments.png" class="pinglun-img"><!--
				--><div id="disnum"><span id="spid2044619"></span></div>
				</div>
				<h3 style="font-size: 33px;font-weight: normal;color:#121212;">士乃中总及村民协会．6月23联办周年联欢宴</h3>
				<a style="text-decoration:none;color:#207062;" href="https://www.sinchew.com.my/m/johor.html" >大柔佛</a>
				
				<div style="float:right;width:95%;margin:0 auto;text-align:right;padding-bottom:8px;">
					<button class="hasFav"></button>
					<span class="hasFav_des" style="display:none;">收藏</span>
				</div>

				<div style="clear:both;"></div>
							
				
				<!-- lead Ad  -->
				<div class="neiwen-logn-guanggao">
					<div style="max-width:320px;margin:0 auto;text-align:left;"><span class="ad_words">广告 </span>
						<div id='div-gpt-ad-1543077840668-0'>
						<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1543077840668-0');});</script>
						</div>
					</div><br/>
				</div>
				
				<div id="dirnum" class="neiwen-content"><!--enpcontent--><p style="white-space: normal;"></p><figure class="imgComment_figure" id="1556245448045" style="text-align: center;position:relative;margin: 0px"><img id="../../xy/image.do?path=图片存储;xy/201904/25/6a5d051c-3214-43da-b330-e3270840440a.jpg" src="https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg.2" title="1662NHK20194251822452336007.jpg" alt="1662NHK20194251822452336007.jpg" style="max-width: 100%; width: 900px; height: 327px;" data_ue_src="https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg.2" width="900" height="327" border="0" imgtime="1556245448045"/><figcaption id="figures_1556245448045" class="imgComment_content" style="top:326px;line-height:20px;padding: 0px 0px 0px;font-size: 0.9em;margin: 5px auto 0px;color: gray;text-align: center;word-wrap: break-word;width:100%">士乃中华总商会董事合照。前排左起为刘国基、陈贵友、蔡明酒、唐亚进、黄苟等。</figcaption></figure><p></p><p><br/></p><p>（古来25日讯）士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。</p><p><br/></p><p>当晚，士乃中华总商会将庆祝成立70周年、士乃村民协会庆祝成立47周年，两会也将同时举行新届董事、理事、青年团及妇女组就职典礼。<br/></p><p><br/></p><p>席金每桌700令吉，捐1000令吉或以上者为大会彩剪人，2000令吉或以上者为大会鸣锣人。<br/></p><p><br/></p><p>有意购席或捐款者，可洽陈贵友（019-711 1289）、黄苟（016-715 8786）、唐亚进（012-738 0206）、蔡明酒（013-799 7288）、张伟成（012-737 1533）、刘国基（019-771 3211）或刘治平（016-763 5355）。<br/></p><p><br/></p><p>陈贵友:加强联系发展业务 <br/></p><p><br/></p><p>士乃中华总商会甫上任的会长陈贵友接受星洲日报《大柔佛》社区报访问时表示，他寄望在新董事及青商团领导的合作下，加强商务方面的发展，让会员能够通过商会的平台联系在一起，加强生意上的发展。<br/></p><p><br/></p><p>他披露，该会目前有140名会员，年轻会员的人数还在不断增加中，该会将加强会员间的联系及提供更多资讯给会员，让大家有机会加强业务的发展。<br/></p><p><br/></p><p>他指出，士乃是柔州唯一拥有机场的地区，希望政府能够提升飞机场的价值，包括开拓更多国际航线、引进更多外来投资及旅客，以带动士乃甚至是整个柔州的经济发展。<br/></p><p><br/></p><p>他也希望更多青年及商家能够加入该会成员会员，协助推动该会会务，并借此平台与全国各地的商家交流取经，以在商务发展上取得双赢的优势。&nbsp;</p><p><br/></p><p><strong>中华总商会新届董事名表：</strong><br/></p><p>会长：陈贵友，署理：黄苟，副：廖迪传、唐亚进；总务：蔡明酒，副：张伟成；财政：刘国基，副：温俊文；政府事务主任：张稼禄，副：黄复达；教育主任：林伟生，副：陈汶安；宣传主任：刘治平，副：陈祈升；文书：刘治平（中）、彭浚宏（英）；交际：黄传燊、蔡进朋、曾进兴；义山主任：黄苟，副：蔡明酒；福利主任：曾妙富；董事：林金树、蔡伟雄、蔡永德；查账：彭迎华、曾英强。 <br/><br/></p><p><br/></p><p><strong>士乃青商团理事名表：</strong><br/></p><p>团长: 黄传燊，副: 彭威能、郑清方、谢副麟；总务: 蔡明权，副: 林胜源，黄意雄；<br/></p><p>财政：钟慧贞；秘书: 管偲琪，副：陈姝璇；交际: 许时嘉、黄治崴、黄锦文、符致陖、张健声。<br/></p><p><br/></p><p><strong>士乃村民协会理事名表：</strong><br/></p><p>主席：蔡明酒，署理：廖迪传，副：林伟生、李玉英；总务：刘治平，副：张伟成；财政：黄苟，副：廖诗云；教育主任：李秀兰，副：温俊文；文书：叶清敬；交际：郑凌蓉，副：刘嫚莉；外丹功主任：冯国金，副：曾妙富；康乐／卡拉OK主任：马金英，副：彭美娇；理事：温洪、张雅妹、蔡傍花、邓吉临、张秀萍、黄亚香、蔡舍妹、邱义翔；查账：彭俊鸿、温祥益。 <br/></p><p><br/></p><p></p><figure class="imgComment_figure" id="1556245518136" style="text-align: center;position:relative;margin: 0px"><img id="../../xy/image.do?path=图片存储;xy/201904/25/efa9c1a3-54a3-4111-be5f-1d45855be670.jpg" src="https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/efa9c1a3-54a3-4111-be5f-1d45855be670_zsize.jpg.2" style="max-width: 100%; width: 500px; height: 660px;" title="1662NHK20194251822462336008.jpg" alt="1662NHK20194251822462336008.jpg" data_ue_src="https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/efa9c1a3-54a3-4111-be5f-1d45855be670_zsize.jpg.2" width="500" height="660" border="0" imgtime="1556245518136"/><figcaption id="figures_1556245518136" class="imgComment_content" style="top:660px;line-height:20px;padding: 0px 0px 0px;font-size: 0.9em;margin: 5px auto 0px;color: gray;text-align: center;word-wrap: break-word;width:100%">陈贵友呼吁政府，在士乃机场开拓更多国际航线。</figcaption></figure><p></p><p><br/></p><p><br/></p><!--/enpcontent--><!--enpproperty <articleid>2044619</articleid><date>2019-04-26 10:25:25:214</date><author></author><title>士乃中总及村民协会．6月23联办周年联欢宴</title><keyword></keyword><subtitle></subtitle><introtitle></introtitle><siteid>1</siteid><nodeid>78</nodeid><nodename>大柔佛</nodename><nodesearchname></nodesearchname><picurl>https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picurl><picbig>https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/t2_(75X0X452X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picbig><picmiddle>https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/t1_(115X0X415X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picmiddle><picsmall>https://cdnpuc.sinchew.com.my/pad/pic/2019-04/26/t0_(112X0X489X217)6a5d051c-3214-43da-b330-e3270840440a_zsize.jpg</picsmall><url>https://www.sinchew.com.my/content/content_2044619.html</url><urlpad>https://www.sinchew.com.my/pad/con/content_2044619.html</urlpad><liability></liability><sourcename>星洲日报</sourcename><abstract>士乃中华总商会及士乃村民协会定于6月23日（星期日）晚上7时，在古来太子城福临门酒家联合举办周年庆及就职联欢晚宴。</abstract><channel>2</channel>/enpproperty--></div>
				<div class="wycamefrom">
<br/>
					
					
					文章来源 ： 
						星洲日报  2019-04-26
							
				</div>

			
			<!-- Pocketimes IVS Widget  -->			
			<!-- IVS Video Block  -->			
<div style="width:100%;margin-top:20px;margin-bottom:20px;">
    <hr style="border-top: 2px solid #eee;"></hr>
    
    {{-- <script id="ivsn" src="https://player.ivideosmart.com/ivideosense/player/js/ivsnload_v1.js?key=x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K&wid=441850eb-7398"></script> --}}

    <div>
        <ivs-video
            thumbnail="https://2.bp.blogspot.com/-xpgIOttwTTg/TWRni-cC-tI/AAAAAAAAA-o/9GMqFyMnQCY/s1600/african-elephant2.jpg"
            id="ivsplayer001"
            :id-i-frame="idIFrame"
          ></ivs-video>
          <ivs-playlist
            type="carousel"
            widget-id="441850eb-4927"
            api-key="e48e3f141bc2312a579dc37f38e655bf"
            :playlist-data="playlistData"
            ivsplayer-id="ivsplayer001"
          ></ivs-playlist>
    </div>
</div>
			
			<!-- BottomBanner_ROS Ad  -->
			<div class="neiwen-logn-guanggao">
				<div style="max-width:320px;margin:0 auto;text-align:left;"><div><span class="ad_words">广告 </span>
					<div id='div-gpt-ad-1542969361433-0'>
					<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1542969361433-0');});</script>
					</div>
					</div>
				</div>
			</div>
			
			<!-- 1x1 Ad  -->
			<div id='div-gpt-ad-1543077774044-0'>
				<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1543077774044-0');});</script>
			</div>
					
		<script>
			var cpj = document.getElementById("dirnum").innerHTML;
			var newcpj = cpj.replace(/<p style=""><br><\/p>/g, "<p style='display:none'><br></p>");
			var new2cpj = newcpj.replace(/<p><br><\/p>/g, "<p style='display:none'><br></p>");
			document.getElementById("dirnum").innerHTML = new2cpj;
			</script>			

		</div>
		
		<br/>
		
		<div class="neiwen_share_container" style="width:100%;margin:0 auto;text-align:center;padding-bottom: 8px;">
			<div class="neiwen_share_social" style="width:90%;margin: 0 auto;">
				<div style="text-align:left;font-size:18px;">分享给朋友:</div>
				
					<div id="forsharebutton" class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="https://www.sinchew.com.my/pad/con/content_2044619.html" data-a2a-title="士乃中总及村民协会．6月23联办周年联欢宴">
						<a class="a2a_button_facebook shareArt"><img src="https://www.sinchew.com.my/resource/share-fb.png"></a><!--
						--><a class="a2a_button_twitter shareArt"><img src="https://www.sinchew.com.my/resource/share-twitter.png"></a><!--
						--><a class="a2a_button_google_plus shareArt"><img src="https://www.sinchew.com.my/resource/share-g+.png"></a><!--
						--><a class="a2a_button_email shareArt"><img src="https://www.sinchew.com.my/resource/share-email.png"></a><!--
						--><a class="a2a_button_whatsapp shareArt"><img src="https://www.sinchew.com.my/resource/share-whatapps.png"></a><!--
						--><a class="a2a_button_wechat shareArt"><img src="https://www.sinchew.com.my/resource/share-wechat.png"></a><!--
						--><a class="a2a_button_copy_link shareArt"><img src="https://www.sinchew.com.my/resource/copy-link.png"></a>
					</div>

			</div>
		</div>
		
		<!-- hide/show others new -->
	  	<div class="neiwen_othersnew">
			<div class="othersnew_left">其他新闻</div>
			<div class="othersnew_right icon-circle-arrow-up" id="show_news_click">&#9660</div>
		</div>
		
		<div class="clearall"></div>
		
		<div id="othernew_dropdown" style="text-align:left;"> <!-- 其他新闻 -->
			
				
		</div>
		
		<div class="clearall"></div>
			
		<hr class="neiwen_hr">
		
		<!-- Tenmax 有兴趣的新闻  -->
		<div class="neiwen-interest" style="text-align:left;font-weight:bold !important;font-size:18px !important;background:#e6e6e6 !important;padding:5px !important;">你也可能感兴趣...</div>
		<div class="_pvmax_recommend" data-widget-id="ba7b862c-9851-4406-b8f0-64b0d9b57413">&nbsp;</div>		
	    <br/> 
		

		      
        <hr class="neiwen_hr">
		
		<!-- hide/show comment-->
 	  	<div class="neiwen_comment">
			<div class="comment_left">评论</div>
			<div class="comment_right icon-circle-arrow-up" id="show_comment_click">&#9660</div>
		</div>
		
		<div class="clearall"></div>
		

<script type="text/javascript" src="https://www.sinchew.com.my/amucsite/stat/jquery.min.js"></script>
<div id="discuss"></div>
<script type="text/javascript">
var discuss = {
   rooturl:"www.sinchew.com.my",
   apiroot:"www.sinchew.com.my/app_if/",
   articleID:"2044619",
   siteID:"1",
   channel:1
}
</script>
<script charset="utf-8" type="text/javascript" src="https://www.sinchew.com.my/amucsite/discuss/js/discussMain.js"></script>
<script charset="utf-8" type="text/javascript" src="https://www.sinchew.com.my/amucsite/discuss/js/jquery.reveal.js"></script>
<link rel="stylesheet" href="https://www.sinchew.com.my/amucsite/discuss/css/reveal.css"><script type="text/javascript" src="https://www.sinchew.com.my/amucsite/stat/jquery.min.js"></script>
<script type="text/javascript" src="https://www.sinchew.com.my/amucsite/stat/WapClick.js"></script>
<input  type="hidden" id="DocIDforCount" name="DocIDforCount" value="2044619">


	    </div><!-- comment dropdown end -->
   	</div><!-- neiwen container end -->
	  
	<br/>
	 
	<!-- footer -->
	<!-- GDPR Start  -->
<style>
.eupopup-container {
	background-color: rgba(25, 25, 25, 0.9);
	color: #efefef;
	padding: 5px 20px;
	font-size: 14px;
	line-height: 1.4em;
	text-align: center;
	display: none;
	z-index: 9999999;
}

.eupopup-container-top,
.eupopup-container-fixedtop {
	position: absolute;
	top: 0; left: 0; right: 0;
}

.eupopup-container-fixedtop {
	position: fixed;
}

.eupopup-container-bottom {
	position: fixed;
	bottom: 0; left: 0; right: 0;
}

.eupopup-container-bottomleft {
	position: fixed;
	bottom: 10px;
	left: 10px;
	width: 300px;
}

.eupopup-container-bottomright {
	position: fixed;
	bottom: 10px;
	right: 10px;
	width: 300px;
}

.eupopup-closebutton {
	font-size: 16px;
	font-weight: 100;
	line-height: 1;
	color: #a2a2a2;
	filter: alpha(opacity=20);
	position: absolute;
	font-family: helvetica, arial, verdana, sans-serif;
	top: 0; right: 0;
	padding: 5px 10px;
}
.eupopup-closebutton:hover,
.eupopup-closebutton:active {
	color: #fff;
	text-decoration: none;
}

.eupopup-head {
	font-size: 1.2em;
	font-weight: bold;
	padding: 7px;
	color: #fff;
}

.eupopup-body {
	color: #a2a2a2;
}

.eupopup-buttons {
	padding: 7px 0 5px 0;
}

.eupopup-button_1 {
	color: #ff8533;
	font-weight: bold;
	font-size: 16px;
        margin-top:10px;
}

.eupopup-button_2 {
	color: #f6a21d;
	font-weight: normal;
	font-size: 12px;
}

.eupopup-button {
	margin: 0 10px;
}

.eupopup-button:hover,
.eupopup-button:focus {
	text-decoration: underline;
	color: #f6a21d;
}

body .eupopup-color-inverse {
	color: #000;
	background-color: rgba(255, 255, 255, 0.9);
}

body .eupopup-color-inverse .eupopup-head {
	color: #000;
}

body .eupopup-style-compact {
	text-align: left;
	padding: 8px 30px 7px 20px;
	line-height: 15px;
}

body .eupopup-style-compact .eupopup-head,
body .eupopup-style-compact .eupopup-body,
body .eupopup-style-compact .eupopup-buttons {
	display: inline;
	padding: 0;
	margin: 0;
}

body .eupopup-style-compact .eupopup-button {
	margin: 0 5px;
}
</style>

<script type="text/javascript">
/**
 *
 * JQUERY EU COOKIE LAW POPUPS
 * version 1.1.1
 *
 * Code on Github:
 * https://github.com/wimagguc/jquery-eu-cookie-law-popup
 *
 * To see a live demo, go to:
 * http://www.wimagguc.com/2018/05/gdpr-compliance-with-the-jquery-eu-cookie-law-plugin/
 *
 * by Richard Dancsi
 * http://www.wimagguc.com/
 *
 */

(function($) {

// for ie9 doesn't support debug console >>>
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };
// ^^^

$.fn.euCookieLawPopup = (function() {

	var _self = this;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PARAMETERS (MODIFY THIS PART) //////////////////////////////////////////////////////////////
	_self.params = {
		cookiePolicyUrl : 'http://archieve.sinchew.com.my/privacy',
		popupPosition : 'bottom',
		colorStyle : 'default',
		compactStyle : false,
		popupTitle : 'This website is using cookies',
		popupText : 'We use cookies to ensure that we give you the best experience on our website. If you continue without changing your settings, we\'ll assume that you are happy to receive all cookies on this website.',
		buttonContinueTitle : '同意',
		buttonLearnmoreTitle : '',
		buttonLearnmoreOpenInNewWindow : true,
		agreementExpiresInDays : 30,
		autoAcceptCookiePolicy : false,
		htmlMarkup : null
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// VARIABLES USED BY THE FUNCTION (DON'T MODIFY THIS PART) ////////////////////////////////////
	_self.vars = {
		INITIALISED : false,
		HTML_MARKUP : null,
		COOKIE_NAME : 'EU_COOKIE_LAW_CONSENT'
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PRIVATE FUNCTIONS FOR MANIPULATING DATA ////////////////////////////////////////////////////

	// Overwrite default parameters if any of those is present
	var parseParameters = function(object, markup, settings) {

		if (object) {
			var className = $(object).attr('class') ? $(object).attr('class') : '';
			if (className.indexOf('eupopup-top') > -1) {
				_self.params.popupPosition = 'bottom';
			}
			else if (className.indexOf('eupopup-fixedtop') > -1) {
				_self.params.popupPosition = 'fixedtop';
			}
			else if (className.indexOf('eupopup-bottomright') > -1) {
				_self.params.popupPosition = 'bottomright';
			}
			else if (className.indexOf('eupopup-bottomleft') > -1) {
				_self.params.popupPosition = 'bottomleft';
			}
			else if (className.indexOf('eupopup-bottom') > -1) {
				_self.params.popupPosition = 'bottom';
			}
			else if (className.indexOf('eupopup-block') > -1) {
				_self.params.popupPosition = 'block';
			}
			if (className.indexOf('eupopup-color-default') > -1) {
				_self.params.colorStyle = 'default';
			}
			else if (className.indexOf('eupopup-color-inverse') > -1) {
				_self.params.colorStyle = 'inverse';
			}
			if (className.indexOf('eupopup-style-compact') > -1) {
				_self.params.compactStyle = true;
			}
		}

		if (markup) {
			_self.params.htmlMarkup = markup;
		}

		if (settings) {
			if (typeof settings.cookiePolicyUrl !== 'undefined') {
				_self.params.cookiePolicyUrl = settings.cookiePolicyUrl;
			}
			if (typeof settings.popupPosition !== 'undefined') {
				_self.params.popupPosition = settings.popupPosition;
			}
			if (typeof settings.colorStyle !== 'undefined') {
				_self.params.colorStyle = settings.colorStyle;
			}
			if (typeof settings.popupTitle !== 'undefined') {
				_self.params.popupTitle = settings.popupTitle;
			}
			if (typeof settings.popupText !== 'undefined') {
				_self.params.popupText = settings.popupText;
			}
			if (typeof settings.buttonContinueTitle !== 'undefined') {
				_self.params.buttonContinueTitle = settings.buttonContinueTitle;
			}
			if (typeof settings.buttonLearnmoreTitle !== 'undefined') {
				_self.params.buttonLearnmoreTitle = settings.buttonLearnmoreTitle;
			}
			if (typeof settings.buttonLearnmoreOpenInNewWindow !== 'undefined') {
				_self.params.buttonLearnmoreOpenInNewWindow = settings.buttonLearnmoreOpenInNewWindow;
			}
			if (typeof settings.agreementExpiresInDays !== 'undefined') {
				_self.params.agreementExpiresInDays = settings.agreementExpiresInDays;
			}
			if (typeof settings.autoAcceptCookiePolicy !== 'undefined') {
				_self.params.autoAcceptCookiePolicy = settings.autoAcceptCookiePolicy;
			}
			if (typeof settings.htmlMarkup !== 'undefined') {
				_self.params.htmlMarkup = settings.htmlMarkup;
			}
		}

	};

	var createHtmlMarkup = function() {

		if (_self.params.htmlMarkup) {
			return _self.params.htmlMarkup;
		}

		var html =
			'<div class="eupopup-container' +
			    ' eupopup-container-' + _self.params.popupPosition +
			    (_self.params.compactStyle ? ' eupopup-style-compact' : '') +
				' eupopup-color-' + _self.params.colorStyle + '">' +
				'<div class="eupopup-head">' + _self.params.popupTitle + '</div>' +
				'<div class="eupopup-body">' + _self.params.popupText + '</div>' +
				'<div class="eupopup-buttons">' +
				  '<a href="#" class="eupopup-button eupopup-button_1">' + _self.params.buttonContinueTitle + '</a>' +
				  '<a href="' + _self.params.cookiePolicyUrl + '"' +
				 	(_self.params.buttonLearnmoreOpenInNewWindow ? ' target=_blank ' : '') +
					' class="eupopup-button eupopup-button_2">' + _self.params.buttonLearnmoreTitle + '</a>' +
				  '<div class="clearfix"></div>' +
				'</div>' +
				'<a href="#" class="eupopup-closebutton">x</a>' +
			'</div>';

		return html;
	};

	// Storing the consent in a cookie
	var setUserAcceptsCookies = function(consent) {
		var d = new Date();
		var expiresInDays = _self.params.agreementExpiresInDays * 24 * 60 * 60 * 1000;
		d.setTime( d.getTime() + expiresInDays );
		var expires = "expires=" + d.toGMTString();
		document.cookie = _self.vars.COOKIE_NAME + '=' + consent + "; " + expires + ";path=/";

		$(document).trigger("user_cookie_consent_changed", {'consent' : consent});
	};

	// Let's see if we have a consent cookie already
	var userAlreadyAcceptedCookies = function() {
		var userAcceptedCookies = false;
		var cookies = document.cookie.split(";");
		for (var i = 0; i < cookies.length; i++) {
			var c = cookies[i].trim();
			if (c.indexOf(_self.vars.COOKIE_NAME) == 0) {
				userAcceptedCookies = c.substring(_self.vars.COOKIE_NAME.length + 1, c.length);
			}
		}

		return userAcceptedCookies;
	};

	var hideContainer = function() {
		// $('.eupopup-container').slideUp(200);
		$('.eupopup-container').animate({
			opacity: 0,
			height: 0
		}, 200, function() {
			$('.eupopup-container').hide(0);
		});
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// PUBLIC FUNCTIONS  //////////////////////////////////////////////////////////////////////////
	var publicfunc = {

		// INITIALIZE EU COOKIE LAW POPUP /////////////////////////////////////////////////////////
		init : function(settings) {

			parseParameters(
				$(".eupopup").first(),
				$(".eupopup-markup").html(),
				settings);

			// No need to display this if user already accepted the policy
			if (userAlreadyAcceptedCookies()) {
        $(document).trigger("user_cookie_already_accepted", {'consent': true});
				return;
			}

			// We should initialise only once
			if (_self.vars.INITIALISED) {
				return;
			}
			_self.vars.INITIALISED = true;

			// Markup and event listeners >>>
			_self.vars.HTML_MARKUP = createHtmlMarkup();

			if ($('.eupopup-block').length > 0) {
				$('.eupopup-block').append(_self.vars.HTML_MARKUP);
			} else {
				$('BODY').append(_self.vars.HTML_MARKUP);
			}

			$('.eupopup-button_1').click(function() {
				setUserAcceptsCookies(true);
				hideContainer();
				return false;
			});
			$('.eupopup-closebutton').click(function() {
				setUserAcceptsCookies(true);
				hideContainer();
				return false;
			});
			// ^^^ Markup and event listeners

			// Ready to start!
			$('.eupopup-container').show();

			// In case it's alright to just display the message once
			if (_self.params.autoAcceptCookiePolicy) {
				setUserAcceptsCookies(true);
			}

		}

	};

	return publicfunc;
});

$(document).ready( function() {
	if ($(".eupopup").length > 0) {
		$(document).euCookieLawPopup().init({
			'info' : 'YOU_CAN_ADD_MORE_SETTINGS_HERE',
			'popupTitle' : '',
			'popupText' : '我们使用cookies来了解您如何使用和浏览我们的网站，并藉此提升您的使用体验，包括个性化的内容和广告。<br/>如您继续使用和浏览我们的网站，即表示您同意我们以上使用cookies的目的、更新的<a style="color:yellow;" href="http://archive.sinchew.com.my/privacy" target="blank">隐私条款</a> 和 <a style="color:yellow;" href="http://archieve.sinchew.com.my/terms" target="blank">使用条款</a>。'
		});
	}
});

$(document).bind("user_cookie_consent_changed", function(event, object) {
	console.log("User cookie consent changed: " + $(object).attr('consent') );
});

}(jQuery));
</script>

<div class="eupopup eupopup-top"></div>


<!-- GDPR End  -->

<div class="footer_container">
	
	<div class="footer_backtotop" >
		<a href="javascript:scrollTo(0,0);">&#9650;&#160; 回到最頂</a>
	</div> 
	
	<div class="footer_login footer_login_special" id="changefooterlogin">
		<div class="footerlogin" onclick="ssologin()">登入 / 注冊</div>
	</div>
	
	<br/>
	
	<div class="footer_lanmu">
		
			<div class="footer_changethistodiv">
										<a href="https://www.sinchew.com.my/m/nation.html">全国</a>
						<a href="https://www.sinchew.com.my/m/world.html">国际</a>
						<a href="https://www.sinchew.com.my/m/business.html">财经</a>
						<a href="https://www.sinchew.com.my/m/opinion.html">言路</a>
						<a href="https://www.sinchew.com.my/m/mykampung.html">地方</a>
						<a href="https://www.sinchew.com.my/m/entertainment.html">娱乐</a>
						<a href="https://www.sinchew.com.my/m/sport.html">体育</a>
						<a href="https://www.sinchew.com.my/m/life.html">副刊</a>
						<a href="https://www.sinchew.com.my/m/pocketimes.html">百格</a>
				<a href="http://classifieds.sinchew.com.my/content.phtml" target="blank">分类</a>
			</div>

	</div>
	
	<div class="footer_other_urllist">
	
		<hr class="footer_hr">
		<div class="footer_small_title"><b>星洲集团</b></div>
	  
		<div class="footer_first_line">
			<a href="http://epaper.sinchew.my" target="_blank">星洲电子报</a>&emsp; 
			<a href="http://www.mediachinese.com/" target="_blank">世华网</a>&emsp; 
			<a href="https://www.sinchew.com.my/m/sarawak.html" target="_blank">砂拉越星洲日报</a>&emsp; 
			<a href="http://www.guangming.com.my/" target="_blank">光明日报</a>&emsp; 
			<a href="http://indonesia.sinchew.com.my" target="_blank">印尼星洲日报</a>&emsp; 
			<a href="http://www.mysinchew.com/">MySinchew</a>&emsp; 
			<a href="http://www.logon.my/" target="_blank">Logon Online Shopping</a>&emsp; 
			<a href="http://www.xuehaiblog.com/" target="_blank">学海</a>&emsp; 
			<a href="http://archive.sinchew.com.my/node/1705896" target="_blank">小星星</a>
		</div>

		<hr class="footer_hr">

		<div class="footer_small_title"><b>友情连接</b></div>
		
		<div class="footer_second_line">
			<a href="https://www.chinapress.com.my" target="_blank">中国报</a>&emsp; 
			<a href="https://www.enanyang.my" target="_blank">e南洋商报</a>&emsp; 
			<a href="http://www.yunnan.cn" target="_blank">云南网 </a>&emsp; 
			<a href="http://www.asianews.network/" target="_blank">ANN ASIAN NEWSNETWORK</a>&emsp; 
			<a href="http://www.rhhotels.com.my/" target="_blank">RH Hotel, sibu, sarawak</a>&emsp; 
			<!--<a href="https://news.sinchew.com.my/topic/taxonomy/term/255" target="_blank">爱家</a>-->
		</div>

		<hr class="footer_hr">
		
		<div class="footer_small_title"><a href="http://lifemagazines.com.my/" class="block-title-link" target="_blank"><b>生活杂志</b></a></div>
		
		<div class="footer_third_line">
			<a href="https://www.newtide.com.my/" target="_blank">新潮</a>&emsp; 
			<a href="http://feminine.com.my/my-wellness/" target="_blank">My Wellness</a>&emsp; 
			<a href="http://feminine.com.my/" target="_blank">风采</a>&emsp; 
			<a href="http://feminine.com.my/oriental-cuisine/" target="_blank">美味风采</a>
		</div>
		
		<hr class="footer_hr">
		
		<div class="footer_small_title"><a href="http://archive.sinchew.com.my/intro" class="block-title-link" target="_blank"><b>关于我们</b></a></div>	
		
		<div class="footer_4_line">
			<a href="http://archive.sinchew.com.my/other/intro/index.php" target="_blank">公司简介</a>&emsp; 
			<a href="http://archive.sinchew.com.my/feedback " target="_blank">联络我们</a>&emsp; 
			<a href="https://www.sinchew.com.my/job.html"  target="_blank">求才若渴</a>&emsp; 
			<a href="http://archive.sinchew.com.my/faq" target="_blank">常见问题</a>&emsp; 
			<a href="https://www.sinchew.com.my/SubscriptionPress" target="_blank">订阅服务</a>&emsp; 
			<a href="https://www.sinchew.com.my/ratecard" target="_blank">刊登广告</a>&emsp; 
			<a href="http://archive.sinchew.com.my/Disclaimer" target="_blank">Disclaimer</a>&emsp; 
			<a href="http://archive.sinchew.com.my/privacy" target="_blank">私隐政策声明</a>&emsp; 
			<a href="http://archive.sinchew.com.my/terms" target="_blank">使用者条例</a>
		</div>
			
		<hr class="footer_hr">
		
		<div class="footer_small_title"><a href="https://www.sinchew.com.my/intro" class="block-title-link" target="_blank"><b>关注我们</b></a></div>
		
		<div class="footer_5_line">
			<div class="footer_social_img">
				<a href="https://www.facebook.com/SinChewDaily/" class="footer_social_ahref" target="_blank"><img src="https://www.sinchew.com.my/resource/icon-fb.png" class="social_image_size" target="_blank"></a>
				<a href="https://twitter.com/SinChewPress" class="footer_social_ahref" target="_blank"><img src="https://www.sinchew.com.my/resource/icon-twitter.png" class="social_image_size"></a>
				<a href="https://www.youtube.com/channel/UCKC1q1zc_CG2MtOCEHNUt1Q" class="footer_social_ahref" target="_blank"><img src="https://www.sinchew.com.my/resource/icon-youtube.png" class="social_image_size" target="_blank"></a>
				<a href="https://www.instagram.com/sinchewdaily/" class="footer_social_ahref" target="_blank"><img src="https://www.sinchew.com.my/resource/icon-instagram.png" class="social_image_size" target="_blank"></a>
				<a href="https://www.weibo.com/sinchewi" class="footer_social_ahref" target="_blank"><img src="https://www.sinchew.com.my/resource/icon-weibo.png" class="social_image_size"></a>
				<a href="http://archive.sinchew.com.my/wechat" class="footer_social_ahref" target="_blank"><img src="https://www.sinchew.com.my/resource/icon-wechat.png" class="social_image_size"></a>
			</div>
		</div>
		
		<hr class="footer_hr">
	  
		<div class="footer_6_line" style="text-align:center;">
			<div style="line-height:35px;" class="downloadourapp">
				<a href="https://www.sinchew.com.my/intro" class="block-title-link">现在就下载星洲网APP</a>
			</div>
			<div class="footer_app">	
				<a href="https://itunes.apple.com/my/app/sin-chew-%E6%98%9F%E6%B4%B2%E6%97%A5%E6%8A%A5/id1077727843?mt=8" target="_blank"><img src="https://www.sinchew.com.my/resource/apple-app.png"></a>&emsp; 
				<a href="https://play.google.com/store/apps/details?id=com.sinchewnews&hl=en_US" target="_blank"><img src="https://www.sinchew.com.my/resource/google-app.png"></a>
			</div>	
			<div class="footer_copyright">
				世华多媒体有限公司◆版权所有◆不得转载<br/>
				Copyright © 2018 MCIL Multimedia Sdn Bhd (515740-D). <br/>
				All rights reserved.<br/>
			</div>
		</div>
	</div> <!-- footer url list end -->
</div> <!-- footer container end -->

<script>
var userID = doGetCookie2("uid_fouNder");
	
function doGetCookie2(uid)
{
	var aCookie = document.cookie.split("; ");
	for (var i = 0; i < aCookie.length; i++) 
	{
		var aCrumb = aCookie[i].split("=");
		if (uid == aCrumb[0])
		{
			//return unescape(aCrumb[1]);
			if(aCrumb[1] == "")
			{
				document.getElementById("changefooterlogin").innerHTML = '<div class="footerlogin" onclick="ssologin()">登入 / 注冊</div>';
				return false;
			}
			
			document.getElementById("changefooterlogin").innerHTML = '<div class="footerlogin" onclick="logoutnow()">登出</div>';
			return true;
		}
	}

	document.getElementById("changefooterlogin").innerHTML = '<div class="footerlogin" onclick="ssologin()">登入 / 注冊</div>';
	return false;
}

function ssologin() 
{
	window.open("https://sso.sinchew.com.my:1443/SSOv2/user/ssoLogin?code=" + myFav.authCode + "&redirectUrl=" + myFav.login_out + "sso/setCookie.html?&anyUrl=" + location.href, "newwindow", myFav.window_site);
}

function logoutnow()
{
	window.open("https://sso.sinchew.com.my:1443/SSOv2/user/ssoLogout?code=" + personalMessage.authCode + "&uid=" + personalMessage.uid + "&sso_token=" + personalMessage.doGetCookie("sso_token") + "&from=" + personalMessage.login_out + "sso/delCookie.html", "newwindow", personalMessage.window_site);
	
	setTimeout(function () {
		location.reload();
	}, 5000);
}
</script>

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "10028553" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=10028553&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WC3H2D"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->  

{!! $options['{v3_script_tag}'] !!}
</body>
</html>