@extends('errors::minimal')

@section('title', __('422 - Unprocessable Entity'))
@section('code', '422')
@section('message', $exception->getMessage())

