<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ @$title ? str_finish(@$title, ' - ') : '' }}{{ config('player.title') }}</title>

    @section('before_style')

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @section('after_style')
</head>
<body>
    <div id="app">
        
        {{-- header --}}
        <div class="header mb-5 {{-- header-gradient --}} bg-white">
            <div class="pb-2 pt-4 mb-2">
                <h1 class="text-center">
                    IVS Proxy
                    <p class="my-1 text-muted" style="font-size: 20px;">Best Proxy Tools for demo You've ever seen!</p>
                    {{-- <small style="font-size: 15px;"><a href="{{ route('logout', $subdomain) }}">[Logout]</a></small> --}}
                </h1>
            </div>
            <div>
                <ul class="nav nav-tabs justify-content-center">
                    <li class="nav-item">
                        <a href="{{ route('site.collections', $subdomain) }}" class="nav-link {{ Route::currentRouteName() == 'site.collections' ? 'active' : '' }}">Collections</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('site.create', $subdomain) }}" class="nav-link {{ Route::currentRouteName() == 'site.create' ? 'active' : '' }}">Add Website</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('getShortcode') }}" class="nav-link {{ Route::currentRouteName() == 'getShortcode' ? 'active' : '' }}">Shortcode</a>
                    </li>
                    <li class="nav-item" style="margin-left: 50px;">
                        <a href="{{ route('logout', $subdomain) }}" class="nav-link text-danger">Logout</a>
                    </li>
                </ul>
            </div>
        </div>

        @if (session('message'))
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-md-10">
                        <div class="alert alert-success">
                            {!! session('message') !!}
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @yield('content')

        {{-- footer --}}
        <div class="footer-copyright text-center py-5 mt-5"><p>&copy;2019 Copyright ~ Built with Love by antoni@ivideosmart.com</p></div>
    </div>

    @yield('before_script')

    <script src="{{ asset('js/app.js') }}"></script>

    @yield('after_script')
</body>
</html>