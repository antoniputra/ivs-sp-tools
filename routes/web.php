<?php

require_once 'web_manual.php';

$domain = '{subdomain}.'. parse_url(config('app.url'), PHP_URL_HOST);
$mainDomain = str_replace('{subdomain}', env('APP_MAIN_SUBDOMAIN'), $domain);

Route::domain($mainDomain)->group(function () {
    Route::get('coba', function () {
        return view('dummy');
    });

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');

    Route::get('v3-demo', function () {
        return view('examples.v3-demo');
    });
    
    // shortcode
    Route::get('shortcode', 'SiteController@getShortcode')->name('getShortcode');
    Route::post('shortcode', 'SiteController@updateShortcode')->name('updateShortcode');
    Route::get('shortcode/delete/{shortcodeId}', 'SiteController@deleteShortcode')->name('deleteShortcode');
    // end shortcode

    Route::get('/', 'SiteController@index')->name('site.index');
    Route::get('/collections', 'SiteController@collections')->name('site.collections');
    Route::get('/create', 'SiteController@create')->name('site.create');
    Route::get('/{siteId}/edit', 'SiteController@edit')->name('site.edit');
    Route::get('/{siteId}/delete', 'SiteController@delete')->name('site.delete');
    Route::post('/process-website/{siteId?}', 'SiteController@process')->name('site.process');

    // will be removed
    Route::get('/update-status/{siteId}', 'SiteController@updateStatus')->name('site.updateStatus');
});

Route::domain($domain)->group(function ($subdomain) {
    Route::get('/{any}', 'SiteController@page')->where('any', '(.*)')->name('page');
});
