<?php

use App\Models\Site;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// FOsdPByn5pSHcIMu3gw3peCNnpGXFD2QDOvIfFIo9O41hihw6Ly1VprSXjqN
Route::middleware(['auth:api'])->group(function () {
    Route::get('/collections', function () {
        $status = request('status');
        $search = request('search');
        $sites = Site::latest()
            // orderByRaw("FIELD(status, '1', '0') ASC")
            ->when($search, function ($q) use ($search) {
                $q->where('title', 'like', "%$search%")
                    ->orWhere('url', 'like', "%$search%");
            })
            ->when($status, function ($q) use ($status) {
                if ($status != 'all') {
                    $status = $status == 'verified' ? 1 : 0;
                    $q->where('status', $status);
                }
            })
            ->paginate(5);

        header("Access-Control-Allow-Origin: *");
        return $sites;
    });
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
