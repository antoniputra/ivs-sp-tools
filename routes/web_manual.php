<?php

use App\Models\Option;

// Original Page: https://www.sinchew.com.my/content/content_2044619.html
$domainSinchew = 'spp-sinchew.'. parse_url(config('app.url'), PHP_URL_HOST);
Route::domain($domainSinchew)->group(function () {
    Route::get('/content/content_2044619.html', function () {
        $options = Option::get()->pluck('value', 'name')->toArray();
        return view('proxy-page.sinchew', compact('options'));
    });

    Route::get('/pad/con/content_2044619.html', function () {
        $options = Option::get()->pluck('value', 'name')->toArray();
        return view('proxy-page.sinchew-pad', compact('options'));
    });
});

// Original Page: https://www.mingweekly.com/beauty/beautynews/content-17413.html
$domainMingweekly = 'spp-mingweekly.'. parse_url(config('app.url'), PHP_URL_HOST);
Route::domain($domainMingweekly)->group(function () {
    Route::get('/beauty/beautynews/content-17413.html', function () {
        $options = Option::get()->pluck('value', 'name')->toArray();
        return view('proxy-page.mingweekly', compact('options'));
    });
});

// Original Page: https://womantalk.com/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj
$domainMingweekly = 'spp-womantalk.'. parse_url(config('app.url'), PHP_URL_HOST);
Route::domain($domainMingweekly)->group(function () {
    Route::get('/nutrition/articles/kepop-6-menurut-pakar-ini-yang-sebenarnya-terjadi-pada-tubuh-saat-sedang-puasa-xOrpj', function () {
        $options = Option::get()->pluck('value', 'name')->toArray();
        return view('proxy-page.womantalk', compact('options'));
    });
});
