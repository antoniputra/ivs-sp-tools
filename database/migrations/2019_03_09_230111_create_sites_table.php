<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 150)->nullable()->index();
            $table->longText('url');
            $table->string('subdomain');
            $table->longText('style_tag')->nullable();
            $table->longText('script_tag')->nullable();
            $table->longText('widget_tag');
            $table->string('widget_target_pattern');
            $table->string('widget_insert_position', 100);
            $table->string('replace_link_pattern')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sites');
    }
}
