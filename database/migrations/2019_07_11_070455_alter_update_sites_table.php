<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUpdateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sites', function (Blueprint $table) {
            // Update
            $table->longText('widget_tag')->nullable()->change();
            $table->string('widget_target_pattern')->nullable()->change();
            $table->string('widget_insert_position', 100)->nullable()->change();
            
            // add
            $table->boolean('is_php_dom')->after('script_tag')->default(0);
            $table->text('regex_pattern')->after('widget_insert_position')->nullable();
            $table->text('regex_replacement')->after('regex_pattern')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sites', function (Blueprint $table) {
            //
        });
    }
}
