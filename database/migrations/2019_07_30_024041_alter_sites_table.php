<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sites', function (Blueprint $table) {

            // Drop
            $table->dropColumn('is_php_dom');
            $table->dropColumn('widget_tag');
            $table->dropColumn('widget_target_pattern');
            $table->dropColumn('widget_insert_position');

            // Add
            $table->string('cdn')->nullable()->after('script_tag');
            $table->longText('inject_script')->nullable()->after('replace_link_pattern');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sites', function (Blueprint $table) {
            //
        });
    }
}
