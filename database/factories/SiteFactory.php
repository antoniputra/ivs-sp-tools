<?php

use App\Models\Replacement;
use App\Models\Site;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Site::class, function (Faker $faker) {
    return [
        'title' => $faker->words(3, true),
        'url' => $faker->url,
        'subdomain' => $faker->word,
        'style_tag' => null,
        'script_tag' => null,
        'widget_tag' => '<div id="ivs-widget-98586cd3-5138"></div>',
        'widget_target_pattern' => '.col-12.article p:nth-child(2)',
        'widget_insert_position' => $faker->randomElement(['before', 'after']),
        'replace_link_pattern' => null,
    ];
});

$factory->define(Replacement::class, function (Faker $faker) {
    return [
        'search' => $faker->word,
        'replace' => $faker->paragraph
    ];
});
