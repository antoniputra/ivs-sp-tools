<?php

use App\Models\Site;
use App\Models\User;
use App\Models\Option;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedOption();
        $this->seedUser();

        $data = [
            [
                'title' => 'Jawapos - Player v2',
                'url' => 'https://www.jawapos.com/internasional/09/04/2019/israel-laksanakan-pemilu-hari-ini-netanyahu-vs-benny-gantz/',
                'subdomain' => 'spp-jawapos-v2',
                'style_tag' => config('player.v2.style_tag'),
                'script_tag' => config('player.v2.script_tag'),
                'widget_tag' => config('player.v2.widget_tag'),
                'widget_target_pattern' => 'div.row div.col div.content p:nth-child(2)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => null,
                'status' => 1,
            ],
            [
                'title' => 'Jawapos - Player v3',
                'url' => 'https://www.jawapos.com/internasional/09/04/2019/israel-laksanakan-pemilu-hari-ini-netanyahu-vs-benny-gantz/',
                'subdomain' => 'spp-jawapos-v3',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.row div.col div.content p:nth-child(2)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => null,
                'status' => 1,
            ],
            [
                'title' => 'IDNTimes - Player v3 and Player v2',
                'url' => 'https://www.idntimes.com/news/indonesia/santi-dewi/breaking-kemenlu-cari-identitas-wni-penumpang-ethiopian-airlines',
                'subdomain' => 'spp-idntimes-v3',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'section.content-post.clearfix div article#article-content p:nth-child(2)',
                'widget_insert_position' => 'after',
                'replace_link_pattern' => null,
                'status' => 1,
                'replacements' => [
                    [
                        'search' => '<script id="ivsn" src="https://player.ivideosmart.com/ivideosense/player/js/ivsnload_v1.js?key=x0hySnavrT3936DPoxM078G09pqdXVG53pwvnw3K&amp;wid=ac37dab5-9363"></script>',
                        'replace' => null,
                    ]
                ],
            ],
            [
                'title' => 'BeritaSatu - Player v3',
                'url' => 'https://www.beritasatu.com/bisnis/542873/siang-ini-rupiah-dan-mata-uang-asia-tergerus',
                'subdomain' => 'spp-beritasatu-v3',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.primary-content div.hz_main_post_content div#ads_center_mobile p:nth-child(2)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => 'bisnis',
                'status' => 1,
            ],
            [
                'title' => 'Okezone - Player v3',
                'url' => 'https://news.okezone.com/read/2019/03/15/608/2030287/jokowi-serahkan-kartu-indonesia-pintar-untuk-ribuan-siswa-di-toba-samosir',
                'subdomain' => 'spp-okezone-v3',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.center-block div.detail.detail__billboard.class-vidy div#contentx p:nth-child(9)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => 'read',
                'status' => 1,
            ],
            [
                'title' => 'Harian Jogja - Player v3',
                'url' => 'https://news.harianjogja.com/read/2019/03/15/500/978271/-jelang-debat-ketiga-bpn-sebut-sandiaga-orang-cerdas-maruf-amin-bakal-pusing-tujuh-keliling',
                'subdomain' => 'spp-harianjogja-v3',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.row div.col.col_12_of_12 div.col.col_9_of_12.alpha.omega p:nth-child(3)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => 'read',
                'status' => 1,
            ],
            [
                'title' => 'MalaysiaKini - Player v3',
                'url' => 'https://www.malaysiakini.com/news/471100',
                'subdomain' => 'spp-malaysiakini',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.uk-card-body div#article_content div.content-drop p:nth-child(3)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => null,
                'status' => 1
            ],
            [
                'title' => 'HMetro - Player v3',
                'url' => 'https://www.hmetro.com.my/rap/2019/04/442041/normah-enggan-komen-video-aksi-lucah',
                'subdomain' => 'spp-hmetro',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.field.field-name-body.field-type-text-with-summary.field-label-hidden div.field-items div.field-item.even p:nth-child(5)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => null,
                'status' => 1
            ],
            [
                'title' => 'StraitsTimes - Player v3',
                'url' => 'https://www.straitstimes.com/asia/se-asia/jokowi-aide-blasts-prabowos-lies-on-corruption',
                'subdomain' => 'spp-straitstimes',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.field-name-body-linked.field.field-name-body.field-type-text-with-summary.field-label-hidden div.field-items div.odd.field-item p',
                'widget_insert_position' => 'after',
                'replace_link_pattern' => null,
                'status' => 1
            ],
            [
                'title' => 'Channelnewsasia - Player v3',
                'url' => 'https://www.channelnewsasia.com/news/singapore/singapore-malaysia-cooperative-forward-looking-relationship-11425048',
                'subdomain' => 'spp-channelnewsasia',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'div.grid__col-7.is-offset-3 article.c-article--default div.c-rte--article p:nth-child(2)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => null,
                'status' => 1
            ],
            [
                'title' => 'Asiaone - Player v3',
                'url' => 'https://www.asiaone.com/singapore/womans-leg-trapped-between-mrt-train-and-platform-buona-vista-station-during-peak-hour',
                'subdomain' => 'spp-asiaone',
                'style_tag' => '{v3_style_tag}',
                'script_tag' => '{v3_script_tag}',
                'widget_tag' => '{v3_widget_tag}',
                'widget_target_pattern' => 'section#block-system-main article#node-442807 div.field.field-name-body p:nth-child(2)',
                'widget_insert_position' => 'before',
                'replace_link_pattern' => null,
                'status' => 1
            ],
        ];

        foreach ($data as $d) {
            $replacements = array_get($d, 'replacements');
            if (isset($d['replacements'])) {
                unset($d['replacements']);
            }

            $site = Site::create($d);
            if ($replacements) {
                $site->replacements()->createMany($replacements);
            }
        }

        // dummy data
        // factory(Site::class, 4)->create();
    }

    protected function seedOption()
    {
        $datas = [
            [
                'title' => 'V3 Style Tag',
                'name' => '{v3_style_tag}',
                'value' => '<link href="https://s3v3.5kdg.com/app.css" rel="stylesheet">',
            ],
            [
                'title' => 'V3 Script Tag',
                'name' => '{v3_script_tag}',
                'value' => '<script type="text/javascript" src="https://s3v3.5kdg.com/1.bundle_cd4511ce3a12742a5703.chunk.js"></script><script type="text/javascript" src="https://s3v3.5kdg.com/sp-tools.bundle.js"></script>',
            ],
            [
                'title' => 'V3 Widget Tag',
                'name' => '{v3_widget_tag}',
                'value' => '<div>
    <ivs-video thumbnail="https://2.bp.blogspot.com/-xpgIOttwTTg/TWRni-cC-tI/AAAAAAAAA-o/9GMqFyMnQCY/s1600/african-elephant2.jpg" id="ivsplayer001" :id-i-frame="idIFrame"></ivs-video>
    <ivs-playlist type="carousel" widget-id="e40647ab-4135" api-key="38727828a6bcbae337183a66c50af04d" :playlist-data="playlistData" ivsplayer-id="ivsplayer001"></ivs-playlist>
</div>',
            ],
            [
                'title' => 'V2 AMP Resources',
                'name' => '{v2_amp_resources}',
                'value' => '<script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
<script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
<script async custom-element="amp-ima-video" src="https://cdn.ampproject.org/v0/amp-ima-video-0.1.js"></script>',
            ],
            [
                'title' => 'V2 AMP Widget',
                'name' => '{v2_amp_widget}',
                'value' => '<div>
    <!-- start ivx amp player -->
    <amp-list
            id="ivsVideoList"
            width="360"
            height="200"
            layout="responsive"
            credentials="include"
            src="https://ivxplayer.ivideosmart.com/prod/amp/763e9270-6319?title=TITLE&domain=SOURCE_HOST&ref=SOURCE_PATH&width=SCREEN_WIDTH">
        <template type="amp-mustache">
            <amp-analytics type="comscore" config="{{comscore_url}}"></amp-analytics>
            <amp-analytics type="metrika" config="{{metrika_url}}"></amp-analytics>
            <amp-ima-video
                    id="ivsVideoPlayer"
                    width="360"
                    height="200"
                    data-tag="{{ad_tag}}"
                    layout="responsive"
                    data-poster="{{thumbnail}}"
                    title="{{title}}"
                    data-vars-domain-appname="${ampdocHostname}"
                    data-vars-product-id="{{product_id}}"
                    data-vars-video-id="{{video_id}}"
                    data-vars-video-title="{{title}}"
                    data-vars-video-duration="{{video_duration}}"
                    data-vars-video-type="{{video_type}}"
                    data-vars-cp-id="{{cp_id}}"
                    data-vars-cp-name="{{cp_name}}"
                    data-vars-sp-id="{{sp_id}}"
                    data-vars-sp-name="{{sp_name}}"
                    data-vars-client-id="{{client_id}}"
                    data-vars-client-group="{{client_group}}"
                    data-vars-client-country="{{client_country}}"
                    data-vars-comscore-publisher-id="{{comscore_publisher_id}}"
                    data-delay-ad-request
                    rotate-to-fullscreen>
                <source src="{{video_src}}" type="application/vnd.apple.mpegURL">
            </amp-ima-video>
        </template>
    </amp-list>
    <!-- end ivx amp player -->
</div>',
            ],
        ];

        foreach ($datas as $data) {
            Option::create($data);
        }
    }

    protected function seedUser()
    {
        factory(User::class)->create([
            'name' => 'Admin SP Proxy',
            'email' => 'admin@ivideosites.com',
            'password' => bcrypt(env('ADMIN_PASSWORD', '!secret123')),
            'api_token' => 'FOsdPByn5pSHcIMu3gw3peCNnpGXFD2QDOvIfFIo9O41hihw6Ly1VprSXjqN',
        ]);
    }
}
