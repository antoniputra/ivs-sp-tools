# Task

## Major Task

- Search & Replace with Regex


## Minor Task

- Update required field Forms to be correct.
- Update UX when adding website.


# Objective

1. PHP DOM issue on SP Site that have incorrect html.
Since PHP Dom have an issue when the site have incorrect HTML, we need to provide another options to do appending without PHP DOM.
2. Make sure this tools can work with website that have pre-template (like mustache, etc..)
3. Fix issue on website that have CDN Assets.
4. Think solutions for website that load the content via ajax.


# Noted for next update

1. Tantannews & GMA should not use PHP DOM


# List SP Site to test

1. https://www.malaysiakini.com/news/471100
2. https://www.hmetro.com.my/rap/2019/04/442041/normah-enggan-komen-video-aksi-lucah
3. https://www.straitstimes.com/asia/se-asia/jokowi-aide-blasts-prabowos-lies-on-corruption
4. https://www.channelnewsasia.com/news/singapore/singapore-malaysia-cooperative-forward-looking-relationship-11425048
5. https://www.asiaone.com/singapore/womans-leg-trapped-between-mrt-train-and-platform-buona-vista-station-during-peak-hour


## Template for testing

```html

<div class="container">
    <div class="row">
        <div class="col-md-9">
            <h1>I am title</h1>

            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident labore maxime voluptatibus nam dolor fugiat. Provident, itaque ipsa, omnis facilis ut debitis assumenda necessitatibus eius quas a suscipit deleniti beatae?</p>

            <!-- put content here -->
            
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident labore maxime voluptatibus nam dolor fugiat. Provident, itaque ipsa, omnis facilis ut debitis assumenda necessitatibus eius quas a suscipit deleniti beatae?</p>
            
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident labore maxime voluptatibus nam dolor fugiat. Provident, itaque ipsa, omnis facilis ut debitis assumenda necessitatibus eius quas a suscipit deleniti beatae?</p>
        </div>
    </div>
</div>

```