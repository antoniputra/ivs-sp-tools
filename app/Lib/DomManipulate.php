<?php

namespace App\Lib;

use App\Models\Site;
use GuzzleHttp\Client as GuzzleClient;
use Symfony\Component\DomCrawler\Crawler;

class DomManipulate
{
    /** 
     * Site class
     * 
     * @var App\Models\Site
     */
    protected $site;

    /**
     * Crawler Class
     * 
     * @var \Symfony\Component\DomCrawler\Crawler
     */
    protected $crawler;

    /**
     * Result of crawler
     * 
     * @var string
     */
    protected $html;

    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    /**
     * Set a crawler
     * 
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     * @return $this
     */
    public function setCrawler(Crawler $crawler)
    {
        $this->crawler = $crawler;
        return $this;
    }

    /**
     * Get Crawler
     * 
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getCrawler()
    {
        return $this->crawler;
    }

    /**
     * Set Html
     * 
     * @param string $html
     * @return string
     */
    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * Get Html
     * 
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Get the content purely
     */
    public function requestContent($url)
    {
        $client = new GuzzleClient;
        $response = $client->request('GET', $url, [
            'verify' => false,
            'headers' => [
                'referer' => $url,
            ]
        ]);

        $this->html = $response->getBody()->getContents();
        return $this;
    }

    public function appendStyleScript()
    {
        $this->html = str_replace([
            '</head>',
            '</body>'
        ],
        [
            $this->site->renderTag($this->site->style_tag) .'</head>',
            $this->site->renderTag($this->site->script_tag) . $this->site->inject_script .'</body>'
        ], $this->getHtml());

        return $this;
    }

    /**
     * Only doing Replacement Link, under <body>
     */
    public function applyLinkReplacement($pattern, $siteId = null)
    {
        // -> THE FORMULA
        // $rep = url('/') .'/$2/news/indonesia/$3';
        // $html = $this->crawler->html();
        // $html = preg_replace('/(?:http|https):\/\/((?:\w+.)\w+.\w+\/)(.*)news\/indonesia\/(\S+)/i', $rep, $html);
        // return $html;

        $pattern = trim($pattern, '/');
        $replace = url('/') .'/$2'. $pattern .'/$3';
        $html = $this->getHtml();

        // Start html from body
        $lengthToBody = strpos($html, '<body');
        $htmlBodyStart = substr($html, $lengthToBody);
        $htmlBodyEnd = substr($html, 0, $lengthToBody);

        $pattern = str_replace('/', '\/', $pattern);
        $htmlReplacement = preg_replace("/(?:http|https):\/\/((?:\w+.)\w+.\w+\/)(.*){$pattern}\/(\S+)/i", $replace, $htmlBodyStart);
        $html = $htmlBodyEnd . $htmlReplacement;

        $this->html = $html;
        return $this;
    }

    public function applyRegex($html = null)
    {
        // do nothing if there is no regex pattern.
        if (!$this->site->regex_pattern) {
            return $this;
        }

        $html = $html ?: $this->getHtml();
        $regexPattern = trim($this->site->regex_pattern, '/');
        $htmlReplacement = preg_replace("/{$regexPattern}/", $this->site->regex_replacement, $html);
        
        $this->html = $this->site->renderTag($htmlReplacement);
        return $this;
    }

    public function applyReplacements($html = null)
    {
        $items = $this->site->replacements;
        if (count($items) < 1) {
            return $this;
        }

        $html = $html ?: $this->getHtml();
        foreach ($items as $item) {
            $html = str_replace($item['search'], $this->site->renderTag($item['replace']), $html);
        }

        $this->html = $html;
        return $this;
    }
}
