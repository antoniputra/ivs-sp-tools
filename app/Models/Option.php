<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = ['title', 'name', 'value'];

    public function getNameAsFieldName()
    {
        return str_replace(['{', '}'], ['', ''], $this->attributes['name']);
    }

    public function setNameAttribute($value)
    {
        if (!str_contains($value, ['{', '}'])) {
            $value = str_replace(['{', '}'], [' ', ' '], $value);
            $value = '{'. $value .'}';
        }
        $this->attributes['name'] = $value;
    }
}