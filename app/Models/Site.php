<?php

namespace App\Models;

use App\Models\Option;
use App\Models\Replacement;
use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    const STATUS_CREATED = 0;
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 2;

    protected $appends = ['result_url'];

    protected $fillable = ['title', 'url', 'subdomain', 'style_tag', 'script_tag', 'cdn', 'regex_pattern', 'regex_replacement', 'replace_link_pattern', 'inject_script'];

    public function replacements()
    {
        return $this->hasMany(Replacement::class);
    }

    public function getParseUrl()
    {
        return parse_url($this->url);
    }

    public function getResultUrl()
    {
        $parseUrl = $this->getParseUrl();
        $base_url = preg_replace("(^https?://)", "", config('app.url'));
        return 'http://'. $this->subdomain .'.'. $base_url . $parseUrl['path'];
    }

    public function getResultUrlAttribute()
    {
        return $this->getResultUrl();
    }

    public function renderTag($content)
    {
        $options = Option::get()->pluck('value', 'name')->toArray();

        return  str_replace(array_keys($options), array_values($options), $content);
    }

    public function getRegexStart()
    {
        // THIS IS REGEX FORMULA
        // =>       /()([^]*?)()/g
        // e.g:     /(FROM_USER)([^]*?)(FROM_USER)/g

        // /(<p>Sekadar informasi)([^]*?)(</p>)/g
        if (preg_match('/\/\((.*?)\)/', $this->regex_pattern, $match) == 1) {
            return $match[1];
        }
    }

    public function getRegexEnd()
    {
        if (preg_match('/\?\)\((.*?)\)\//', $this->regex_pattern, $match) == 1) {
            return $match[1];
        }
    }

    public function getRegexGroup($group = 1)
    {
        // THIS IS REGEX FORMULA
        // =>       /()([^]*?)()/g
        // e.g:     /(FROM_USER)([^]*?)(FROM_USER)/g

        if ($group == 1) {
            // /(<p>Sekadar informasi)([^]*?)(</p>)/g
            if (preg_match('/\/\((.*?)\)/', $this->regex_pattern, $match) == 1) {
                return $match[1];
            }
        }

        if ($group == 2) {
            return '(.*?)';
        }

        if ($group == 3) {
            if (preg_match('/\?\)\((.*?)\)\//', $this->regex_pattern, $match) == 1) {
                return $match[1];
            }
        }
    }
}
