<?php

namespace App\Models;

use App\Models\Site;
use Illuminate\Database\Eloquent\Model;

class Replacement extends Model
{
    protected $table = 'site_replacements';
    
    protected $fillable = ['search', 'replace'];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
