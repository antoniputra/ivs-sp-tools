<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\Models\Option;
use App\Lib\DomManipulate;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'process', 'page');
        view()->share([
            'subdomain' => request()->subdomain,
        ]);
    }

    public function index()
    {
        if (auth()->check()) {
            return redirect()->route('site.collections');
        }
        return redirect()->route('login');
    }

    public function collections()
    {
        $status = request('status');
        $search = request('search');
        $sites = Site::latest()
            // orderByRaw("FIELD(status, '1', '0') ASC")
            ->when($search, function ($q) use ($search) {
                $q->where('title', 'like', "%$search%")
                    ->orWhere('url', 'like', "%$search%");
            })
            ->when($status, function ($q) use ($status) {
                if ($status != 'all') {
                    $status = $status == 'verified' ? 1 : 0;
                    $q->where('status', $status);
                }
            })
            ->paginate(10);

        $data = [
            'sites' => $sites
        ];
        return view('site.collections', $data);
    }

    public function create()
    {
        $data = [
            'title' => 'Create new Website',
            'shortcodes' => Option::get(),
        ];
        return view('site.form', $data);
    }

    public function edit($siteId)
    {
        $site = Site::with('replacements')->findOrFail($siteId);
        $data = [
            'title' => 'Edit site: '. $site->title,
            'site' => $site,
            'shortcodes' => Option::get(),
        ];
        return view('site.form', $data);
    }

    public function process($siteId = null, Request $request)
    {
        $isEdit = $request->site_id;
        $strRand = strtolower(str_random(rand(3,4)));

        // Get exact "domain" from given "url" to store in "subdomain" field,
        if (!$request->input('subdomain')) {
            $target = explode('.', preg_replace("(^https?://)", "", $request->url));
            $arrOffset = count($target) > 2 ? 1 : 0;
            $sub = array_get($target, $arrOffset, $strRand);
            $request->offsetSet('subdomain', $sub);
        }

        if (!str_contains($request->input('subdomain'), env('APP_SP_SUBDOMAIN', 'spp-'))) {
            $sub = env('APP_SP_SUBDOMAIN', 'spp-'). $request->input('subdomain');
            $request->merge(['subdomain' => $sub]);
        }

        // make sure the "subdomain" is unique, otherwise give unique string.
        if (!$isEdit) {
            $sub = $request->input('subdomain');
            $check = Site::where('subdomain', $sub)->count();
            $sub = $check ? $sub .'-'. $strRand : $sub;
            $request->merge(['subdomain' => $sub]);
        }
        // return $request->all();

        // if not edit
        if ($siteId && !$isEdit) {
            $site = Site::find($siteId);
        }

        // handle create or edit
        else {
            $site_id = $request->site_id ?: 'NULL';
            $validator = Validator::make($request->all(), [
                'url' => 'required|url',
                'subdomain' => 'required|alpha_dash|unique:sites,subdomain,'. $site_id,
                'title' => 'required'
            ]);

            if ($validator->fails()) {
                if ($request->get('source_extension')) {
                    $error = $validator->errors()->all();
                    $error = implode('<br/>', $error);
                    return abort(422, $error);
                }

                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $site = $this->saveSite($request);
        }

        if (!$site) {
            abort(404);
        }

        if ($request->get('source_extension')) {
            return redirect()->to($site->getResultUrl());
        }

        $msg = '<a href="'. $site->getResultUrl() .'" target="_blank">Click Here</a> to open the page.';
        return redirect()->route('site.edit', $site->id)->withMessage('Successfully Save: "'. $site->title .'" - '. $msg);
    }

    public function page($subdomain, $any = null)
    {
        $site = Site::where('subdomain', $subdomain)->first();
        if (!$site) {
            $site = session('site_temporary');
        }

        if (!$site) {
            abort(404);
        }

        // Proxy assets of SP Page
        // Because this method are pointed as wildcard routes
        if ($spAssets = $this->getSpAssets($site)) {
            return $spAssets;
        }

        $siteParse = $site->getParseUrl();
        $url = $siteParse['scheme'] .'://'. str_finish($siteParse['host'], '/') . trim(request()->getPathInfo(), '/');

        $dom = new DomManipulate($site);
        return $dom->requestContent($url)
            ->appendStyleScript()
            ->applyRegex()
            ->applyReplacements()
            ->getHtml();
    }

    public function updateStatus($subdomain, $siteId, Request $request)
    {
        $site = Site::findOrFail($siteId);
        $site->status = (integer) $request->status;
        $site->save();

        $msg = $request->status == 1 ? 'Has been marked as works' : 'Works marks has been removed';

        return redirect()->route('site.collections', $subdomain)->withMessage($site->title .' - '. $msg);
    }

    public function delete($siteId)
    {
        $site = Site::findOrFail($siteId);
        $site->delete();

        return redirect()->route('site.collections')->withMessage($site->title .' - Successfully deleted');
    }

    public function getShortcode()
    {
        $options = Option::whereNotIn('id', [1,2,3])->get();
        $data = [
            'title' => 'Manage Shortcode',
            'options_default' => Option::whereIn('id', [1,2,3])->get(),
            'options' => $options,
            'newKey' => $options->count() +1,
        ];
        return view('setting', $data);
    }

    public function updateShortcode(Request $request)
    {
        $this->validate($request, [
            'v3_style_tag' => 'required',
            'v3_script_tag' => 'required',
            'v3_widget_tag' => 'required',
            // 'settings.*.name' => 'required',
            // 'settings.*.value' => 'required',
        ]);

        Option::where('name', '{v3_style_tag}')->update([
            'value' => $request->get('v3_style_tag'),
        ]);
        Option::where('name', '{v3_script_tag}')->update([
            'value' => $request->get('v3_script_tag'),
        ]);
        Option::where('name', '{v3_widget_tag}')->update([
            'value' => $request->get('v3_widget_tag'),
        ]);

        $settings = $request->get('settings');
        if (count($settings) > 0) {
            foreach ($settings as $key => $setting) {
                if (array_get($setting, 'name') && array_get($setting, 'value')) {

                    $settingName = $setting['name'];
                    if (!str_contains($settingName, ['{', '}'])) {
                        $settingName = '{'. $setting['name'] .'}';
                    }

                    Option::updateOrCreate(
                        [
                            'id' => array_get($setting, 'id')
                        ],
                        [
                            'title' => str_replace(['_', '{', '}'], [' ', '', ''], $setting['name']),
                            'name' => $settingName,
                            'value' => $setting['value']
                        ]
                    );
                }
            }
        }

        return redirect()->route('getShortcode')->withMessage('All Shortcode has been Successfully Updated.');
    }

    public function deleteShortcode($shortcodeId)
    {
        $shortcode = Option::findOrFail($shortcodeId);
        $shortcode->delete();

        return redirect()->route('getShortcode')->withMessage(' Shortcode: "'. $shortcode->name .'" - Successfully deleted');
    }

    private function saveSite(Request $request)
    {
        $replacements = collect($request->get('replacements'))->filter(function ($item) {
            return !empty($item['search']) || !empty($item['id']);
        })->toArray();

        // Handle save to database
        if ($request->title && !$request->is_preview) {
            if ($site_id = $request->site_id) {
                $site = Site::find($site_id);
                $site->fill($request->all());
                $site->save();
            } else {
                $site = Site::create($request->all());
            }

            if (!empty($replacements)) {
                // edit
                if ($request->site_id) {
                    foreach ($replacements as $rep) {
                        if(!empty($rep['search'])) {
                            $site->replacements()->updateOrCreate(['id' => array_get($rep, 'id')], [
                                'search' => array_get($rep, 'search'),
                                'replace' => array_get($rep, 'replace'),
                            ]);
                        } else {
                            $site->replacements()->where('id', $rep['id'])->delete();
                        }
                    }
                }
                
                // create
                else {
                    $site->replacements()->createMany($replacements);
                }
            }

            session()->forget('site_temporary_on_edit');
        }

        // If only preview
        else {
            $siteTemp = (new Site)->fill($request->all());
            if (!empty($replacements)) {
                $siteTemp->replacements = $replacements;
            }
            session(['site_temporary' => $siteTemp]);
            session(['site_temporary_on_edit' => $request->site_id]);
            $site = session('site_temporary');
        }

        return $site;
    }

    private function getSpAssets($site)
    {
        header("Access-Control-Allow-Origin: *");

        $contentType = null;
        $uriPath = request()->getPathInfo();

        // For original resources/assets
        if (str_contains($uriPath, '.jpg')) {
            $contentType = 'image/jpg';
        }
        elseif(str_contains($uriPath, '.jpeg')) {
            $contentType = 'image/jpeg';
        }
        elseif(str_contains($uriPath, '.png')) {
            $contentType = 'image/png';
        }
        elseif(str_contains($uriPath, '.gif')) {
            $contentType = 'image/gif';
        }
        elseif(str_contains($uriPath, '.svg')) {
            $contentType = 'image/svg';
        }
        elseif(str_contains($uriPath, '.css')) {
            $contentType = 'text/css';
        }
        elseif(str_contains($uriPath, '.js')) {
            $contentType = 'application/javascript';
        }
        elseif(str_contains($uriPath, '.woff')) {
            $contentType = 'application/font-woff';
        }
        elseif(str_contains($uriPath, '.woff2')) {
            $contentType = 'application/octet-stream';
        }
        elseif(str_contains($uriPath, '.otf')) {
            $contentType = 'font/opentype';
        }

        // for other extension
        // elseif(str_contains($uriPath, '.php')) {
        //     $contentType = 'text/plain';
        // }

        if ($contentType) {
            $siteParse = $site->getParseUrl();
            $url = $siteParse['scheme'] .'://'. str_finish($siteParse['host'], '/') . trim($uriPath, '/');

            // Proxy the CDN Url
            if ($site->cdn) {
                $url = str_finish($site->cdn, '/') . trim($uriPath, '/');
            }

            try {
                $client = new GuzzleClient;
                $response = $client->request('GET', $url, [
                    'verify' => false,
                    'headers' => [
                        'referer' => $url,
                    ],
                    'connect_timeout' => 10,
                    'timeout' => 15
                ]);
                $statusCode = $response->getStatusCode();
                $content = $response->getBody()->getContents();

                if ($statusCode >= 200 && $statusCode < 400) {
                    return response($content)->header('Content-Type', $contentType);
                }
            } catch (\GuzzleHttp\Exception\ClientException $err) {
                \Log::error('IVS Assistant on: '. $site->url .' (Guzzle Error)', [$err->getMessage()]);
            } catch (\Exception $err) {
                \Log::error('IVS Assistant on: '. $site->url .' (Guzzle Error) ', [$err->getMessage()]);
            }
        }
    }
}
